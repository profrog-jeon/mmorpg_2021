#include "stdafx.h"
#include "GameServer.h"
#include "MonsterContainer.h"

CGameServer::CGameServer(void)
	: CMainGameLoop(&m_Renderer)
	, CServer()
	, m_hGameThread(NULL)
	, m_bContinue(false)
{
}

CGameServer::~CGameServer(void)
{
}

DWORD WINAPI GameThreadCaller(void* pContext)
{
	CGameServer* pThis = (CGameServer*)pContext;
	pThis->GameThread();
	return 0;
}

int CGameServer::StartUp(const ST_SERVER_INFO& info)
{
	m_bContinue = true;
	GlobalTimer()->SetTime(0);
	m_hGameThread = CreateThread(nullptr, 0, GameThreadCaller, this, 0, nullptr);
	return CServer::StartUp(info);
}

void CGameServer::ShutDown(void)
{
	m_bContinue = false;
	CServer::ShutDown();
	::TerminateThread(m_hGameThread, 0);
	::CloseHandle(m_hGameThread);
}

void CGameServer::Loop(void)
{
	CServer::Loop();
}

bool CGameServer::OnFrontLoop(DWORD dwGameTime)
{
	CPlayerObject* pPlayer = NULL;
	while (ERROR_SUCCESS == m_AddPlayerQueue.Pop(pPlayer))
	{
		Objs()->AddPlayer(pPlayer);

		CMD_ADD_PLAYER cmd;
		cmd.objectData = *static_cast<ST_OBJECT_DATA*>(pPlayer);
		cmd.playerData = *static_cast<ST_PLAYER_DATA*>(pPlayer);
		Broadcast(cmd);
	}

	while (ERROR_SUCCESS == m_RemovePlayerQueue.Pop(pPlayer))
	{
		CMD_REMOVE_PLAYER cmd;
		cmd.nUserID = pPlayer->GetPlayerID();
		Objs()->RemovePlayer(pPlayer->GetPlayerID());
		Broadcast(cmd);
	}

	REQ_USERINFO_UPDATE reqUserInfo;
	while (ERROR_SUCCESS == m_UserUpdateQueue.Pop(reqUserInfo))
	{
		CMD_USERINFO_UPDATE cmd;
		cmd.nUserID = reqUserInfo.nUserID;
		cmd.cSpecialParts = reqUserInfo.cSpecialParts;		
		Broadcast(cmd);
	}

	std::vector<CMonsterObject*> vecNewMonsters;
	MonsterContainer()->RegenMonster(vecNewMonsters);

	for (CMonsterObject* pNewMonster : vecNewMonsters)
	{
		CMD_ADD_MONSTER cmd;
		cmd.objectData = *static_cast<ST_OBJECT_DATA*>(pNewMonster);
		cmd.monsterData = *static_cast<ST_MONSTER_DATA*>(pNewMonster);
		Objs()->AddObject(pNewMonster);
		Broadcast(cmd);
	}
	
	{
		CMD_INPUT cmd;
		cmd.dwGameTick = GlobalTimer()->GetTime();
		cmd.nAggregateCount = 0;

		REQ_INPUT input;
		while (ERROR_SUCCESS == m_InputQueue.Pop(input))
		{
			cmd.aggregate[cmd.nAggregateCount++] = input;
			Update(input.nUserID, input.input, input.nCount);
		}
		Broadcast(cmd);
	}
	return true;
}

void CGameServer::GameThread(void)
{
	CMainGameLoop::Loop(true);
}
