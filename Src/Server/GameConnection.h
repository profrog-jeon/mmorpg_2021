#pragma once

#include "../NetworkFramework/NetworkFramework.h"

class CGameConnection : public CConnectionSocket
{
	CPlayerObject* m_pPlayer;
	std::string m_strName;

public:
	CGameConnection(void);
	~CGameConnection(void);

private:
	void OnPacketRecved(const PCK_HEADER& header, void* pBody);
	void OnClose(void);
};

