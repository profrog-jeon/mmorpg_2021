#pragma once

class CDatabase
{
	int m_nUserIDIssuer;

	CDatabase(void);
	~CDatabase(void);

public:
	bool GetPlayerData(std::string strID, std::string strPassword, ST_PLAYER_DATA& outData);

	static CDatabase* GetInstance(void)
	{
		static CDatabase instance;
		return &instance;
	}
};

inline CDatabase* DB(void)
{
	return CDatabase::GetInstance();
}