﻿#include "stdafx.h"
#include "GameServer.h"
#include "GameConnection.h"

int main()
{
    int nRet = 0;

    ST_SERVER_INFO info_game_srv;
	info_game_srv.wPort = GAME_SERVER_PORT;

	for (int i = 0; i < g_nMaxPlayerCount; i++) {
		info_game_srv.Connections.push_back(new CGameConnection());
	}

    nRet = GameServer()->StartUp(info_game_srv);
    if (nRet)
    {
        ::printf("server.StartUp(info) failure, %d\n", nRet);
        return -1;
    }

	::printf("Server start up successfully.\n");

    GameServer()->Loop();
    GameServer()->ShutDown();
    return 0;
}
