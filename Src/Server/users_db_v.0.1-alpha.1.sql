create database mmorpg;
use mmorpg;
CREATE TABLE `users` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `nickname` char(20),
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  `pos_x` double DEFAULT 43,
  `pos_y` double DEFAULT 78,
  `pos_z` double DEFAULT 27,
  `flags` char(100) DEFAULT NULL,
  `salt` varchar(255),
  `pass` varchar(64)
);

insert into  users(id, nickname, pass,salt) values(0,'root','1234','SALT');
insert into  users(id, nickname, pass,salt) values(0,'test','1234','SALT');