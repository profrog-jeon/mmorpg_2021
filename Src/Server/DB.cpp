#include "stdafx.h"
#include "DB.h"

CDatabase::CDatabase(void)
	: m_nUserIDIssuer(0)
{

}

CDatabase::~CDatabase(void)
{

}

bool CDatabase::GetPlayerData(std::string strID, std::string strPassword, ST_PLAYER_DATA& outData)
{
	outData.m_nPlayerID = ++m_nUserIDIssuer;
	sprintf_s(outData.m_szUserName, g_nMaxUserNameLen, "%s", strID.c_str());
	return true;
}
