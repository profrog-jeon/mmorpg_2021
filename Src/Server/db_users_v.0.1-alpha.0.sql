CREATE TABLE `users` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `nickname` char(20),
  `created_at` timestamp,
  `pos_x` int,
  `pos_y` int,
  `pos_z` int,
  `flags` char(100),
  `salt` varchar(255),
  `pass` varchar(255)
);
