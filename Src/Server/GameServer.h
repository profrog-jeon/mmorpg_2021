#pragma once

#include "../GameFramework/GameFramework.h"
#include "../NetworkFramework/NetworkFramework.h"
#include "DummyRenderer.h"

class CGameServer : public CMainGameLoop, public CServer
{
	friend DWORD WINAPI GameThreadCaller(void* pContext);
	friend class CGameConnection;

	HANDLE m_hGameThread;
	bool m_bContinue;

	CSafeQueue<REQ_INPUT> m_InputQueue;
	CSafeQueue<REQ_USERINFO_UPDATE> m_UserUpdateQueue;
	CSafeQueue<CPlayerObject*> m_AddPlayerQueue;
	CSafeQueue<CPlayerObject*> m_RemovePlayerQueue;

	CDummyRenderer m_Renderer;

	CGameServer(void);
	~CGameServer(void);

public:
	int StartUp(const ST_SERVER_INFO& info);
	void ShutDown(void);
	void Loop(void);

	static CGameServer* GetInstance(void)
	{
		static CGameServer instance;
		return &instance;
	}

private:
	bool OnFrontLoop(DWORD dwGameTime);

	void GameThread(void);
};

inline CGameServer* GameServer(void)
{
	return CGameServer::GetInstance();
}