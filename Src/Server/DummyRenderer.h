#pragma once

class CDummyRenderer : public CRendererSuper
{
public:
	CDummyRenderer(void);
	~CDummyRenderer(void);

private:
	void Clear(void);
	void Flush(void);

	void Rectangle(int left, int top, int right, int bottom, COLORREF color, char patch);
	void Ellipse(int left, int top, int right, int bottom, COLORREF color, char patch);
	void TextOut(int left, int top, std::string text);
};

