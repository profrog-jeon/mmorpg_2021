#pragma once

#include "../GameFramework/GameFramework.h"

class CMonsterContainer
{
	std::list<ST_MONSTER_REGEN_DATA> m_MonsterData;

	CMonsterContainer(void);
	~CMonsterContainer(void);

public:
	void RegenMonster(std::vector<CMonsterObject*>& vecNewMonsters);

	static CMonsterContainer* GetInstance(void)
	{
		static CMonsterContainer instance;
		return &instance;
	}
};

inline CMonsterContainer* MonsterContainer(void)
{
	return CMonsterContainer::GetInstance();
}