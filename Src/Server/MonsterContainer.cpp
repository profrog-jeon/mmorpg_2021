#include "stdafx.h"
#include "MonsterContainer.h"

CMonsterContainer::CMonsterContainer(void)
{
	m_MonsterData.push_back(ST_MONSTER_REGEN_DATA{ 1, 3000, "������", 66, 106, 3, 3, 50, 0, 0 });
}

CMonsterContainer::~CMonsterContainer(void)
{
}

void CMonsterContainer::RegenMonster(std::vector<CMonsterObject*>& vecNewMonsters)
{
	for (ST_MONSTER_REGEN_DATA& regenData : m_MonsterData)
	{
		if (0 < Objs()->CountMonster(regenData.id))
			continue;
				
		if (0 == regenData.dwRegenTimeStamp)
		{
			regenData.dwRegenTimeStamp = GetTickCount() + regenData.dwCoolTime;
			continue;
		}

		if (GetTickCount() < regenData.dwRegenTimeStamp)
			continue;
		
		CMonsterObject* pMonster = new CMonsterObject(regenData);
		vecNewMonsters.push_back(pMonster);
		regenData.dwRegenTimeStamp = 0;
	}
	
}
