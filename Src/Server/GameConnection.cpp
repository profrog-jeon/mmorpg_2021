#include "stdafx.h"
#include "GameConnection.h"
#include "GameServer.h"
#include "DB.h"

CGameConnection::CGameConnection(void)
	: m_pPlayer(nullptr)
{
}

CGameConnection::~CGameConnection(void)
{
}

void CGameConnection::OnPacketRecved(const PCK_HEADER& header, void* pBody)
{
	//printf("Recv packet id :  %d\n", header.ID);
	srand(GetTickCount());

	int nRet = 0;
	if (header.ID == REQ_LOGIN::ID)
	{
		REQ_LOGIN* pReq = (REQ_LOGIN*)pBody;
		CMD_LOGIN cmd;

		CMD_INITIAL_INFO cmd2;
		cmd.bSucceeded = DB()->GetPlayerData(pReq->szUserID, pReq->szPassword, cmd2.user);
		nRet = SendPacket(this, cmd);

		if (!cmd.bSucceeded)
			return;

		{
			m_pPlayer = new CPlayerObject(cmd2.user);
			m_pPlayer->m_PosX = rand() % 10 + 43;
			m_pPlayer->m_PosY = rand() % 10 + 78;
			GameServer()->m_AddPlayerQueue.Push(m_pPlayer);
		}

		printf("%s(id:%d) logged in at(%.0lf, %.0lf)\n"
			, pReq->szUserID, cmd2.user.m_nPlayerID
			, m_pPlayer->m_PosX, m_pPlayer->m_PosY);

		m_strName = pReq->szUserID;

		Objs()->QueryObjects(cmd2.game);
		nRet = SendPacket(this, cmd2);
	}
	if (header.ID == REQ_INPUT::ID)
	{
		REQ_INPUT* pReq = (REQ_INPUT*)pBody;
		GameServer()->m_InputQueue.Push(*pReq);
	}
	if (header.ID == REQ_USERINFO_UPDATE::ID)
	{
		REQ_USERINFO_UPDATE* pReq = (REQ_USERINFO_UPDATE*)pBody;
		GameServer()->m_UserUpdateQueue.Push(*pReq);
	}
}

void CGameConnection::OnClose(void)
{
	if (m_pPlayer)
	{
		GameServer()->m_RemovePlayerQueue.Push(m_pPlayer);
		printf("%s logged out\n", m_strName.c_str());
		m_pPlayer = nullptr;
	}
}
