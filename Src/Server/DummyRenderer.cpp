#include "stdafx.h"
#include "DummyRenderer.h"

CDummyRenderer::CDummyRenderer(void)
{

}

CDummyRenderer::~CDummyRenderer(void)
{

}

void CDummyRenderer::Clear(void)
{

}

void CDummyRenderer::Flush(void)
{

}

void CDummyRenderer::Rectangle(int left, int top, int right, int bottom, COLORREF color, char patch)
{

}

void CDummyRenderer::Ellipse(int left, int top, int right, int bottom, COLORREF color, char patch)
{

}

void CDummyRenderer::TextOut(int left, int top, std::string text)
{

}
