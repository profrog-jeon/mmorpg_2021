#pragma once

#include "../GameFramework/GameFramework.h"
#include "../NetworkFramework/NetworkFramework.h"

class CChatServer : public CChatLoop, public CServer
{
	friend DWORD WINAPI ChatThreadCaller(void* pContext);
	friend class CChatConnection;

	HANDLE m_hChatThread;
	bool m_bContinue;

	CSafeQueue<REQ_CHAT> m_ChatQueue;
	CSafeQueue<CPlayerObject*> m_AddPlayerQueue;
	CSafeQueue<CPlayerObject*> m_RemovePlayerQueue;

	CChatServer(void);
	~CChatServer(void);

public:
	int StartUp(const ST_SERVER_INFO& info);
	void ShutDown(void);
	void Loop(void);

	static CChatServer* GetInstance(void)
	{
		static CChatServer instance;
		return &instance;
	}

private:
	bool OnFrontLoop(DWORD dwChatTime);
	bool OnBackLoop(DWORD dwChatTime);

	void ChatThread(void);
};

inline CChatServer* ChatServer(void)
{
	return CChatServer::GetInstance();
}