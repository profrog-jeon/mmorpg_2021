﻿#include "stdafx.h"
#include "ChatServer.h"
#include "ChatConnection.h"



int main()
{

    int nRet = 0;

    ST_SERVER_INFO info_chat_srv;
	info_chat_srv.wPort = CHAT_SERVER_PORT;
	for (int i = 0; i < g_nMaxPlayerCount; i++) {
		info_chat_srv.Connections.push_back(new CChatConnection());
	}

	nRet = ChatServer()->StartUp(info_chat_srv);
	if (nRet)
	{
		::printf("server.StartUp(info) failure, %d\n", nRet);
		return -1;
	}

    ::printf("Server start up successfully.\n");


	ChatServer()->Loop();
	ChatServer()->ShutDown();

    return 0;
}
