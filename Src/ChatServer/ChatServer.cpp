#include "stdafx.h"
#include "ChatServer.h"

CChatServer::CChatServer(void)
	: CChatLoop()
	, CServer()
	, m_hChatThread(NULL)
	, m_bContinue(false)
{
}

CChatServer::~CChatServer(void)
{
}

DWORD WINAPI ChatThreadCaller(void* pContext)
{
	CChatServer* pThis = (CChatServer*)pContext;
	pThis->ChatThread();
	return 0;
}

int CChatServer::StartUp(const ST_SERVER_INFO& info)
{
	m_bContinue = true;
	GlobalTimer()->SetTime(0);
	m_hChatThread = CreateThread(nullptr, 0, ChatThreadCaller, this, 0, nullptr);
	return CServer::StartUp(info);
}

void CChatServer::ShutDown(void)
{
	m_bContinue = false;
	CServer::ShutDown();
	::TerminateThread(m_hChatThread, 0);
	::CloseHandle(m_hChatThread);
}

void CChatServer::Loop(void)
{
	CServer::Loop();
}

bool CChatServer::OnFrontLoop(DWORD dwGameTime)
{
	CPlayerObject* pPlayer = NULL;
	while (ERROR_SUCCESS == m_AddPlayerQueue.Pop(pPlayer))
	{
		Objs()->AddPlayer(pPlayer);

		CMD_CHAT_ADD_PLAYER cmd;
		cmd.objectData = *static_cast<ST_OBJECT_DATA*>(pPlayer);
		cmd.playerData = *static_cast<ST_PLAYER_DATA*>(pPlayer);
		Broadcast(cmd);
	}

	while (ERROR_SUCCESS == m_RemovePlayerQueue.Pop(pPlayer))
	{
		CMD_CHAT_REMOVE_PLAYER cmd;
		cmd.nUserID = pPlayer->GetPlayerID();
		Objs()->RemovePlayer(pPlayer->GetPlayerID());
		Broadcast(cmd);
	}

	{


		REQ_CHAT chat;
		while (ERROR_SUCCESS == m_ChatQueue.Pop(chat))
		{
			CMD_CHAT cmdChat;

			printf("%d : %s\n", chat.nUserID, chat.str);
			cmdChat.nUserID = chat.nUserID;
			strncpy(cmdChat.str, chat.str, strlen(chat.str)+1);
			Broadcast(cmdChat);
		}

	}
	return true;
}

bool CChatServer::OnBackLoop(DWORD dwGameTime)
{
	return true;
}

void CChatServer::ChatThread(void)
{
	CChatLoop::Loop(true);
}
