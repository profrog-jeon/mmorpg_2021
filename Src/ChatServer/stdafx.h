#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include "../NetworkFramework/NetworkFramework.h"
#include "../ProtocolFramework/ProtocolFramework.h"
#include "../GameFramework/GameFramework.h"
#include "../Server/DB.h"