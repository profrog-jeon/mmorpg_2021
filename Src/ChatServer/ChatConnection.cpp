#include "stdafx.h"
#include "ChatConnection.h"
#include "ChatServer.h"

CChatConnection::CChatConnection(void)
	: m_pPlayer(NULL)
{
}

CChatConnection::~CChatConnection(void)
{
}

void CChatConnection::OnPacketRecved(const PCK_HEADER& header, void* pBody)
{
	//printf("Recv packet id :  %d\n", header.ID);


	int nRet = 0;
	if (header.ID == REQ_CHAT_INIT::ID)
	{
		REQ_CHAT_INIT* pReq = (REQ_CHAT_INIT*)pBody;
		CMD_CHAT_INIT cmd;

		CMD_INITIAL_INFO cmd2;
		cmd.bSucceeded = DB()->GetPlayerData(pReq->szUserID, pReq->szPassword, cmd2.user, cmd2.pos);
		nRet = SendPacket(this, cmd);

		if (!cmd.bSucceeded)
			return;

		{
			m_pPlayer = new CPlayerObject(cmd2.user);
			ChatServer()->m_AddPlayerQueue.Push(m_pPlayer);
		}

		printf("%s(id:%d) logged in to chat server.\n"
			, pReq->szUserID, cmd2.user.m_nPlayerID);

		m_strName = pReq->szUserID;


	}
	if (header.ID == REQ_CHAT::ID)
	{
		REQ_CHAT* pReq = (REQ_CHAT*)pBody;
		ChatServer()->m_ChatQueue.Push(*pReq);
	}
}

void CChatConnection::OnClose(void)
{
	ChatServer()->m_RemovePlayerQueue.Push(m_pPlayer);
	printf("%s logged out\n", m_strName.c_str());
	m_pPlayer = NULL;
}
