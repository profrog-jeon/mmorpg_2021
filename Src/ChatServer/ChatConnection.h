#pragma once

#include "../NetworkFramework/NetworkFramework.h"

class CChatConnection : public CConnectionSocket
{
	CPlayerObject* m_pPlayer;
	std::string m_strName;

public:
	CChatConnection(void);
	~CChatConnection(void);

private:
	void OnPacketRecved(const PCK_HEADER& header, void* pBody);
	void OnClose(void);
};

