#pragma once

class CUserInfoDialog : public CDialogSuper
{
	std::vector<std::string> m_vecMessage;
	int m_nCurSel;

public:
	CUserInfoDialog(void);
	~CUserInfoDialog(void);

	void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	void Draw(CRendererSuper* pRenderer);
};

