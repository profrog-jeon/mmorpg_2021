#include "stdafx.h"
#include "MenuDialog.h"
#include "QuestListDialog.h"
#include "PlayerListDialog.h"
#include "UserInfoDialog.h"

CMenuDialog::CMenuDialog()
	: CDialogSuper(100, 200, g_nPixelCountX - 100, 220)
{
	index = 0;

	m_vecMenus.push_back("캐릭터 상태정보");
	m_vecMenus.push_back("퀘스트 진행정보");
	m_vecMenus.push_back("접속중인 유저정보");
	m_vecMenus.push_back("게임 종료");

	m_nBottom += m_vecMenus.size() * 20;
}

CMenuDialog::~CMenuDialog(void)
{
}

void CMenuDialog::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
	for (int i = 0; i < nArrCount; i++)
	{
		if (pInputArr[i].bDown && KEY_ID_ESC == pInputArr[i].wKey)
			Close();

		if (pInputArr[i].bDown && KEY_ID_SPACE == pInputArr[i].wKey
		||  pInputArr[i].bDown && KEY_ID_ENTER == pInputArr[i].wKey)
			Select();

		if (pInputArr[i].bDown && KEY_ID_UP == pInputArr[i].wKey) {
			if (index != 0)
				index--;
		}

		if (pInputArr[i].bDown && KEY_ID_DOWN == pInputArr[i].wKey) {
			if (index < (int)m_vecMenus.size() - 1)
				index++;
		}
	}
}

void CMenuDialog::Draw(CRendererSuper* pRenderer)
{
	CDialogSuper::Draw(pRenderer);

	if (m_vecMenus.empty())
		return;

	const int nWindowWidth = 300;
	const int nWindowHeight = 80;

	const size_t numOfMenus = m_vecMenus.size();
	const size_t heightOfText = 20;
	const size_t topMargin = 10;

	for (int i = 0; i < numOfMenus; i++) {
		std::string strMessage = m_vecMenus[i];

		if (i == index)
			pRenderer->Rectangle(
				m_nLeft + nWindowWidth / 6,
				m_nTop + topMargin + heightOfText * i,
				m_nLeft + nWindowWidth / 6 * 5,
				m_nTop + topMargin + heightOfText * (i+1),
				RGB(255, 255, 255), ' ');

		pRenderer->TextOutW(g_nPixelCountX / 2,
			m_nTop + topMargin + heightOfText * i,
			strMessage);
	}
}

void CMenuDialog::Select()
{
	switch (index)
	{
	case 0:
		DialogContainer()->Dialogs.push_back(new CUserInfoDialog());
		break;
	case 1:
		DialogContainer()->Dialogs.push_back(new CQuestListDialog());
		break;
	case 2:
		DialogContainer()->Dialogs.push_back(new CPlayerListDialog());
		break;
	case 3:
		//ExitProcess(0);
		GameLoopFlag = false;
		break;
	}
}
