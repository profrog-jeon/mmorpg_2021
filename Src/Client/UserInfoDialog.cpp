#include "stdafx.h"
#include "UserInfoDialog.h"

CUserInfoDialog::CUserInfoDialog(void)
: CDialogSuper(10, 110, g_nPixelCountX - 10, g_nPixelCountY - 110)
, m_vecMessage()
, m_nCurSel(0)
{
	CPlayerObject* pPlayer = Objs()->GetMyPlayer();

	m_vecMessage.push_back("[�������ͽ�]");
	m_vecMessage.push_back(Format("����: %d", (int)pPlayer->m_nLevel));
	m_vecMessage.push_back(Format("�̵��ӵ�: %d", (int)pPlayer->m_Speed));
	m_vecMessage.push_back(Format("���ݷ�: %d", (int)pPlayer->m_Power));


	int nQuestClearCount = QuestContainer()->GetClearCount(pPlayer);

	m_vecMessage.push_back("");
	m_vecMessage.push_back("[����Ʈ ����]");
	m_vecMessage.push_back(Format("�Ϸ��: %d", nQuestClearCount));

	m_vecMessage.push_back("");
	m_vecMessage.push_back("[ȹ���� Ư������]");

	const size_t tLastCount = m_vecMessage.size();
	for (int i = 0; i < 8; i++)
	{
		const unsigned char cMask = 0x01 << (7 - i);
		const char* pszItemName = GetPartsName(cMask);
		if (nullptr == pszItemName)
			continue;

		if(cMask & pPlayer->m_SpecialParts)
			m_vecMessage.push_back(pszItemName);
	}

	if(tLastCount == m_vecMessage.size())
		m_vecMessage.push_back("** �ƹ��͵� ���� **");
}

CUserInfoDialog::~CUserInfoDialog(void)
{
}

const int g_nMaxLine = 13;

void CUserInfoDialog::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
	for (int i = 0; i < nArrCount; i++)
	{
		if (pInputArr[i].bDown && KEY_ID_SPACE == pInputArr[i].wKey
			|| pInputArr[i].bDown && KEY_ID_ENTER == pInputArr[i].wKey
			|| pInputArr[i].bDown && KEY_ID_ESC == pInputArr[i].wKey)
			Close();

		if (pInputArr[i].bDown && KEY_ID_UP == pInputArr[i].wKey)
			m_nCurSel -= g_nMaxLine;

		if (pInputArr[i].bDown && KEY_ID_DOWN == pInputArr[i].wKey)
			m_nCurSel += g_nMaxLine;

		m_nCurSel = std::min<int>(m_nCurSel, (int)m_vecMessage.size() - g_nMaxLine);
		m_nCurSel = std::max<int>(0, m_nCurSel);
	}
}

void CUserInfoDialog::Draw(CRendererSuper* pRenderer)
{
	CDialogSuper::Draw(pRenderer);

	for (int i = 0; i < g_nMaxLine && (i + m_nCurSel) < m_vecMessage.size(); i++)
		pRenderer->TextOut((m_nLeft + m_nRight) * 0.5, m_nTop + 10 + i * 20, m_vecMessage[i + m_nCurSel]);
}
