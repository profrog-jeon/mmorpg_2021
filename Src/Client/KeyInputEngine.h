#pragma once
#include "..\ProtocolFramework\PacketReq.h"
#include <map>

class CKeyInputEngine
{
	enum E_KEY_EVENT
	{
		KEY_EVENT_NONE = 0,
		KEY_EVENT_DOWN,
		KEY_EVENT_UP,
	};

	std::map<int, short> m_mapKey;
	HWND tempHwnd;

	CKeyInputEngine(void);
	~CKeyInputEngine(void);

	E_KEY_EVENT GetKeyEvent(int nVirtKey);

public:
	boolean isChatted();
	boolean isFocused();
	int Query(ST_INPUT_INFO* outInputArr);

	static CKeyInputEngine* GetInstance(void)
	{
		static CKeyInputEngine instance;
		return &instance;
	}
};

inline CKeyInputEngine* KeyInput(void)
{
	return CKeyInputEngine::GetInstance();
}