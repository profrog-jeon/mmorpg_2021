#include "stdafx.h"
#include "GDIClass.h"
#include "GDIHelper.h"
#include "../GameFramework/Renderer.h"




CGDIRenderer::CGDIRenderer()
{
}

CGDIRenderer::~CGDIRenderer()
{
}

void CGDIRenderer::MessageloopThread()
{
	{
		WNDCLASS WndClass;
		HINSTANCE hInstance = GetModuleHandle(NULL);
		WndClass.cbClsExtra = 0;
		WndClass.cbWndExtra = 0;
		WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		WndClass.hInstance = hInstance;
		WndClass.lpfnWndProc = (WNDPROC)WndProc;
		WndClass.lpszClassName = L"BOB MMORPG";
		WndClass.lpszMenuName = NULL;
		WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		RegisterClass(&WndClass);

		m_hWnd = CreateWindow(L"BOB MMORPG", L"BOB MMORPG", WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT, g_nPixelCountX + 5, g_nPixelCountY + g_nDialogCountY + 5,
			NULL, (HMENU)NULL, hInstance, NULL);
		mh_chat_log = CreateWindow(L"EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | ES_MULTILINE | ES_AUTOVSCROLL | ES_READONLY,
			10, 510, 470, 200, m_hWnd, (HMENU)ID_EDIT_CHAT_LOG, NULL, NULL);
		mh_chat_input = CreateWindow(L"EDIT", NULL, WS_CHILD | WS_VISIBLE | WS_BORDER ,
			10, 710, 470, 50, m_hWnd, (HMENU)ID_EDIT_CHAT_INPUT, NULL, NULL);
		ShowWindow(m_hWnd, 5);
	}

	MSG msg;

	while (GetMessage(&msg, m_hWnd,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

DWORD WINAPI MessageloopThreadCaller(void* pContext)
{
	CGDIRenderer* pThis = (CGDIRenderer*)pContext;
	pThis->MessageloopThread();
	return 0;
}

void CGDIRenderer::GDICreat()
{
	m_hThread = CreateThread(NULL, 0, MessageloopThreadCaller, this, 0, NULL);
}

BOOL CGDIRenderer::Render(void)
{
	SendMessage(m_hWnd, MSG_RENDER, 0, 0);
	return 0;
}

static ST_MEM_DC g_HandlerMemDC;

LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_CREATE:
	{
		g_HandlerMemDC.Create(hWnd, SIZE{ g_nPixelCountX, g_nPixelCountY + g_nDialogCountY });
		return TRUE;
	}
	case MSG_RENDER:
	{
		InvalidateRect(hWnd, NULL, FALSE);
		return TRUE;
	}
	case WM_PAINT:
	{
		g_HandlerMemDC.Clear();

		ST_VIEWPORT viewport;
		Camera()->QueryViewport(viewport);

		{
			ST_PEN UsePen(g_HandlerMemDC.hDC, RGB(150, 150, 150));
			ST_BRUSH UseBrush(g_HandlerMemDC.hDC, RGB(150, 150, 150));

			Rectangle(g_HandlerMemDC.hDC, 0, g_nPixelCountY, g_nPixelCountX, g_nPixelCountY + g_nDialogCountY);
		}

		for (CBillboardSuper* pBillboard : Objs()->BackBillboardList)
			pBillboard->DrawGDI(viewport, g_HandlerMemDC.hDC);

		for (CObjectSuper* pObject : Objs()->ObjectList)
			pObject->DrawGDI(viewport, g_HandlerMemDC.hDC);

		for (CBillboardSuper* pBillboard : Objs()->FrontBillboardList)
			pBillboard->DrawGDI(viewport, g_HandlerMemDC.hDC);

		//DrawDialogRect();

		{
			ST_PAINT_DC PaintDC(hWnd);
			g_HandlerMemDC.Flush(PaintDC.hDC, { 0, 0 });
		}
		return TRUE;
	}
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return TRUE;
	}
	}
	return(DefWindowProc(hWnd, iMessage, wParam, lParam));
}