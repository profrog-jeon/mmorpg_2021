#include "stdafx.h"
#include "DialogSuper.h"
#include "GDIHelper.h"

CDialogSuper::CDialogSuper(int left, int top, int right, int bottom, COLORREF bg)
    : m_BgColor(bg)
    , m_bClosed(false)
    , m_nLeft(left)
    , m_nRight(right)
    , m_nTop(top)
    , m_nBottom(bottom)
{
}

CDialogSuper::~CDialogSuper(void)
{
}

void CDialogSuper::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
}

void CDialogSuper::Draw(CRendererSuper* pRenderer)
{
    pRenderer->Rectangle(m_nLeft, m_nTop, m_nRight, m_nBottom, m_BgColor, '.');
}

void CDialogSuper::Close(void)
{
    m_bClosed = true;
}

bool CDialogSuper::IsClosed(void)
{
    return m_bClosed;
}
