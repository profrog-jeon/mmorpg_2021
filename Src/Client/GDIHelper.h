#pragma once

struct ST_MEM_DC
{
	HWND hWnd;
	HDC hDC;

private:
	HBITMAP m_hBitmap;
	HBITMAP m_hOldBitmap;
	SIZE m_Size;

public:
	ST_MEM_DC(void);
	~ST_MEM_DC(void);

	void Create(HWND hWnd, SIZE szWindow);
	void Create(HWND hWnd, HDC hDefaultDC, SIZE szWindow);
	void Delete(void);

	void Clear(void);
	void Flush(HDC hDC, POINT ptOffset = { 0, 0 });
};

struct ST_PAINT_DC
{
	HWND hWnd;
	HDC hDC;
	PAINTSTRUCT stPS;

	ST_PAINT_DC(HWND hInWnd)
		: hWnd(hInWnd)
		, hDC(NULL)
		, stPS()
	{
		hDC = BeginPaint(hWnd, &stPS);
	}

	~ST_PAINT_DC(void)
	{
		EndPaint(hWnd, &stPS);
	}
};

struct ST_CDC
{
	HWND hWnd;
	HDC hDC;
	PAINTSTRUCT stPS;

	ST_CDC(HWND hInWnd)
		: hWnd(hInWnd)
		, hDC(NULL)
		, stPS()
	{
		hDC = ::GetDC(hWnd);
	}

	~ST_CDC(void)
	{
		::ReleaseDC(hWnd, hDC);
	}
};


struct ST_PEN
{
private:
	HDC m_hDC;
	HPEN m_hPen;
	HPEN m_hOldPen;

public:
	ST_PEN(HDC dc, COLORREF color, int iStyle = PS_SOLID, int cWidth = 1)
		: m_hDC(dc)
		, m_hPen(NULL)
		, m_hOldPen(NULL)
	{
		m_hPen = CreatePen(iStyle, cWidth, color);
		m_hOldPen = (HPEN)SelectObject(m_hDC, m_hPen);
	}

	~ST_PEN(void)
	{
		SelectObject(m_hDC, m_hOldPen);
		DeleteObject(m_hPen);
	}
};

struct ST_BRUSH
{
private:
	HDC m_hDC;
	HBRUSH m_hBrush;
	HBRUSH m_hOldBrush;

public:
	ST_BRUSH(HDC dc, COLORREF color)
		: m_hDC(dc)
		, m_hBrush(NULL)
		, m_hOldBrush(NULL)
	{
		m_hBrush = CreateSolidBrush(color);
		m_hOldBrush = (HBRUSH)SelectObject(m_hDC, m_hBrush);
	}

	~ST_BRUSH(void)
	{
		SelectObject(m_hDC, m_hOldBrush);
		DeleteObject(m_hBrush);
	}
};

struct ST_STOCK_BRUSH
{
private:
	HDC m_hDC;
	HBRUSH m_hBrush;
	HBRUSH m_hOldBrush;

public:
	ST_STOCK_BRUSH(HDC dc, int nStockIndex)
		: m_hDC(dc)
		, m_hOldBrush(NULL)
	{
		m_hBrush = (HBRUSH)GetStockObject(nStockIndex);
		m_hOldBrush = (HBRUSH)SelectObject(m_hDC, m_hBrush);
	}

	~ST_STOCK_BRUSH(void)
	{
		SelectObject(m_hDC, m_hOldBrush);
	}
};

std::wstring S2WS(const std::string& s);
std::string string_format(const std::string fmt, ...);