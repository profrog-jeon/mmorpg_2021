#pragma once

#include "../GameFramework/GameFramework.h"
#include "GDIRenderer.h"
#include "DialogSuper.h"

class CClientLoop : public CMainGameLoop
{
	friend DWORD WINAPI GamePacketRecvThreadCaller(void* pContext);
	friend DWORD WINAPI ChatPacketRecvThreadCaller(void* pContext);
	CClientSocket m_Game_Socket, m_Chat_Socket;
	HANDLE m_Game_hThread, m_Chat_hThread;
	bool m_bContinue;
	bool m_bInitialized;

	CGDIRenderer m_Renderer;
	CSafeQueue<CMD_INPUT> m_InputQueue;
	CSafeQueue<CMD_CHAT> m_ChatQueue;

	ST_PLAYER_DATA m_PlayerData;

public:
	CClientLoop(void);
	~CClientLoop(void);

	bool StartUp(int x, int y);
	void ShutDown(void);

private:
	bool OnProcessDialog(REQ_INPUT& req);
	bool OnFrontLoop(DWORD dwGameTime);
	void OnPacketRecved(const PCK_HEADER& header, void* pBody);

	void PacketRecvThread(boolean isGame);
};

