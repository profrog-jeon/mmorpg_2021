#include "stdafx.h"
#include "QuestInfoDialog.h"

CQuestInfoDialog::CQuestInfoDialog(int nQuestID)
	: CDialogSuper(10, 10, g_nPixelCountX - 10, g_nPixelCountY - 10)
	, m_vecMessage()
	, m_nCurSel(0)
{
	CQuestInfoSuper* pQuest = QuestContainer()->GetQuest(nQuestID);

	std::vector<ST_NPC_MESSAGE> vecNpcMessage;
	pQuest->QueryMessageHistroy(*Objs()->GetMyPlayer(), vecNpcMessage);

	for (const ST_NPC_MESSAGE& msg : vecNpcMessage)
	{
		std::string strMessage;
		if (0 == msg.nNpcID)
			strMessage = "��";
		else
		{
			ST_NPC_INFO* pNpc = QuestContainer()->GetNPC(msg.nNpcID);
			strMessage = pNpc == nullptr ? "NPC" : pNpc->szName;
		}
		strMessage += ": ";
		strMessage += msg.szMessage;
		m_vecMessage.push_back(strMessage);
	}
}

CQuestInfoDialog::~CQuestInfoDialog(void)
{
}

const int g_nMaxLine = 23;

void CQuestInfoDialog::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
	for (int i = 0; i < nArrCount; i++)
	{
		if (pInputArr[i].bDown && KEY_ID_SPACE == pInputArr[i].wKey
		|| pInputArr[i].bDown && KEY_ID_ENTER == pInputArr[i].wKey
		|| pInputArr[i].bDown && KEY_ID_ESC == pInputArr[i].wKey)
			Close();

		if (pInputArr[i].bDown && KEY_ID_UP == pInputArr[i].wKey)
			m_nCurSel -= g_nMaxLine;

		if (pInputArr[i].bDown && KEY_ID_DOWN == pInputArr[i].wKey)
			m_nCurSel += g_nMaxLine;

		m_nCurSel = std::min<int>(m_nCurSel, (int)m_vecMessage.size() - g_nMaxLine);
		m_nCurSel = std::max<int>(0, m_nCurSel);
	}
}

void CQuestInfoDialog::Draw(CRendererSuper* pRenderer)
{
	CDialogSuper::Draw(pRenderer);

	for (int i = 0; i < g_nMaxLine && (i + m_nCurSel) < m_vecMessage.size(); i++)
		pRenderer->TextOutW((m_nLeft + m_nRight) * 0.5, m_nTop + 10 + i * 20, m_vecMessage[i + m_nCurSel]);
}
