#pragma once

class CLevelUpDialog : public CDialogSuper
{
	std::vector<std::string> m_vecMessage;
	int m_nLevel;

public:
	CLevelUpDialog(int nLevel);
	~CLevelUpDialog(void);

	void SelectedParts(char cPartMask);

	void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	void Draw(CRendererSuper* pRenderer);

	static int CalcLevel(int nClearCount);

private:
	void ApplyLevelReward(int nLevel);
};


