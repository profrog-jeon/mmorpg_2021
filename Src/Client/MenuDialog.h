#pragma once

#include "DialogSuper.h"

class CMenuDialog : public CDialogSuper
{
	size_t index;
	std::vector<std::string> m_vecMenus;

public:
	CMenuDialog();
	~CMenuDialog(void);

	void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	void Draw(CRendererSuper* pRenderer);
	void Select();
};

