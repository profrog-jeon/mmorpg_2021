#include "stdafx.h"
#include "ConsoleRenderer.h"

void DrawOnVideoMemory(double x, double y, double w, double h, char cPatch, std::vector<std::string>& refVideoMemory);
void DrawOnVideoMemory(double x, double y, std::string strMessage, std::vector<std::string>& refVideoMemory);
void DrawOnVideoMemory(std::string strMessage, std::vector<std::string>& refVideoMemory);

CConsoleRenderer::CConsoleRenderer(void)
	: m_VideoMemory(),
	m_FrameQueue()
{
	m_VideoMemory.resize(g_nPixelCountY);

	for (std::string& strLine : m_VideoMemory)
		strLine.resize(g_nPixelCountX);

	{
		// 콘솔 커서 숨기기(윈도우 API)
		CONSOLE_CURSOR_INFO cursorInfo = { 0, };
		cursorInfo.dwSize = 1;
		cursorInfo.bVisible = FALSE;
		SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursorInfo);
	}
	
	{
		// ANSI Escape sequence 지원 모드
		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		DWORD dwMode = 0;
		GetConsoleMode(hOut, &dwMode);
		dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
		SetConsoleMode(hOut, dwMode);
	}
}

CConsoleRenderer::~CConsoleRenderer(void)
{
}

void CConsoleRenderer::Clear(void)
{
	for (std::string& strLine : m_VideoMemory)
		memset((void*)strLine.c_str(), ' ', strLine.size());
}

void CConsoleRenderer::Flush(void)
{
	// ANSI Escape sequence
	// 참고: http://ascii-table.com/ansi-escape-sequences-vt-100.php
	printf("\x1b[H");
	for (std::string& strLine : m_VideoMemory)
		printf("%s\n", strLine.c_str());

	// fps 계산
	const DWORD dwCurrentTick = GetTickCount();
	while (0 < m_FrameQueue.size())
	{
		if (dwCurrentTick - m_FrameQueue.front() < 1000) break;
		m_FrameQueue.pop();
	}
	m_FrameQueue.push(dwCurrentTick);

	// fps 출력
	char szFpsBuffer[100] = { 0, };
	sprintf(szFpsBuffer, "FPS: %u", (DWORD)m_FrameQueue.size());
	DrawOnVideoMemory(szFpsBuffer, m_VideoMemory);
}

void CConsoleRenderer::Rectangle(int left, int top, int right, int bottom, COLORREF color, char patch)
{
	//nLeft = std::max<int>(0, nLeft);
	//nTop = std::max<int>(0, nTop);
	//nRight = std::min<int>(g_nPixelCountX - 1, nRight);
	//nBottom = std::min<int>(g_nPixelCountY - 1, nBottom);

	//const size_t tRaw = refVideoMemory.size();

	//memset((void*)refVideoMemory[tRaw - 3].c_str(), '.', refVideoMemory[tRaw - 3].length());
	//memset((void*)refVideoMemory[tRaw - 2].c_str(), '.', refVideoMemory[tRaw - 2].length());
	//memset((void*)refVideoMemory[tRaw - 1].c_str(), '.', refVideoMemory[tRaw - 1].length());

	//if (m_queMessages.empty())
	//	return;

	//const std::string& strMessage = m_queMessages.front();
	//memcpy((void*)(refVideoMemory[tRaw - 2].c_str() + 3), strMessage.c_str(), strMessage.length());
}

void CConsoleRenderer::Ellipse(int left, int top, int right, int bottom, COLORREF color, char patch)
{
}

void CConsoleRenderer::TextOut(int left, int top, std::string text)
{

	bool setBackground = false;

	// ANSI Escape sequence
	// 참고: http://ascii-table.com/ansi-escape-sequences-vt-100.php
	printf("\x1b[H");

	for (std::string& strLine : m_VideoMemory) {
		if (strLine[0] == MARK_WHITE) {   // background를 위한 extended ASCII
			setBackground = true;
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x70);
			printf("%s\n", strLine.c_str() + 1);
			continue;
		}
		
		if (setBackground) {
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x07);
			setBackground = false;
		}

		printf("%s\n", strLine.c_str());
	}
}

void DrawOnVideoMemory(double x, double y, double w, double h, char cPatch, std::vector<std::string>& refVideoMemory)
{
	int nLeft = (int)(x - w / 2) + (g_nPixelCountX / 2);
	int nRight = (int)(x + w / 2) + (g_nPixelCountX / 2);
	int nTop = -(int)(y + h / 2) + (g_nPixelCountY / 2);
	int nBottom = -(int)(y - h / 2) + (g_nPixelCountY / 2);

	if (nRight < 0)
		return;
	if (g_nPixelCountX <= nLeft)
		return;
	if (nBottom < 0)
		return;
	if (g_nPixelCountY <= nTop)
		return;

	nLeft = std::max<int>(0, nLeft);
	nTop = std::max<int>(0, nTop);
	nRight = std::min<int>(g_nPixelCountX - 1, nRight);
	nBottom = std::min<int>(g_nPixelCountY - 1, nBottom);

	for (int y = nTop; y <= nBottom; y++)
	{
		char* pLine = (char*)refVideoMemory[y].c_str();

		for (int x = nLeft; x <= nRight; x++)
			pLine[x] = cPatch;
	}
}

void DrawOnVideoMemory(double x, double y, std::string strMessage, std::vector<std::string>& refVideoMemory)
{
	int nLeft = (int)(x - strMessage.length() / 2) + (g_nPixelCountX / 2);
	int nRight = (int)(x + strMessage.length() / 2) + (g_nPixelCountX / 2);
	int nTop = -(int)(y - 1) + (g_nPixelCountY / 2);

	if (nLeft < 0)
		return;
	if (g_nPixelCountX <= nRight)
		return;
	if (nTop < 0)
		return;
	if (g_nPixelCountY <= nTop)
		return;

	memcpy((void*)(refVideoMemory[nTop].c_str() + nLeft), strMessage.c_str(), strMessage.size());
}

void DrawOnVideoMemory(std::string strMessage, std::vector<std::string>& refVideoMemory)
{
	double x = 60;
	int nLeft = (int)(x - strMessage.length() / 2);
	int nRight = (int)(x + strMessage.length() / 2);
	int nTop = 0;

	if (nLeft < 0)
		return;
	if (g_nPixelCountX <= nRight)
		return;
	if (nTop < 0)
		return;
	if (g_nPixelCountY <= nTop)
		return;

	memcpy((void*)(refVideoMemory[nTop].c_str() + nLeft), strMessage.c_str(), strMessage.size());
}