#pragma once

#include "../GameFramework/RendererSuper.h"

class CDialogSuper
{
	COLORREF m_BgColor;
	bool m_bClosed;

protected:
	int m_nLeft;
	int m_nRight;
	int m_nTop;
	int m_nBottom;

public:
	CDialogSuper(int left, int top, int right, int bottom, COLORREF bg = RGB(244, 244, 244));
	virtual ~CDialogSuper(void);

	virtual void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	virtual void Draw(CRendererSuper* pRenderer);

	void Close(void);
	bool IsClosed(void);
};

