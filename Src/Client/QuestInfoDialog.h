#pragma once

#include "DialogSuper.h"

class CQuestInfoDialog : public CDialogSuper
{
	std::vector<std::string> m_vecMessage;
	int m_nCurSel;

public:
	CQuestInfoDialog(int nQuestID);
	~CQuestInfoDialog(void);

	void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	void Draw(CRendererSuper* pRenderer);
};

