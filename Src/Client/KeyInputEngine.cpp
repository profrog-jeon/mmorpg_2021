#include "stdafx.h"
#include "KeyInputEngine.h"

CKeyInputEngine::CKeyInputEngine(void)
	: m_mapKey()
{
	tempHwnd = ::FindWindow(NULL, NULL);
	
	while (tempHwnd != NULL)
	{
		if (::GetParent(tempHwnd) == NULL) {
			DWORD pid;
			::GetWindowThreadProcessId(tempHwnd, &pid);
			if (::GetCurrentProcessId() == pid) {
				return;
			}
		}
		tempHwnd = ::GetWindow(tempHwnd, GW_HWNDNEXT);
	}
}

CKeyInputEngine::~CKeyInputEngine(void)
{
}

CKeyInputEngine::E_KEY_EVENT CKeyInputEngine::GetKeyEvent(int nVirtKey)
{
	const short nPreState = m_mapKey[nVirtKey];
	const short nCurState = GetAsyncKeyState(nVirtKey);
	const bool bIsPressed = nPreState & 0x8000;
	const bool bIsPressing = nCurState & 0x8000;

	m_mapKey[nVirtKey] = nCurState;

	if (bIsPressed && !bIsPressing)
		return KEY_EVENT_UP;

	if (!bIsPressed && bIsPressing)
		return KEY_EVENT_DOWN;

	return KEY_EVENT_NONE;
}


boolean CKeyInputEngine::isFocused() {
	
	return (::GetForegroundWindow() == tempHwnd);
}


boolean CKeyInputEngine::isChatted()
{
	const E_KEY_EVENT nEnterKey = GetKeyEvent(VK_RETURN);

	return (nEnterKey == KEY_EVENT_DOWN) && CKeyInputEngine::isFocused();
}


int CKeyInputEngine::Query(ST_INPUT_INFO* outInputArr)
{
#ifdef SERVER_MODE
	if (!CKeyInputEngine::isFocused())
		return 0;
#endif

	int nCount = 0;

	const E_KEY_EVENT nSpaceKey = GetKeyEvent(' ');
	const E_KEY_EVENT nReturnKey = GetKeyEvent(VK_RETURN);
	const E_KEY_EVENT nEscKey = GetKeyEvent(VK_ESCAPE);
	const E_KEY_EVENT nLeftKey = GetKeyEvent(VK_LEFT);
	const E_KEY_EVENT nRightKey = GetKeyEvent(VK_RIGHT);
	const E_KEY_EVENT nUpKey = GetKeyEvent(VK_UP);
	const E_KEY_EVENT nDownKey = GetKeyEvent(VK_DOWN);

	if (KEY_EVENT_DOWN == nLeftKey)		outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_LEFT);
	if (KEY_EVENT_UP == nLeftKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_LEFT);
	if (KEY_EVENT_DOWN == nRightKey)	outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_RIGHT);
	if (KEY_EVENT_UP == nRightKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_RIGHT);
	if (KEY_EVENT_DOWN == nUpKey)		outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_UP);
	if (KEY_EVENT_UP == nUpKey)			outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_UP);
	if (KEY_EVENT_DOWN == nDownKey)		outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_DOWN);
	if (KEY_EVENT_UP == nDownKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_DOWN);
	if (KEY_EVENT_DOWN == nSpaceKey)	outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_SPACE);
	if (KEY_EVENT_UP == nSpaceKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_SPACE);
	if (KEY_EVENT_DOWN == nEscKey)		outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_ESC);
	if (KEY_EVENT_UP == nEscKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_ESC);
	if (KEY_EVENT_DOWN == nReturnKey)	outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_ENTER);
	if (KEY_EVENT_UP == nReturnKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_ENTER);

#ifdef SERVER_MODE
	if (!CKeyInputEngine::isFocused())
		return 0;
#endif

	return nCount;
}
