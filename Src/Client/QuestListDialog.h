#pragma once

#include "DialogSuper.h"

class CQuestListDialog : public CDialogSuper
{
	size_t index;

	struct ST_QUEST_LIST_ITEM
	{
		int nQuestID;
		std::string strLastMessage;
	};

	std::vector<ST_QUEST_LIST_ITEM> m_vecQuests;

public:
	CQuestListDialog(void);
	~CQuestListDialog(void);

	void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	void Draw(CRendererSuper* pRenderer);
};

