#pragma once

class CConsoleRenderer : public CRendererSuper
{
	friend class CCamera;

	std::vector<std::string> m_VideoMemory;
	std::queue<DWORD> m_FrameQueue;

public:
	CConsoleRenderer(void);
	~CConsoleRenderer(void);

private:
	void Clear(void);
	void Flush(void);

	void Rectangle(int left, int top, int right, int bottom, COLORREF color, char patch);
	void Ellipse(int left, int top, int right, int bottom, COLORREF color, char patch);
	void TextOut(int left, int top, std::string text);
};
