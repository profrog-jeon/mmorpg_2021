#include "stdafx.h"
#include "QuestListDialog.h"
#include "QuestInfoDialog.h"

CQuestListDialog::CQuestListDialog()
	: CDialogSuper(50, 50, g_nPixelCountX - 50, g_nPixelCountY - 50, RGB(231, 181, 111))
	, index(0)
	, m_vecQuests()
{
	CPlayerObject* pPlayer = Objs()->GetMyPlayer();

	std::vector<CQuestInfoSuper*> vecQuestInfo;
	QuestContainer()->QueryQuests(pPlayer, vecQuestInfo);

	for (CQuestInfoSuper* pQuestInfo : vecQuestInfo)
	{
		ST_NPC_MESSAGE stLastMessage;
		if (!pQuestInfo->PeekLastMessage(*pPlayer, stLastMessage))
			continue;

		ST_QUEST_LIST_ITEM stItem;
		stItem.nQuestID = pQuestInfo->GetID();
		stItem.strLastMessage = stLastMessage.szMessage;
		m_vecQuests.push_back(stItem);
	}
}

CQuestListDialog::~CQuestListDialog(void)
{
}

void CQuestListDialog::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
	for (int i = 0; i < nArrCount; i++)
	{
		if (pInputArr[i].bDown && KEY_ID_ESC == pInputArr[i].wKey)
			Close();

		if (pInputArr[i].bDown && KEY_ID_SPACE == pInputArr[i].wKey
		|| pInputArr[i].bDown && KEY_ID_ENTER == pInputArr[i].wKey)
		{
			if (index < m_vecQuests.size())
			{
				CQuestInfoDialog* pInfoDialog = new CQuestInfoDialog(m_vecQuests[index].nQuestID);
				DialogContainer()->Dialogs.push_back(pInfoDialog);
			}
		}

		if (pInputArr[i].bDown && KEY_ID_UP == pInputArr[i].wKey) {
			if (index != 0)
				index--;
		}

		if (pInputArr[i].bDown && KEY_ID_DOWN == pInputArr[i].wKey) {
			if (index < (int)m_vecQuests.size() - 1)
				index++;
		}
	}
}

void CQuestListDialog::Draw(CRendererSuper* pRenderer)
{
	CDialogSuper::Draw(pRenderer);

	const size_t numOfMenus = 4;
	const size_t heightOfText = 20;
	const size_t topMargin = 150, betweenMargin = 10;

	int start = index - index % numOfMenus;

	std::string title = "����Ʈ ���";

	pRenderer->TextOutW(g_nPixelCountX / 2, m_nTop + heightOfText, title);
	
	for (int i = 0; i < numOfMenus; i++) {
		const int nItemIndex = i + start;
		if (m_vecQuests.size() <= nItemIndex)
			continue;

		const ST_QUEST_LIST_ITEM& stItem = m_vecQuests[nItemIndex];

		const ST_NPC_INFO* pNpc = QuestContainer()->GetNPC(stItem.nQuestID);
		if (nullptr == pNpc)
			continue;

		std::string npcName = pNpc->szName;

		if (i == index % numOfMenus)
			pRenderer->Rectangle(g_nPixelCountX / 6,
				m_nTop + topMargin + i * betweenMargin - 2 * heightOfText * (numOfMenus / 2 - i - 1) - 2,
				g_nPixelCountX / 6 * 5,
				m_nTop + topMargin + i * betweenMargin - 2 * heightOfText * (numOfMenus / 2 - i - 2) - 2,
				RGB(255,255,255), ' ');

		pRenderer->TextOutW(g_nPixelCountX / 2,
			m_nTop + topMargin + i * betweenMargin - heightOfText * (numOfMenus / 2 - 2 * i),
			npcName);

		pRenderer->TextOutW(g_nPixelCountX / 2,
			m_nTop + topMargin + i * betweenMargin - heightOfText * (numOfMenus / 2 - 2 * i - 1),
			"\"" + stItem.strLastMessage + "\"");
	}
}
