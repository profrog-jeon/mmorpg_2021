#include "stdafx.h"
#include "GDIRenderer.h"
#include "GDIHelper.h"

CGDIRenderer::CGDIRenderer()
{
	HWND hWndConsole = GetConsoleWindow();
	ShowWindow(hWndConsole, SW_SHOW);
}

CGDIRenderer::~CGDIRenderer()
{
}

void CGDIRenderer::MessageloopThread()
{
	{
		WNDCLASS WndClass;
		HINSTANCE hInstance = GetModuleHandle(NULL);
		WndClass.cbClsExtra = 0;
		WndClass.cbWndExtra = 0;
		WndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
		WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
		WndClass.hInstance = hInstance;
		WndClass.lpfnWndProc = (WNDPROC)WndProc;
		WndClass.lpszClassName = L"BOB MMORPG";
		WndClass.lpszMenuName = NULL;
		WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		RegisterClass(&WndClass);

		m_hWnd = CreateWindow(L"BOB MMORPG", L"BOB MMORPG", WS_POPUP,
			CW_USEDEFAULT, CW_USEDEFAULT, g_nPixelCountX, g_nPixelCountY,
			NULL, (HMENU)NULL, hInstance, NULL);
		ShowWindow(m_hWnd, 5);
	}

	MSG msg;

	while (GetMessage(&msg, m_hWnd,0,0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

DWORD WINAPI MessageloopThreadCaller(void* pContext)
{
	CGDIRenderer* pThis = (CGDIRenderer*)pContext;
	pThis->MessageloopThread();
	return 0;
}

void CGDIRenderer::Create()
{
	//HWND hWndConsole = GetConsoleWindow();
	//ShowWindow(hWndConsole, SW_HIDE);

	m_hThread = CreateThread(NULL, 0, MessageloopThreadCaller, this, 0, NULL);
}

static ST_MEM_DC g_HandlerMemDC;
void CGDIRenderer::Clear(void)
{
	g_HandlerMemDC.Clear();
	{
		ST_PEN UsePen(g_HandlerMemDC.hDC, RGB(150, 150, 150));
		ST_BRUSH UseBrush(g_HandlerMemDC.hDC, RGB(150, 150, 150));

		::Rectangle(g_HandlerMemDC.hDC, 0, g_nPixelCountY, g_nPixelCountX, g_nPixelCountY);
	}
}

void CGDIRenderer::Flush(void)
{
	// ���̾�α� ȭ�� �׸���
	for(CDialogSuper* pDialog : DialogContainer()->Dialogs)
		pDialog->Draw(this);

	// FPS �׸���
	const DWORD dwCurrentTick = GetTickCount();
	m_FrameQueue.push(dwCurrentTick);
	while (0 < m_FrameQueue.size())
	{
		if (dwCurrentTick - m_FrameQueue.front() < 1000) break;
		m_FrameQueue.pop();
	}

	std::string fpsStatus = Format("FPS: %u", (DWORD)m_FrameQueue.size());
	this->TextOut(450, 20, fpsStatus);

	ST_CDC dc(g_HandlerMemDC.hWnd);
	g_HandlerMemDC.Flush(dc.hDC, { 0, 0 });
}


void CGDIRenderer::Rectangle(int left, int top, int right, int bottom, COLORREF color, char patch)
{
	ST_PEN UsePen(g_HandlerMemDC.hDC, RGB(0,0,0));
	ST_BRUSH UseBrush(g_HandlerMemDC.hDC, color);
	::Rectangle(g_HandlerMemDC.hDC, left, top, right, bottom);
}

void CGDIRenderer::Ellipse(int left, int top, int right, int bottom, COLORREF color, char patch)
{
	ST_PEN UsePen(g_HandlerMemDC.hDC, RGB(0, 0, 0));
	ST_BRUSH UseBrush(g_HandlerMemDC.hDC, color);
	::Ellipse(g_HandlerMemDC.hDC, left, top, right, bottom);
}

void CGDIRenderer::TextOut(int cx, int cy, std::string text)
{
	ST_PEN UsePen(g_HandlerMemDC.hDC, RGB(0,0,0));

	int nOldMode = SetBkMode(g_HandlerMemDC.hDC, TRANSPARENT);
	::SetTextAlign(g_HandlerMemDC.hDC, TA_CENTER);
	::TextOutA(g_HandlerMemDC.hDC, cx, cy, text.c_str(), text.length());
	SetBkMode(g_HandlerMemDC.hDC, nOldMode);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
	case WM_CREATE:
	{
		g_HandlerMemDC.Create(hWnd, SIZE{ g_nPixelCountX, g_nPixelCountY });
		return TRUE;
	}
	case WM_NCHITTEST:
	{
		LRESULT nHitTestResult = DefWindowProc(hWnd, iMessage, wParam, lParam);
		if (HTCLIENT == nHitTestResult)
			return HTCAPTION;
		return nHitTestResult;
	}
	case WM_PAINT:
	{
		return TRUE;
	}
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		GameLoopFlag = false;
		return TRUE;
	}
	}
	return(DefWindowProc(hWnd, iMessage, wParam, lParam));
}