#pragma once

#include <list>
#include "DialogSuper.h"

class CDialogContainer
{
	CDialogContainer(void);
	~CDialogContainer(void);

public:
	std::list<CDialogSuper*> Dialogs;

	static CDialogContainer* GetInstance(void)
	{
		static CDialogContainer instance;
		return &instance;
	}
};

inline CDialogContainer* DialogContainer(void)
{
	return CDialogContainer::GetInstance();
}
