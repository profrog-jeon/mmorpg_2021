#pragma once

#define MSG_RENDER WM_USER + 0x01
#define ID_EDIT_CHAT_LOG					100
#define ID_EDIT_CHAT_INPUT              1000


LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam);

class CGDIRenderer
{
	friend DWORD WINAPI MessageloopThreadCaller(void* pContext);
	
	HWND m_hWnd, mh_chat_log, mh_chat_input;

	CGDIRenderer();
	~CGDIRenderer();

	void MessageloopThread();

	HANDLE m_hThread;

public:
	void GDICreat();
	BOOL Render(void);

	HWND Get_m_hWnd() {
		return m_hWnd;
	}

	HWND Get_mh_chat_input() {
		return mh_chat_input;
	}

	HWND Get_mh_chat_log() {
		return mh_chat_log;
	}

	static CGDIRenderer* GetInstance(void)
	{
		static CGDIRenderer instance;
		return &instance;
	}
};

inline CGDIRenderer* GDICreater(void)
{
	return CGDIRenderer::GetInstance();
}