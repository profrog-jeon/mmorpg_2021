#include "stdafx.h"
#include "ClientLoop.h"
#include "GDIRenderer.h"
#include "MenuDialog.h"
#include "NpcChatDialog.h"
#include "KeyInputEngine.h"
#include <atlstr.h>

#include "resource.h"

CClientLoop::CClientLoop(void)
	: CMainGameLoop(&m_Renderer)
	, m_Game_Socket()
	, m_Chat_Socket()
	, m_Game_hThread(NULL)
	, m_Chat_hThread(NULL)
	, m_bContinue(false)
	, m_bInitialized(false)
{
}

CClientLoop::~CClientLoop(void)
{
}

DWORD WINAPI GamePacketRecvThreadCaller(void* pContext)
{
	CClientLoop* pThis = (CClientLoop*)pContext;
	pThis->PacketRecvThread(true);
	return 0;
}

DWORD WINAPI ChatPacketRecvThreadCaller(void* pContext)
{
	CClientLoop* pThis = (CClientLoop*)pContext;
	pThis->PacketRecvThread(false);
	return 0;
}


static char szID[g_nMaxUserIDLen + 1];
static char szPASSWD[g_nMaxPasswordLen + 1];


BOOL CALLBACK DialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
	{
		return 0;
	}
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDOK:
			HWND hEdit = GetDlgItem(hwndDlg, IDC_EDIT_ID);
			HWND hEdit2 = GetDlgItem(hwndDlg, IDC_EDIT2);
			GetWindowTextA(hEdit, szID, g_nMaxUserIDLen + 1);
			GetWindowTextA(hEdit2, szPASSWD, g_nMaxPasswordLen + 1);
			EndDialog(hwndDlg, IDOK);
			break;
		}
		return TRUE;
	}
	}
	return FALSE;
}


bool CClientLoop::StartUp(int x, int y)
{
	m_bInitialized = false;
#ifdef SERVER_MODE
	char szIP[100];
	REQ_LOGIN reqLogin;
	REQ_CHAT_INIT reqChatInit;
#ifdef SERVER_DEBUGGING
	//HWND hWndConsole = GetConsoleWindow();
	//ShowWindow(hWndConsole, SW_HIDE);
	int nAnswer = ::DialogBox(NULL, MAKEINTRESOURCE(IDD_DIALOG_LOGIN), HWND_DESKTOP, (DLGPROC)DialogProc);
	if (IDOK == nAnswer) {
		
		sprintf(reqLogin.szUserID, szID);
		sprintf(reqLogin.szPassword, szPASSWD);
		sprintf(reqChatInit.szUserID, szID);
		sprintf(reqChatInit.szPassword, szPASSWD);

	}
	else
	{
		return false;
	}


	sprintf(szIP, "127.0.0.1");

#else
	sprintf(szIP, "127.0.0.1");
	//printf("server ip: ");
	//scanf("%s", szIP);

	printf("user-id: ");
	scanf("%s", reqLogin.szUserID);

	printf("password: ");
	scanf("%s", reqLogin.szPassword);
#endif

	if (m_Game_Socket.Connect(szIP, GAME_SERVER_PORT) != 0)
	{
		printf("Connecting failure\n");
		return false;
	}
	if (SendPacket(&m_Game_Socket, reqLogin) <= 0)
	{
		printf("Sending Login failure\n");
		return false;
	}

	CMD_LOGIN cmdLogin;
	while(true)
	{
		if (RecvPacket(&m_Game_Socket, cmdLogin) > 0) {
			if (cmdLogin.ID == 0 && cmdLogin.bSucceeded) {
				printf("Receiving Login Success\n");
				break;;
			}
			if (!cmdLogin.bSucceeded)
			{
				printf("Login authentication failure\n");
			}
		}else
		{
			printf("Receiving Login failure\n");
		}
	}

	m_bContinue = true;
	m_Game_hThread = CreateThread(NULL, 0, GamePacketRecvThreadCaller, this, 0, NULL);
#else
	m_bInitialized = true;
	m_PlayerData.m_nPlayerID = 1;
	strcpy(m_PlayerData.m_szUserName, "Profrog");

	CPlayerObject* pPlayer = new CPlayerObject(m_PlayerData);
	pPlayer->m_PosX = x;
	pPlayer->m_PosY = y;
	Objs()->AddPlayer(pPlayer);
	Objs()->SetMyPlayer(pPlayer->GetPlayerID());

#endif

	GDICreater()->Create();
	return true;
}

void CClientLoop::ShutDown(void)
{
	m_bContinue = false;
	m_Game_Socket.Close();
	m_Chat_Socket.Close();

	if ((WAIT_OBJECT_0 == WaitForSingleObject(m_Game_hThread, 5000))
		&& (WAIT_OBJECT_0 == WaitForSingleObject(m_Chat_hThread, 5000)))
		printf("Gracefully socket closed.\n");
	else
	{
		printf("Waiting for socket timed out.\n");
		::TerminateThread(m_Game_hThread, 0);
		::TerminateThread(m_Chat_hThread, 0);
	}

	CloseHandle(m_Game_hThread);
	CloseHandle(m_Chat_hThread);
	m_Game_Socket.Close();
	m_Chat_Socket.Close();
}



CNpcObject* ChatToNpc(void)
{
	CPlayerObject* pPlayer = Objs()->GetMyPlayer();
	if (nullptr == pPlayer)
		return nullptr;

	const double dDirX = cos(pPlayer->m_Angle);
	const double dDirY = sin(pPlayer->m_Angle);

	CObjectSuper* pFrontObj = Objs()->FindObject(pPlayer->m_PosX + dDirX, pPlayer->m_PosY + dDirY, OBJECT_TYPE_NPC);
	if (nullptr == pFrontObj)
		return nullptr;

	if (OBJECT_TYPE_NPC != pFrontObj->m_Type)
		return nullptr;

	return dynamic_cast<CNpcObject*>(pFrontObj);
}

bool IsKeyPressed(const REQ_INPUT& input, E_KEY_ID nKeyID)
{
	for (int i = 0; i < input.nCount; i++)
	{
		if (input.input[i].bDown && nKeyID == input.input[i].wKey)
			return true;
	}

	return false;
}

bool CClientLoop::OnProcessDialog(REQ_INPUT& req)
{
	while (0 < DialogContainer()->Dialogs.size() && DialogContainer()->Dialogs.back()->IsClosed())
	{
		delete DialogContainer()->Dialogs.back();
		DialogContainer()->Dialogs.pop_back();
	}

	if (!DialogContainer()->Dialogs.empty())
	{
		DialogContainer()->Dialogs.back()->Update(req.input, req.nCount);
		req.nCount = 0;
		return true;
	}

	if (IsKeyPressed(req, KEY_ID_ESC))
	{
		DialogContainer()->Dialogs.push_back(new CMenuDialog());

		req.nCount = 0;
		req.input[req.nCount++] = ST_INPUT_INFO(false, KEY_ID_LEFT);
		req.input[req.nCount++] = ST_INPUT_INFO(false, KEY_ID_RIGHT);
		req.input[req.nCount++] = ST_INPUT_INFO(false, KEY_ID_UP);
		req.input[req.nCount++] = ST_INPUT_INFO(false, KEY_ID_DOWN);
		return true;
	}

	if (!IsKeyPressed(req, KEY_ID_SPACE))
		return false;

	CNpcObject* pNpc = ChatToNpc();
	if (nullptr == pNpc)
		return false;

	CPlayerObject* pPlayer = Objs()->GetMyPlayer();
	CQuestInfoSuper* pQuest = QuestContainer()->GetQuest(pNpc->GetID());
	if (pQuest)
	{
		std::vector<ST_NPC_MESSAGE> vecMessage;
		pQuest->Process(*dynamic_cast<ST_USER_QUESTINFO*>(pPlayer), vecMessage);
		DialogContainer()->Dialogs.push_back(new CNpcChatDialog(vecMessage));
	}
	else
	{
		// ������ ����Ʈ�� ������ �⺻ �λ�޽���
		std::vector<ST_NPC_MESSAGE> vecMessage;
		vecMessage.push_back(ST_NPC_MESSAGE(pNpc->GetID(), pNpc->GetGreetingMsg().c_str()));
		DialogContainer()->Dialogs.push_back(new CNpcChatDialog(vecMessage));
	}

	req.nCount = 0;
	req.input[req.nCount++] = ST_INPUT_INFO(false, KEY_ID_LEFT);
	req.input[req.nCount++] = ST_INPUT_INFO(false, KEY_ID_RIGHT);
	req.input[req.nCount++] = ST_INPUT_INFO(false, KEY_ID_UP);
	req.input[req.nCount++] = ST_INPUT_INFO(false, KEY_ID_DOWN);
	return true;
}
bool CClientLoop::OnFrontLoop(DWORD dwGameTime)
{
	try
	{
#ifdef SERVER_MODE
		if (!m_bInitialized)
			return true;

		REQ_INPUT req;
		req.nUserID = m_PlayerData.m_nPlayerID;
		req.nCount = KeyInput()->Query(req.input);

		CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();
		unsigned char cPreParts = 0;
		if (pMyPlayer)
			cPreParts = pMyPlayer->m_SpecialParts;

		OnProcessDialog(req);

		if (pMyPlayer && cPreParts != pMyPlayer->m_SpecialParts)
		{
			REQ_USERINFO_UPDATE req;
			req.nUserID = pMyPlayer->m_nPlayerID;
			req.cSpecialParts = pMyPlayer->m_SpecialParts;

			SendPacket(&m_Game_Socket, req);
		}

		if (0 < req.nCount)
		{
			if (SendPacket(&m_Game_Socket, req) <= 0)
				throw std::exception("Disconnected from game server.");
		}

		CMD_INPUT input;
		const int nMaxCount = 10000;
		int i;
		for (i = 0; i < nMaxCount && ERROR_SUCCESS != m_InputQueue.Pop(input); i++)
		{
			//printf("Waiting for server response...(%d/%d)\n", i+1, nMaxCount);
			Sleep(10);
		}
		if (i == nMaxCount)
		{
			Log("[ERROR] No response from game server.");
			return false;
		}

		GlobalTimer()->SetTime(input.dwGameTick);
		for (i = 0; i < input.nAggregateCount; i++)
		{
			Debug("%u\t%u\t%d\t%d", GlobalTimer()->GetTime(), input.dwGameTick, input.aggregate[i].nUserID, input.aggregate[i].nCount);
			Update(input.aggregate[i].nUserID, input.aggregate[i].input, input.aggregate[i].nCount);
		}
#else
		if (!m_bInitialized)
		return true;

		REQ_INPUT req;
		req.nUserID = m_PlayerData.m_nPlayerID;
		req.nCount = KeyInput()->Query(req.input);

		CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();
		unsigned char cPreParts = 0;
		if (pMyPlayer)
			cPreParts = pMyPlayer->m_SpecialParts;

		OnProcessDialog(req);
		
		Update(req.nUserID, req.input, req.nCount);
#endif
	}
	catch (std::exception& e)
	{
		printf("%s\n", e.what());
		m_Game_Socket.Close();
		m_Chat_Socket.Close();
		return false;
	}

	return true;
}

void CClientLoop::OnPacketRecved(const PCK_HEADER& header, void* pBody)
{
	//if (10 != header.ID)
	//printf("Recv packet id :  %d\n", header.ID);


	if (header.ID == CMD_LOGIN::ID)
	{
		CMD_LOGIN* pCmd = (CMD_LOGIN*)pBody;
	}
	if (header.ID == CMD_INITIAL_INFO::ID)
	{
		CMD_INITIAL_INFO* pCmd = (CMD_INITIAL_INFO*)pBody;

		m_bInitialized = true;
		m_PlayerData = pCmd->user;

		Objs()->DestroyObject();
		Objs()->AddObjects(pCmd->game);
		Objs()->SetMyPlayer(pCmd->user.m_nPlayerID);
		GlobalTimer()->SetTime(pCmd->game.dwGameTick);
	}
	if (header.ID == CMD_INPUT::ID)
	{
		CMD_INPUT* pCmd = (CMD_INPUT*)pBody;
		m_InputQueue.Push(*pCmd);


	}
	if (header.ID == CMD_CHAT::ID)
	{
		CMD_CHAT* pCmd = (CMD_CHAT*)pBody;
		m_ChatQueue.Push(*pCmd);


	}
	if (header.ID == CMD_CHAT_INIT::ID)
	{
		CMD_CHAT_INIT* pCmd = (CMD_CHAT_INIT*)pBody;
	}
	if (header.ID == CMD_ADD_PLAYER::ID)
	{
		CMD_ADD_PLAYER* pCmd = (CMD_ADD_PLAYER*)pBody;

		CPlayerObject* pNewPlayer = new CPlayerObject(pCmd->playerData);
		*static_cast<ST_OBJECT_DATA*>(pNewPlayer) = pCmd->objectData;
		Objs()->AddPlayer(pNewPlayer);
	}
	if (header.ID == CMD_REMOVE_PLAYER::ID)
	{
		CMD_REMOVE_PLAYER* pCmd = (CMD_REMOVE_PLAYER*)pBody;
		Objs()->RemovePlayer(pCmd->nUserID);
	}
	    if (header.ID == CMD_ADD_MONSTER::ID)
    {
        CMD_ADD_MONSTER* pCmd = (CMD_ADD_MONSTER*)pBody;

        CMonsterObject* pNewMonster = new CMonsterObject(pCmd->objectData, pCmd->monsterData);
        Objs()->AddObject(pNewMonster);
    }
    if (header.ID == CMD_USERINFO_UPDATE::ID)
    {
        CMD_USERINFO_UPDATE* pCmd = (CMD_USERINFO_UPDATE*)pBody;
        CPlayerObject* pPlayer = Objs()->GetPlayer(pCmd->nUserID);
        if (pPlayer)
            pPlayer->UpdateParts(pCmd->cSpecialParts);
    }
	if (header.ID == CMD_CHAT_ADD_PLAYER::ID)
	{
		CMD_ADD_PLAYER* pCmd = (CMD_ADD_PLAYER*)pBody;
		printf("ID : %d connected to chat server!\n", pCmd->ID);
	}
	if (header.ID == CMD_CHAT_REMOVE_PLAYER::ID)
	{
		CMD_REMOVE_PLAYER* pCmd = (CMD_REMOVE_PLAYER*)pBody;
		printf("ID : %d disconnected from chat server...\n", pCmd->nUserID);
		if (header.ID == CMD_ADD_MONSTER::ID)
		{
			CMD_ADD_MONSTER* pCmd = (CMD_ADD_MONSTER*)pBody;

			CMonsterObject* pNewMonster = new CMonsterObject(pCmd->objectData, pCmd->monsterData);
			Objs()->AddObject(pNewMonster);
		}
	}
}
void CClientLoop::PacketRecvThread(boolean isGame)
{
	PCK_HEADER header;
	std::vector<char> body;

	CClientSocket m_Socket = isGame == true ? m_Game_Socket : m_Chat_Socket;

	while (m_bContinue)
	{
		char cTest;
		int nRet = m_Socket.Peek(&cTest, 1);
		if (nRet < 0 && ERROR_TIMEOUT == GetLastError())
			continue;

		if (nRet <= 0)
		{
			printf("Client Network failure, %d\n", WSAGetLastError());
			continue;
		}

		nRet = m_Socket.Recv(&header, sizeof(header));
		if (nRet != (int)sizeof(header))
		{
			printf("Recving header failure\n");
			continue;
		}

		body.resize(header.LENGTH);
		nRet = m_Socket.Recv(&body[0], header.LENGTH);
		if (nRet != header.LENGTH)
		{
			printf("Recving body failure\n");
			continue;
		}

		OnPacketRecved(header, &body[0]);
	}

	m_Socket.Close();
}
