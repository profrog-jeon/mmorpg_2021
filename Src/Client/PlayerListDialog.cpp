#include "stdafx.h"
#include "PlayerListDialog.h"

CPlayerListDialog::CPlayerListDialog()
	: CDialogSuper(100, 100, 200, 200)
{
}

CPlayerListDialog::~CPlayerListDialog(void)
{
}

void CPlayerListDialog::getPlayers()
{
	Objs()->GetPlayers(0, 0, 1000, m_setPlayers);
}

void CPlayerListDialog::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
	for (int i = 0; i < nArrCount; i++)
	{
		// esc를 누르면 닫혀요
		if (pInputArr[i].bDown && KEY_ID_ESC == pInputArr[i].wKey)
			Close();
	}
}

void CPlayerListDialog::Draw(CRendererSuper* pRenderer)
{
	CDialogSuper::Draw(pRenderer);

	const size_t heightOfText = 20;
	const size_t topMargin = 150, windowMargin = 50, betweenMargin = 10;

	pRenderer->Rectangle(windowMargin, windowMargin,
		g_nPixelCountX - windowMargin, g_nPixelCountY - windowMargin,
		RGB(112, 146, 190), ' ');

	std::string title = "접속자 목록";
	pRenderer->TextOutW(g_nPixelCountX / 2, windowMargin + heightOfText, title);

	const size_t  numOfPlayer = m_setPlayers.size();

	if (m_setPlayers.empty()) return;

	auto iterSetPlayers = m_setPlayers.begin();

	for (int i = 0; i < numOfPlayer; i++)
	{
		std::string strMessage = (*iterSetPlayers)->GetName();

		pRenderer->TextOutW(g_nPixelCountX / 2, 
			windowMargin + heightOfText + (betweenMargin + heightOfText) * (i+1),
			strMessage);

		iterSetPlayers++;
	}

}