#pragma once

#include "LevelUpDialog.h"

class CPartsSelectDialog : public CDialogSuper
{
	CLevelUpDialog* m_pTargetDlg;

	std::vector<std::string> m_vecMenu;
	int m_nCurSel;

public:
	CPartsSelectDialog(CLevelUpDialog* pTargetDlg);
	~CPartsSelectDialog(void);

	void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	void Draw(CRendererSuper* pRenderer);
};

