#pragma once

#include "../../../quest/Src/QuestFramework/QuestFramework.h"
#include "DialogSuper.h"

std::string SubStr(std::string str, int startIndex, int endIndex);

class CNpcChatDialog : public CDialogSuper
{
	std::queue<std::string> m_queMessages;

public:
	CNpcChatDialog(const std::vector<ST_NPC_MESSAGE>& vecMsg);

	void Next(void);

private:
	void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	void Draw(CRendererSuper* pRenderer);
};