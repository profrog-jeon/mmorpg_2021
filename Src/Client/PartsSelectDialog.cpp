#include "stdafx.h"
#include "PartsSelectDialog.h"

CPartsSelectDialog::CPartsSelectDialog(CLevelUpDialog* pTargetDlg)
	: CDialogSuper(10, 0, g_nPixelCountX - 10, 0)
	, m_pTargetDlg(pTargetDlg)
	, m_nCurSel(0)
{
	m_vecMenu.push_back(GetPartsName(PARTS_MULTISHOT));
	m_vecMenu.push_back(GetPartsName(PARTS_POWERUP));
	m_vecMenu.push_back(GetPartsName(PARTS_BIGSHOT));

	int nHeight = 10 + m_vecMenu.size() * 20 + 10;
	int nTopMargin = (g_nPixelCountY - nHeight) / 2;
	m_nTop = nTopMargin;
	m_nBottom = nTopMargin + nHeight;
}

CPartsSelectDialog::~CPartsSelectDialog(void)
{
}

void CPartsSelectDialog::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
	for (int i = 0; i < nArrCount; i++)
	{
		if (pInputArr[i].bDown && KEY_ID_UP == pInputArr[i].wKey)
			m_nCurSel--;
		if (pInputArr[i].bDown && KEY_ID_DOWN == pInputArr[i].wKey)
			m_nCurSel++;

		m_nCurSel = std::min<int>(m_nCurSel, (int)m_vecMenu.size() - 1);
		m_nCurSel = std::max<int>(0, m_nCurSel);

		if (pInputArr[i].bDown && KEY_ID_SPACE == pInputArr[i].wKey
			|| pInputArr[i].bDown && KEY_ID_ENTER == pInputArr[i].wKey)
		{
			switch (m_nCurSel)
			{
			case 0:		m_pTargetDlg->SelectedParts(PARTS_MULTISHOT);		break;
			case 1:		m_pTargetDlg->SelectedParts(PARTS_POWERUP);			break;
			case 2:		m_pTargetDlg->SelectedParts(PARTS_BIGSHOT);			break;
			}
			
			Close();
		}
	}
}

void CPartsSelectDialog::Draw(CRendererSuper* pRenderer)
{
	CDialogSuper::Draw(pRenderer);

	// Draw cursor
	{
		int nLeft = m_nLeft + 10;
		int nRight = m_nRight - 10;
		int nTop = m_nTop + 10 + (20 * m_nCurSel);
		int nBottom = nTop + 20;
		pRenderer->Rectangle(nLeft, nTop, nRight, nBottom, RGB(255,255,255), '.');
	}

	// Draw item
	for (int i = 0; i < m_vecMenu.size(); i++)
	{
		int nPosX = g_nPixelCountX / 2;
		int nPosY = m_nTop + 10 + (20 * i);
		pRenderer->TextOut(nPosX, nPosY, m_vecMenu[i]);
	}
}
