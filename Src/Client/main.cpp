﻿#include "stdafx.h"
#include "ClientLoop.h"
#include "../GameFramework/Camera.h"

int main(int argc, char* argv[])
{
    int x = 0, y = 0;
    if (3 <= argc)
    {
        x = atoi(argv[1]);
        y = atoi(argv[2]);
    }
    else
    {
        x = 43;
        y = 78;
    }

    CClientLoop ClientLoop;
    ClientLoop.StartUp(x, y);
#ifdef SERVER_MODE
    ClientLoop.Loop(false);
#else
    ClientLoop.Loop(true);
#endif
    ClientLoop.ShutDown();
    return 0;
}
