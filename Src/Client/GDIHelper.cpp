#include "stdafx.h"
#include "GDIHelper.h"

ST_MEM_DC::ST_MEM_DC(void)
	: hDC(NULL)
	, m_hBitmap(NULL)
	, m_hOldBitmap(NULL)
{
}

ST_MEM_DC::~ST_MEM_DC(void)
{
	if (NULL == hDC)
		return;

	::SelectObject(hDC, m_hOldBitmap);
	::DeleteObject(m_hBitmap);
	::DeleteDC(hDC);
	hDC = NULL;
}

void ST_MEM_DC::Create(HWND inHWnd, SIZE szWindow)
{
	hWnd = inHWnd;
	HDC hDefaultDC = ::GetDC(hWnd);
	Create(hWnd, hDefaultDC, szWindow);
	::ReleaseDC(hWnd, hDefaultDC);
}

void ST_MEM_DC::Create(HWND inHWnd, HDC hDefaultDC, SIZE szWindow)
{
	hWnd = inHWnd;
	hDC = ::CreateCompatibleDC(hDefaultDC);

	m_hBitmap = ::CreateCompatibleBitmap(hDefaultDC, szWindow.cx, szWindow.cy);
	m_hOldBitmap = (HBITMAP)::SelectObject(hDC, m_hBitmap);

	m_Size = szWindow;
}

void ST_MEM_DC::Delete(void)
{
	if (NULL == hDC)
		return;

	::SelectObject(hDC, m_hOldBitmap);
	::DeleteObject(m_hBitmap);
	::DeleteDC(hDC);
	hDC = NULL;
}

void ST_MEM_DC::Clear(void)
{
	::PatBlt(hDC, 0, 0, m_Size.cx, m_Size.cy, WHITENESS);
}

void ST_MEM_DC::Flush(HDC hTargetDC, POINT ptOffset)
{
	::BitBlt(hTargetDC, ptOffset.x, ptOffset.y, m_Size.cx, m_Size.cy, hDC, 0, 0, SRCCOPY);
}

std::wstring S2WS(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

std::string string_format(const std::string fmt, ...) {
	int size = ((int)fmt.size()) * 2;
	std::string buffer;
	va_list ap;
	while (1) {
		buffer.resize(size);
		va_start(ap, fmt);
		int n = vsnprintf((char*)buffer.data(), size, fmt.c_str(), ap);
		va_end(ap);
		if (n > -1 && n < size) {
			buffer.resize(n);
			return buffer;
		}
		if (n > -1)
			size = n + 1;
		else
			size *= 2;
	}
	return buffer;
}


