#pragma once
#define MSG_RENDER WM_USER + 0x01
#define ID_EDIT_CHAT_LOG					100
#define ID_EDIT_CHAT_INPUT              1000

LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam);

class CGDIRenderer : public CRendererSuper
{
	friend DWORD WINAPI MessageloopThreadCaller(void* pContext);

	HWND m_hWnd;
	HANDLE m_hThread;

	std::queue<DWORD> m_FrameQueue;

public:

	CGDIRenderer();
	~CGDIRenderer();

	void Create();
	static CGDIRenderer* GetInstance(void)
	{
		static CGDIRenderer instance;
		return &instance;
	}

private:
	void Clear(void);
	void Flush(void);

	void Rectangle(int left, int top, int right, int bottom, COLORREF color, char patch);
	void Ellipse(int left, int top, int right, int bottom, COLORREF color, char patch);
	void TextOut(int cx, int cy, std::string text);

	void MessageloopThread();


};

inline CGDIRenderer* GDICreater(void)
{
	return CGDIRenderer::GetInstance();
}
