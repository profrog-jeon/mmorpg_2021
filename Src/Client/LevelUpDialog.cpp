#include "stdafx.h"
#include "LevelUpDialog.h"
#include "PartsSelectDialog.h"

CLevelUpDialog::CLevelUpDialog(int nLevel)
	: CDialogSuper(10, 110, g_nPixelCountX - 10, g_nPixelCountY - 110, RGB(221, 79, 66))
	, m_vecMessage()
	, m_nLevel(nLevel)
{
	CPlayerObject* pPlayer = Objs()->GetMyPlayer();

	unsigned char cPreParts = pPlayer->m_SpecialParts;
	ApplyLevelReward(nLevel);

	m_vecMessage.push_back(Format("���� %d �޼�", nLevel));

	unsigned char cDiff = cPreParts ^ pPlayer->m_SpecialParts;
	for (int i = 0; i < 8; i++)
	{
		const unsigned char cMask = 0x01 << i;
		if(cDiff & cMask)
			m_vecMessage.push_back(Format("[%s] ���� ȹ��", GetPartsName(cMask)));
	}

}

CLevelUpDialog::~CLevelUpDialog(void)
{
}

void CLevelUpDialog::SelectedParts(char cPartMask)
{
	CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();
	pMyPlayer->m_SpecialParts |= cPartMask;
}

const unsigned char g_LevelReward[10] = {
	/* 0 */0,
	/* 1 */PARTS_SPEEDUP1,
	/* 2 */PARTS_SPEEDUP2,
	/* 3 */PARTS_SPEEDUP3,
	/* 4 */PARTS_SPEEDUP4,
	/* 5 */PARTS_SPEEDUP5,
	/* 6 */0,
};

const int g_nMaxLine = 13;

void CLevelUpDialog::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
	for (int i = 0; i < nArrCount; i++)
	{
		if (pInputArr[i].bDown && KEY_ID_SPACE == pInputArr[i].wKey
			|| pInputArr[i].bDown && KEY_ID_ENTER == pInputArr[i].wKey
			|| pInputArr[i].bDown && KEY_ID_ESC == pInputArr[i].wKey)
		{
			if (7 == m_nLevel)
				DialogContainer()->Dialogs.push_back(new CPartsSelectDialog(this));// �������� ���̾�α�
			Close();
		}
	}
}

void CLevelUpDialog::Draw(CRendererSuper* pRenderer)
{
	CDialogSuper::Draw(pRenderer);

	for (int i = 0; i < m_vecMessage.size(); i++)
		pRenderer->TextOutW((m_nLeft + m_nRight) * 0.5, m_nTop + 10 + i * 20, m_vecMessage[i]);
}

const int g_nLevelUpTable[] = { 0, 2, 2, 3, 3, 3, 4 };
const int g_nLevelUpTableSize = sizeof(g_nLevelUpTable) / sizeof(g_nLevelUpTable[0]);

int CLevelUpDialog::CalcLevel(int nClearCount)
{
	int nTargetCount = 0;
	int nLevel = 0;
	for (nLevel; nLevel < g_nLevelUpTableSize; nLevel++)
	{
		nTargetCount += g_nLevelUpTable[nLevel];
		if (nClearCount < nTargetCount)
			break;
	}
	return nLevel;
}

void CLevelUpDialog::ApplyLevelReward(int nLevel)
{
	CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();

	for (int i = 0; i < nLevel && i < 10; i++)
		pMyPlayer->m_SpecialParts |= g_LevelReward[i];
}
