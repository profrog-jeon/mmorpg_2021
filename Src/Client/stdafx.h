#pragma once

#define _CRT_SECURE_NO_WARNINGS
#include "../../../quest/Src/QuestFramework/QuestFramework.h"
#include "../GameFramework/GameFramework.h"
#include "../NetworkFramework/NetworkFramework.h"
#include "../ProtocolFramework/ProtocolFramework.h"
#include "DialogContainer.h"

#define SERVER_MODE
#define SERVER_DEBUGGING