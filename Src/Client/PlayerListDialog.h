#pragma once

#include "DialogSuper.h"

class CPlayerListDialog : public CDialogSuper
{
	std::set<CPlayerObject*> m_setPlayers;

public:
	CPlayerListDialog();
	~CPlayerListDialog(void);

	void getPlayers();
	void Update(const ST_INPUT_INFO* pInputArr, int nArrCount);
	void Draw(CRendererSuper* pRenderer);
};

