#include "stdafx.h"
#include "NpcChatDialog.h"
#include "GDIHelper.h"
#include "LevelUpDialog.h"

CNpcChatDialog::CNpcChatDialog(const std::vector<ST_NPC_MESSAGE>& vecMsg)
    : CDialogSuper(10, 430, g_nPixelCountX - 10, g_nPixelCountY - 10)
{
	for (const ST_NPC_MESSAGE& msg : vecMsg)
	{
		std::string strMessage;
		if (0 == msg.nNpcID)
			strMessage = "��";
		else
		{
			ST_NPC_INFO* pNpc = QuestContainer()->GetNPC(msg.nNpcID);
			strMessage = pNpc == nullptr? "NPC" : pNpc->szName;
		}
		strMessage += ": ";
		strMessage += msg.szMessage;

		m_queMessages.push(strMessage);
	}
}

void CNpcChatDialog::Next(void)
{
	if (m_queMessages.empty())
		return;

	m_queMessages.pop();
    if (!m_queMessages.empty())
        return;

    // ��ȭ�� ��� ��������
    CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();
    const int nPreLevel = pMyPlayer->m_nLevel;
    const int nClearCount = QuestContainer()->GetClearCount(pMyPlayer);
    pMyPlayer->m_nLevel = CLevelUpDialog::CalcLevel(nClearCount);

    if (nPreLevel != pMyPlayer->m_nLevel)
        DialogContainer()->Dialogs.push_back(new CLevelUpDialog(pMyPlayer->m_nLevel));

	Close();
}

void CNpcChatDialog::Update(const ST_INPUT_INFO* pInputArr, int nArrCount)
{
	for (int i = 0; i < nArrCount; i++)
	{
		if (pInputArr[i].bDown && KEY_ID_SPACE == pInputArr[i].wKey)
			Next();
	}
}

void CNpcChatDialog::Draw(CRendererSuper* pRenderer)
{
    if (m_queMessages.empty())
        return;

    std::vector<std::string> newMessage;

    std::string strMessage = m_queMessages.front();
    if (strMessage.size() > 60)
    {
        std::vector<int> messageLength;
        int lineNumber = strMessage.size() / 20;
        int i;
        for (i = 0; i < lineNumber; i++)
        {
            newMessage.push_back(SubStr(strMessage, i * 20, 20));
            messageLength.push_back(newMessage[i].length());
        }
    }
    else
        newMessage.push_back(strMessage);

    m_nTop = 450 - newMessage.size() * 20;
    CDialogSuper::Draw(pRenderer);

    for (size_t i = 0; i < newMessage.size(); i++)
    {
        pRenderer->TextOut(g_nPixelCountX / 2, m_nTop + 20 + (20 * i), newMessage[i]);
    }
}

std::string SubStr(std::string str, int startIndex, int endIndex)
{
    std::string result;
    auto itr = str.begin();
    if (str.length() > (endIndex * 2))
    {
        int len = 0;
        int start = 0;
        bool flag = startIndex == 0 ? false : true;

        while (itr != str.end())
        {
            if (len == startIndex - 1 && flag)
            {
                startIndex = start;
                flag = false;
                len = 0;
            }
            if (len == endIndex && !flag)
            {
                return result;
            }

            unsigned char c = *itr;

            if (c > 128)
            {
                if (!flag)
                {
                    result += c;
                    itr++;
                    result += *itr;
                    itr++;
                    ++len;
                    continue;
                }
                itr += 2;
                start += 2;
                ++len;
                continue;
            }
            if (!flag)
            {
                result += c;
            }
            ++itr;
            ++len;
            ++start;
        }
    }
    if (result != "")
        return result;
    return str.substr(startIndex + 1, str.length() - startIndex);
}