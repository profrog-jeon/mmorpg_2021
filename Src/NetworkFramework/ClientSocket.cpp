#include "stdafx.h"
#include "ClientSocket.h"

int CClientSocket::Connect(const char* pszIP, unsigned short wPort)
{
	m_Socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == m_Socket)
		return -1;

	DWORD dwTimeOut = 5000;
	int nRet = setsockopt(m_Socket, SOL_SOCKET, SO_RCVTIMEO, (char*)&dwTimeOut, sizeof(dwTimeOut));
	if (SOCKET_ERROR == nRet)
		printf("setsockopt(m_hSocket, SOL_SOCKET_, SO_RCVTIMEO_, %d) failure, %d\n", dwTimeOut, WSAGetLastError());
	nRet = setsockopt(m_Socket, SOL_SOCKET, SO_SNDTIMEO, (char*)&dwTimeOut, sizeof(dwTimeOut));
	if (SOCKET_ERROR == nRet)
		printf("setsockopt(m_hSocket, SOL_SOCKET_, SO_SNDTIMEO, %u) failure, %d\n", dwTimeOut, WSAGetLastError());

	sockaddr_in addr = { 0, };
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(pszIP);
	addr.sin_port = ::htons(wPort);
	return ::connect(m_Socket, (sockaddr*)&addr, sizeof(addr));
}
