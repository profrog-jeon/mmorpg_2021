#include "stdafx.h"
#include "ConnectionSocket.h"

CConnectionSocket::CConnectionSocket(void)
    : CSocket()
{
}

CConnectionSocket::~CConnectionSocket(void)
{
}

int CConnectionSocket::Accept(SOCKET sockListen)
{
    sockaddr addrRemote;
    int nReadSize = sizeof(addrRemote);
    m_Socket = ::accept(sockListen, &addrRemote, &nReadSize);
    if (INVALID_SOCKET == m_Socket)
        return -1;

    return 0;
}

void CConnectionSocket::ConnectionThread(void)
{
	PCK_HEADER header;
	std::vector<char> body;

	while (true)
	{
		char cTest;
		int nRet = Peek(&cTest, 1);
		if (nRet < 0 && ERROR_TIMEOUT == GetLastError())
			continue;

		if (nRet <= 0)
		{
			printf("Network failure, %d\n", WSAGetLastError());
			break;
		}

		nRet = Recv(&header, sizeof(header));
		if (nRet != (int)sizeof(header))
		{
			printf("Recving header failure");
			break;
		}

		body.resize(header.LENGTH);
		nRet = Recv(&body[0], header.LENGTH);
		if (nRet != header.LENGTH)
		{
			printf("Recving body failure");
			break;
		}

		OnPacketRecved(header, &body[0]);
	}
	::closesocket(m_Socket);
	OnClose();
}
