#pragma once

#include "Socket.h"
#include "Packet.h"

class CConnectionSocket : public CSocket
{
public:
	CConnectionSocket(void);
	~CConnectionSocket(void);

	int Accept(SOCKET sockListen);
	void ConnectionThread(void);

protected:
	virtual void OnPacketRecved(const PCK_HEADER& header, void* pBody) = 0;
	virtual void OnClose(void) = 0;
};

