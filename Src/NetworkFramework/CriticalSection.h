#pragma once

#include <Windows.h>

class CCriticalSection
{
public:
	//------------------------------------
	class Owner
	{
	private:
		CCriticalSection&	m_Instance;

	public:
		Owner(CCriticalSection& obj);
		~Owner(void);
	};
	//------------------------------------

private:
	CRITICAL_SECTION	m_cs;

public:
	CCriticalSection(void);
	~CCriticalSection(void);
	void Enter(void);
	void Leave(void);
};
