#pragma once

#include <Windows.h>

class CSocket
{
	HANDLE m_hThread;

protected:
	SOCKET m_Socket;

	CSocket(void);
	virtual ~CSocket(void);

public:
	int Send(const void* pBuffer, int nSize);
	int Recv(void* pBuffer, int nSize);
	int Peek(void* pBuffer, int nSize);
	int Close(void);
};

