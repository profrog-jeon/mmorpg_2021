#include "stdafx.h"
#include "Server.h"

CServer::CServer(void)
	: m_Info()
	, m_ListenSocket(INVALID_SOCKET)
{
}

CServer::~CServer(void)
{
}

int CServer::StartUp(const ST_SERVER_INFO& info)
{
	int nRet = 0;
	m_ListenSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == m_ListenSocket)
		return -1;

	sockaddr_in addr = { 0, };
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = ::htons(info.wPort);
	nRet = ::bind(m_ListenSocket, (sockaddr*)&addr, sizeof(addr));
	if (SOCKET_ERROR == nRet)
		return -1;

	m_Info = info;
	for(CConnectionSocket* pConnection : m_Info.Connections)
		m_ReadyQueue.Push(pConnection);

	return ::listen(m_ListenSocket, 5);
}

void CServer::ShutDown(void)
{
	closesocket(m_ListenSocket);

	for (CConnectionSocket* pConnection : m_Info.Connections)
		delete pConnection;

	m_Info.Connections.clear();
}

DWORD WINAPI ConnectionThreadCaller(void* pContext)
{
	DWORD dwThreadID = ::GetCurrentThreadId();

	CServer* pThis = (CServer*)pContext;
	CConnectionSocket* pConnection = nullptr;
	{
		CCriticalSection::Owner lock(pThis->m_csConnectionMap);
		pConnection = pThis->m_ConnectionMap[dwThreadID];
	}

	pConnection->ConnectionThread();

	{
		CCriticalSection::Owner lock(pThis->m_csConnectionMap);
		pThis->m_ConnectionMap.erase(dwThreadID);
	}

	{
		pThis->m_ReadyQueue.Push(pConnection);
	}

	::printf("Connection thread(id:%X) terminated.\n", dwThreadID);
	return 0;
}

void CServer::Loop(void)
{
	while (true)
	{
		if (m_ReadyQueue.IsEmpty())
		{
			::Sleep(1000);
			continue;
		}

		CConnectionSocket* pConnection;
		m_ReadyQueue.Pop(pConnection);

		if (pConnection->Accept(m_ListenSocket))
		{
			::printf("accept failure.\n");
			return;
		}

		DWORD dwThreadID;
		HANDLE hThread = ::CreateThread(NULL, 0, ConnectionThreadCaller, this, CREATE_SUSPENDED, &dwThreadID);

		{
			CCriticalSection::Owner lock(m_csConnectionMap);
			m_ConnectionMap[dwThreadID] = pConnection;
			::printf("Thread(id:%X) connected.(%u/%u)\n", dwThreadID, m_ReadyQueue.Count(), m_Info.Connections.size());
		}

		::ResumeThread(hThread);
		::CloseHandle(hThread);
	}
}
