#pragma once

#include <vector>
#include <queue>
#include <map>
#include "CriticalSection.h"
#include "ConnectionSocket.h"
#include "SafeQueue.h"

struct ST_SERVER_INFO
{
	unsigned short wPort;
	std::vector<CConnectionSocket*> Connections;
};

class CServer
{
	friend DWORD WINAPI ConnectionThreadCaller(void* pContext);

protected:
	ST_SERVER_INFO m_Info;
	SOCKET m_ListenSocket;

	CSafeQueue<CConnectionSocket*> m_ReadyQueue;

	CCriticalSection m_csConnectionMap;
	std::map<DWORD, CConnectionSocket*> m_ConnectionMap;

public:
	CServer(void);
	~CServer(void);
	virtual int StartUp(const ST_SERVER_INFO& info);
	virtual void ShutDown(void);

	void Loop(void);

	template<typename T>
	void Broadcast(const T& cmd)
	{
		CCriticalSection::Owner Lock(m_csConnectionMap);
		for (auto iter = m_ConnectionMap.begin(); iter != m_ConnectionMap.end(); iter++)
		{
			int nRet = SendPacket(iter->second, cmd);
		}
	}
};

