#include "stdafx.h"
#include "Socket.h"

struct ST_WSA_INITIALIZE
{
	WSADATA wsaData;
	ST_WSA_INITIALIZE(void)
	{
		::WSAStartup(MAKEWORD(2, 2), &wsaData);
	}

	~ST_WSA_INITIALIZE(void)
	{
		::WSACleanup();
	}
};
static ST_WSA_INITIALIZE g_InitWSA;

CSocket::CSocket(void)
{
}

CSocket::~CSocket(void)
{
}

int CSocket::Send(const void* pBuffer, int nSize)
{
	int nTotalSent = 0;
	while (nTotalSent < nSize)
	{
		const int nRemainedSize = nSize - nTotalSent;
		int nSent = ::send(m_Socket, (const char*)pBuffer + nTotalSent, nRemainedSize, 0);
		if (nSent < 0)
			return nSent;

		nTotalSent += nSent;
		if (ERROR_TIMEOUT == WSAGetLastError())
			Sleep(10);
	}

	return nTotalSent;
}

int CSocket::Recv(void* pBuffer, int nSize)
{
	int nTotalRecved = 0;
	while (nTotalRecved < nSize)
	{
		const int nRemainedSize = nSize - nTotalRecved;
		int nRecved = ::recv(m_Socket, (char*)pBuffer + nTotalRecved, nRemainedSize, 0);
		if (nRecved < 0)
			return nRecved;

		nTotalRecved += nRecved;
		if (ERROR_TIMEOUT == WSAGetLastError())
			Sleep(10);
	}

	return nTotalRecved;
}

int CSocket::Peek(void* pBuffer, int nSize)
{
	return ::recv(m_Socket, (char*)pBuffer, nSize, MSG_PEEK);
}

int CSocket::Close(void)
{
	::closesocket(m_Socket);
	return 0;
}
