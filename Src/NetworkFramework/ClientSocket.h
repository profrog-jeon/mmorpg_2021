#pragma once

#include "Socket.h"

class CClientSocket : public CSocket
{
public:
	int Connect(const char* pszIP, unsigned short wPort);
};

