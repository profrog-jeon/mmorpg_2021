#pragma once

#include "../NetworkFramework/NetworkFramework.h"

template<typename T>
int SendPacket(CSocket* pSocket, const T& pck)
{
	//if(10 != T::ID)
		//printf("Send packet id :  %d\n", T::ID);

	std::vector<char> vecBuffer;
	vecBuffer.resize(sizeof(PCK_HEADER) + sizeof(T));

	PCK_HEADER* pHeader = (PCK_HEADER*)&vecBuffer[0];
	pHeader->ID = T::ID;
	pHeader->LENGTH = sizeof(T);

	T* pBody = (T*)&vecBuffer[sizeof(PCK_HEADER)];
	(*pBody) = pck;

	return pSocket->Send(vecBuffer.data(), (int)vecBuffer.size());
}

template<typename T>
int RecvPacket(CSocket* pSocket, T& pck)
{
	PCK_HEADER header;

	int nRet = pSocket->Recv(&header, sizeof(header));
	if (nRet <= 0)
		return nRet;

	if (header.ID != T::ID)
		return -1;

	if (header.LENGTH != sizeof(pck))
		return -1;

	return pSocket->Recv(&pck, sizeof(pck));
}
