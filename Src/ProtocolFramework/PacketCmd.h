#pragma once

#include "../GameFramework/Struct.h"
#include "../GameFramework/MonsterObject.h"
#include "PacketReq.h"

struct CMD_LOGIN
{
	static const int ID = 0;
	bool bSucceeded;
};

struct CMD_CHAT_INIT
{
	static const int ID = 2;
	bool bSucceeded;
};

const int g_nMaxObjecCount = 200;
const int g_nMaxPlayerCount = 50;
const int g_nMaxMonsterCount = 50;
struct ST_GAME_INFO
{
	DWORD dwGameTick;
	int nObjectCount;
	int nPlayerCount;
	int nMonsterCount;
	ST_OBJECT_DATA objects[g_nMaxObjecCount];
	ST_PLAYER_DATA playerData[g_nMaxPlayerCount];
	ST_MONSTER_DATA monsterData[g_nMaxMonsterCount];
};

struct CMD_INITIAL_INFO
{
	static const int ID = 1;
	ST_PLAYER_DATA user;
	ST_GAME_INFO game;
	ST_PLAYER_POS pos;
};

const int g_nMaxInputAggregateCount = g_nMaxPlayerCount;
struct CMD_INPUT
{
	static const int ID = 10;
	DWORD dwGameTick;

	int nAggregateCount;
	REQ_INPUT aggregate[g_nMaxInputAggregateCount];
};

struct CMD_ADD_PLAYER
{
	static const int ID = 11;
	ST_OBJECT_DATA objectData;
	ST_PLAYER_DATA playerData;
};

struct CMD_CHAT_ADD_PLAYER
{
	static const int ID = 14;
	ST_OBJECT_DATA objectData;
	ST_PLAYER_DATA playerData;
};

struct CMD_REMOVE_PLAYER
{
	static const int ID = 12;
	int nUserID;
};

struct CMD_CHAT_REMOVE_PLAYER
{
	static const int ID = 15;
	int nUserID;
};



struct CMD_CHAT
{
	static const int ID = 13;
	int nUserID;
	char str[CHAT_SIZE];
};

struct CMD_ADD_MONSTER
{
	static const int ID = 13;
	ST_OBJECT_DATA objectData;
	ST_MONSTER_DATA monsterData;
};

struct CMD_USERINFO_UPDATE
{
	static const int ID = 14;
	int nUserID;
	unsigned char cSpecialParts;
};
