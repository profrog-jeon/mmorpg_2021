#pragma once

#define CHAT_SIZE 512

const int g_nMaxUserIDLen = 20;
const int g_nMaxPasswordLen = 20;
struct REQ_LOGIN
{
	static const int ID = 0;
	char szUserID[g_nMaxUserIDLen + 1];
	char szPassword[g_nMaxPasswordLen + 1];
};

struct REQ_CHAT_INIT
{
	static const int ID = 2;
	char szUserID[g_nMaxUserIDLen + 1];
	char szPassword[g_nMaxPasswordLen + 1];
};



struct ST_INPUT_INFO
{
	bool bDown;	// true: down, false: up
	char reserved;
	unsigned short wKey;

	ST_INPUT_INFO(void)
		: bDown(false), reserved(0), wKey(0) {}

	ST_INPUT_INFO(bool down, unsigned short key)
		: bDown(down), reserved(0), wKey(key) {}
};



const int g_nMaxInputArrCount = 5;	// �Ѽհ��� ����
struct REQ_INPUT
{
	static const int ID = 10;
	int nUserID;
	int nCount;
	ST_INPUT_INFO input[g_nMaxInputArrCount];
};

struct REQ_CHAT
{
	static const int ID = 13;
	int nUserID;
	char str[CHAT_SIZE];
};

struct REQ_USERINFO_UPDATE
{
	static const int ID = 11;
	int nUserID;
	unsigned char cSpecialParts;
};
