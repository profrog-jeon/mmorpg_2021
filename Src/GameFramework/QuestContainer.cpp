#include "stdafx.h"
#include "QuestContainer.h"
#include "NpcObject.h"

struct ST_DLL_LOADER
{
	HMODULE hModule;
	
	ST_DLL_LOADER(LPCTSTR pszDllName)
	{
		hModule = LoadLibrary(pszDllName);
	}

	~ST_DLL_LOADER(void)
	{
		if (nullptr == hModule)
			return;
		FreeLibrary(hModule);
	}
};

CQuestContainer::CQuestContainer(void)
{
}

CQuestContainer::~CQuestContainer(void)
{
}

void CQuestContainer::Load(void)
{
	WIN32_FIND_DATA stFindData;
	
	HANDLE hFile = FindFirstFile(TEXT("*.dll"), &stFindData);
	if (INVALID_HANDLE_VALUE == hFile)
		return;

	do
	{
		ST_DLL_LOADER stDllLoader(stFindData.cFileName);
		HMODULE& hModule = stDllLoader.hModule;
		if (nullptr == hModule)
			continue;

		// QueryNPC
		FP_QueryNpc pfQueryNpc = (FP_QueryNpc)GetProcAddress(hModule, "QueryNpc");
		if (nullptr == pfQueryNpc)
			continue;

		int nCount = pfQueryNpc(nullptr);
		if (0 == nCount)
			continue;

		std::vector<ST_NPC_INFO> vecNPCInfo;
		vecNPCInfo.resize(nCount);
		pfQueryNpc(&vecNPCInfo[0]);

		for (ST_NPC_INFO& npc : vecNPCInfo)
		{
			Objs()->AddObject(new CNpcObject(npc));
			m_NpcMap.insert(std::make_pair(npc.id, npc));
		}

		// QueryQuest
		FP_QueryQuest pfQueryQuest = (FP_QueryQuest)GetProcAddress(hModule, "QueryQuest");
		if (nullptr == pfQueryQuest)
			continue;

		for (int i = 0; ; i++)
		{
			int nSequenceCount = pfQueryQuest(i, nullptr, nullptr);
			if (0 == nSequenceCount)
				break;

			ST_QUEST_DATA stQuestData;
			stQuestData.m_Sequence.resize(nSequenceCount);
			pfQueryQuest(i, &stQuestData.m_nTargetNpcId, &stQuestData.m_Sequence[0]);

			CQuestInfoSuper* pQuestInfo = new CQuestInfoSuper(stQuestData);
			m_QuestMap.insert(std::make_pair(pQuestInfo->GetID(), pQuestInfo));
		}
	}	while (FindNextFile(hFile, &stFindData));

	FindClose(hFile);
}

ST_NPC_INFO* CQuestContainer::GetNPC(int nNpcID)
{
	if(m_NpcMap.end() == m_NpcMap.find(nNpcID))
		return nullptr;
	return &m_NpcMap[nNpcID];
}

CQuestInfoSuper* CQuestContainer::GetQuest(int nNpcID)
{
	for (auto iter = m_QuestMap.lower_bound(nNpcID); iter != m_QuestMap.upper_bound(nNpcID); iter++)
		return iter->second;
	return nullptr;
}

void CQuestContainer::QueryQuests(const ST_USER_QUESTINFO* pUserQuestInfo, std::vector<CQuestInfoSuper*>& outQuestInfo)
{
	for (auto iter = m_QuestMap.begin(); iter != m_QuestMap.end(); iter++)
	{
		if (!iter->second->IsProcessed(pUserQuestInfo))
			continue;

		outQuestInfo.push_back(iter->second);
	}
}

int CQuestContainer::GetClearCount(const ST_USER_QUESTINFO* pUserQuestInfo)
{
	int nCount = 0;
	for (auto iter = m_QuestMap.begin(); iter != m_QuestMap.end(); iter++)
	{
		if (!iter->second->IsProcessed(pUserQuestInfo))
			continue;

		if (!iter->second->IsCleared())
			continue;

		nCount++;
	}

	return nCount;
}