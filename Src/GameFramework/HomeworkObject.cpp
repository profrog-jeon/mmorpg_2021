#include "stdafx.h"
#include "HomeworkObject.h"
#include "MissileObject.h"
#include "TextObject.h"

CHomeworkObject::CHomeworkObject(const ST_OBJECT_DATA & objData)
{
	ST_OBJECT_DATA* pThis = static_cast<ST_OBJECT_DATA*>(this);
	*pThis = objData;
}

CHomeworkObject::CHomeworkObject(double x, double y, double endX, double endY, double hp, char cPatch, CPlayerObject* pOwner)
	: CObjectSuper(OBJECT_TYPE_HOMEWORK)
	, m_HP(hp)
	, m_MaxHP(hp)
	, m_TargetPosX(endX)
	, m_TargetPosY(endY)
	, m_nOwnerID(0)
	, m_pLastText(nullptr)
{
	m_CenterX = m_PosX = x;
	m_CenterY = m_PosY = y;
	m_Width = 0.5;
	m_Height = 0.5;
	m_Weight = 1;
	m_cPatch = cPatch;
	if (pOwner)
	{
		m_strName = pOwner->GetName() + " 과제";
		m_nOwnerID = pOwner->GetPlayerID();
	}
}

CHomeworkObject::~CHomeworkObject(void)
{
}

void CHomeworkObject::Destroy(void)
{
	m_HP = 0;
}

void CHomeworkObject::OnUpdate(double dElapsedSec)
{
	m_VelX = m_TargetPosX - m_PosX;
	m_VelY = m_TargetPosY - m_PosY;
}

bool CHomeworkObject::IsDestroyed(void)
{
	return m_HP <= 0;
}

void CHomeworkObject::OnCollide(const CObjectSuper* pOther)
{
	if (OBJECT_TYPE_MISSILE != pOther->m_Type)
		return;

	const CMissileObject* pMissile = dynamic_cast<const CMissileObject*>(pOther);
	if (nullptr == pMissile)
		return; 

	const double dDamage = pMissile->m_Power * (GlobalTimer()->GetTime() % 50 + 50) * 0.01;
	if (m_nOwnerID && m_nOwnerID != pMissile->m_nPlayerID)
	{
		// 다른사람 과제 도와주면 데미지 x5만큼 HP 추가
		m_HP += dDamage * 5;

		if (m_pLastText && Objs()->IsExist(m_pLastText))
			return;

		switch (rand() % 3)
		{
		case 0:
			m_pLastText = new CTextObject(pOther->m_PosX, pOther->m_PosY, "베끼지마!", 1.5);
			break;
		case 1:
			m_pLastText = new CTextObject(pOther->m_PosX, pOther->m_PosY, "과제는 스스로!", 1.5);
			break;
		case 2:
			m_pLastText = new CTextObject(pOther->m_PosX, pOther->m_PosY, "보여주는 놈이 더나빠", 1.5);
			break;
		}

		Objs()->AddObject(m_pLastText);
	}
	else
	{
		m_HP -= dDamage;
		Objs()->AddObject(new CTextObject(pOther->m_PosX, pOther->m_PosY, Format("%d", (int)dDamage), 0.5, ANIMATE_RISE));
	}

	{	// 맞으면 랜덤하게 튕겨나가기
		double dAngle = (double)GlobalTimer()->GetTime() * 7;
		m_TargetPosX = m_CenterX + sin(dAngle) * dDamage * 2;
		m_TargetPosY = m_CenterY + cos(dAngle) * dDamage * 2;
	}

	if( 0 < m_HP)
		m_strName = Format("(%d/%d)", (int)m_HP, (int)m_MaxHP);
}

void CHomeworkObject::Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer)
{
	double left = (m_PosX - viewport.cx - m_Width * 0.5) / viewport.scale + g_nPixelCountX / 2;
	double right = (m_PosX - viewport.cx + m_Width * 0.5) / viewport.scale + g_nPixelCountX / 2;
	double top = -(m_PosY - viewport.cy + m_Height * 0.5) / viewport.scale + g_nPixelCountY / 2;
	double bottom = -(m_PosY - viewport.cy - m_Height * 0.5) / viewport.scale + g_nPixelCountY / 2;

	if (right < 0)
		return;
	if (g_nPixelCountX < left)
		return;
	if (bottom < 0)
		return;
	if (g_nPixelCountY < top)
		return;

	std::string m_cString(1, m_cPatch);
	pRenderer->TextOut(left, bottom, m_cString);
}
