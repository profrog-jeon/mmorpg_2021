#include "stdafx.h"
#include "PlayerObject.h"
#include "MissileObject.h"
#include "NpcObject.h"
#include "QuestContainer.h"
#include "SpecialParts.h"

CPlayerObject::CPlayerObject(const ST_PLAYER_DATA& playerData)
	: CPlayerFSM()
	, ST_PLAYER_DATA(playerData)
	, m_nLevel(1)
	, m_HP(130)
	, m_MP(0)
	, m_Power(15)
	, m_dMissileSize(0.5)
	, m_MissileColor(RGB(0,0,0))
{
	m_strName = playerData.m_szUserName;
	m_PosX = 0;
	m_PosY = 0;
	m_Height = 0.5;
	m_Width = 0.5;
	m_Weight = 70;
	m_cPatch = '*';
	m_Color = RGB(0, 0, 255);
	m_Speed = 10;
}

CPlayerObject::~CPlayerObject(void)
{
}

void CPlayerObject::PowerUp(void)
{
	m_Power = 30;
}

void CPlayerObject::SizeUp(void)
{
	m_Size = 0.4;
}

int CPlayerObject::GetPlayerID(void)
{
	return m_nPlayerID;
}

void CPlayerObject::UpdateParts(unsigned char cSpecialPartFlag)
{
	m_SpecialParts = cSpecialPartFlag;

	m_Speed = 10;
	if (PARTS_SPEEDUP1 & m_SpecialParts)
		m_Speed = 12;
	if (PARTS_SPEEDUP2 & m_SpecialParts)
		m_Speed = 14;
	if (PARTS_SPEEDUP3 & m_SpecialParts)
		m_Speed = 16;
	if (PARTS_SPEEDUP4 & m_SpecialParts)
		m_Speed = 18;
	if (PARTS_SPEEDUP5 & m_SpecialParts)
		m_Speed = 20;

	m_dMissileSize = 0.4;
	if (PARTS_BIGSHOT & m_SpecialParts)
		m_dMissileSize = .8;

	m_Power = 15;
	m_MissileColor = RGB(0, 0, 0);
	if (PARTS_POWERUP & m_SpecialParts)
	{
		m_Power = 50;
		m_MissileColor = RGB(255, 0, 0);
	}
}

void CPlayerObject::Skill(void)
{
	/*const double dDirX = cos(m_Angle) * m_dMissileSize * 4;
	const double dDirY = sin(m_Angle) * m_dMissileSize * 4;*/
	const double dDirX = cos(m_Angle);
	const double dDirY = sin(m_Angle);

	CMissileObject* pEnergy = new CMissileObject(m_Power, 
		m_PosX + dDirX,
		m_PosY + dDirY,
		m_VelX + 10 * dDirX,
		m_VelY + 10 * dDirY,
		m_dMissileSize,
		m_MissileColor);

	pEnergy->m_nPlayerID = m_nPlayerID;
	Objs()->ObjectList.push_back(pEnergy);

	if (m_SpecialParts & PARTS_MULTISHOT)
	{
		CMissileObject* pUpEnergy = new CMissileObject(m_Power,
			m_PosX + dDirX - dDirY,
			m_PosY + dDirY + dDirX,
			m_VelX + 10 * dDirX,
			m_VelY + 10 * dDirY,
			m_dMissileSize,
			m_MissileColor);

		pUpEnergy->m_nPlayerID = m_nPlayerID;
		Objs()->ObjectList.push_back(pUpEnergy);

		CMissileObject* pDownEnergy = new CMissileObject(m_Power,
			m_PosX + dDirX + dDirY,
			m_PosY + dDirY - dDirX,
			m_VelX + 10 * dDirX,
			m_VelY + 10 * dDirY,
			m_dMissileSize,
			m_MissileColor);

		pDownEnergy->m_nPlayerID = m_nPlayerID;
		Objs()->ObjectList.push_back(pDownEnergy);
	}
}

void CPlayerObject::AddKillCount(int nMonsterId)
{
	monsterKilled[nMonsterId]++;
}

void CPlayerObject::OnUpdate(double dElapsedTime)
{
	CObjectSuper::OnUpdate(dElapsedTime);

	double dTargetAngle = 0;
	if (0 == m_VelX && 0 == m_VelY)
		return;

	if (0 == m_VelX)
	{
		if (0 < m_VelY)
			dTargetAngle = M_PI_2;
		else
			dTargetAngle = -M_PI_2;
	}
	else if (0 == m_VelY)
	{
		if (0 < m_VelX)
			dTargetAngle = 0;
		else
			dTargetAngle = M_PI;
	}
	else if (m_VelX < 0)
	{
		if (0 < m_VelY)
			dTargetAngle = atan(m_VelY / m_VelX) + M_PI;
		else
			dTargetAngle = atan(m_VelY / m_VelX) - M_PI;
	}
	else
		dTargetAngle = atan(m_VelY / m_VelX);

	m_Angle += (dTargetAngle - m_Angle) * dElapsedTime * 50;
}

void CPlayerObject::OnCollide(const CObjectSuper* pOther)
{
	const CPlayerObject* pPlayer = dynamic_cast<const CPlayerObject*>(pOther);
	if (nullptr == pPlayer)
	{
		CObjectSuper::OnCollide(pOther);
		return;
	}

	bool isWall = Map()->IsBlocked(m_PosX, m_PosY + 1);

	if (isWall)
	{
		m_VelX = 0;
	}
	if (!isWall)
	{
		m_VelY = 0;
	}
}
