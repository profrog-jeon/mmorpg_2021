#include "stdafx.h"
#include "PositionUpdateEngine.h"

CPositionUpdateEngine::CPositionUpdateEngine(void)
{
}

CPositionUpdateEngine::~CPositionUpdateEngine(void)
{

}

void CPositionUpdateEngine::Update(double dElaspedSec)
{
	for (CObjectSuper* pObject : Objs()->ObjectList)
	{
		pObject->m_PosX += pObject->m_VelX * dElaspedSec;
		pObject->m_PosY += pObject->m_VelY * dElaspedSec;
	}
}
