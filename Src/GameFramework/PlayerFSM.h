#pragma once

#include "ObjectSuper.h"

enum E_PLAYER_STATE
{
	PLAYER_STATE_IDLE = 0,
	PLAYER_STATE_WALKING,
	PLAYER_STATE_DEAD,
};

enum E_PLAYER_EVENT
{
	PLAYER_EVENT_LEFT_DOWN,	// = 0
	PLAYER_EVENT_LEFT_UP,
	PLAYER_EVENT_RIGHT_DOWN,
	PLAYER_EVENT_RIGHT_UP,
	PLAYER_EVENT_UP_DOWN,
	PLAYER_EVENT_UP_UP,
	PLAYER_EVENT_DOWN_DOWN,
	PLAYER_EVENT_DOWN_UP,
	PLAYER_EVENT_LANDED,
	PLAYER_EVENT_COLLID
};

class CPlayerFSM
	: public CObjectSuper
{
	E_PLAYER_STATE m_nState;
	bool m_bLeft;
	bool m_bRight;
	bool m_bUp;
	bool m_bDown;

public:
	CPlayerFSM(void);
	~CPlayerFSM(void);

	void OnLeftDown(void);
	void OnLeftUp(void);
	void OnRightDown(void);
	void OnRightUp(void);
	void OnUpDown(void);
	void OnUpUp(void);
	void OnDownDown(void);
	void OnDownUp(void);
	void OnLanded(void);
	void OnCollide(const CObjectSuper* pOther);

private:
	E_PLAYER_STATE ChangeState(E_PLAYER_STATE nCurState, E_PLAYER_EVENT nEvent);
};

