#pragma once

#include "BillboardSuper.h"

class CQuestListBillboard;

class CMenuBillboard : public CBillboardSuper
{
	bool isClosed;
	size_t index;
	std::vector<std::string> m_vecMenues;
	CQuestListBillboard * m_pQuestListBillboard;

public:
	CMenuBillboard();

	void Prev(void);
	void Next(void);
	void Open(void);
	void Close(void);
	void Select(void);
	bool IsDestroyed(void);

private:
	void Draw(const ST_VIEWPORT& viewport, std::vector<std::string>& refVideoMemory);
	void DrawGDI(const ST_VIEWPORT viewport, HDC hDC);
};

