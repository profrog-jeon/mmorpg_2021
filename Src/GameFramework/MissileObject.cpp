#include "stdafx.h"
#include "MissileObject.h"

CEffectObject::CEffectObject(E_OBJECT_TYPE nType, double power, double size, double x, double y, double vx, double vy, double duration)
	: CObjectSuper(nType)
	, m_nPlayerID(-1)
	, m_dwTimeOutTick(GetTickCount() + (DWORD)(duration * 1000))
{
	m_Width = size;
	m_Height = size;
	m_PosX = x;
	m_PosY = y;
	m_VelX = vx;
	m_VelY = vy;
	m_Weight = 1;
	m_Color = RGB(0, 0, 0);
	m_Power = power;
	m_cPatch = '+';
}

bool CEffectObject::IsDestroyed(void)
{
	if (m_dwTimeOutTick < GetTickCount())
		return true;
	return false;
}

void CEffectObject::OnCollide(const CObjectSuper* pOther)
{
	m_dwTimeOutTick = 0;	// �ı�
}

CMissileObject::CMissileObject(void)
	: CEffectObject(OBJECT_TYPE_MISSILE, 0.5, 1, 0, 0, 0, 0, 5)
{
}

CMissileObject::CMissileObject(double power, double x, double y, double vx, double vy, double size, COLORREF color)
	: CEffectObject(OBJECT_TYPE_MISSILE, power * 0.5, size, x, y, vx, vy, 1)
{
	m_cPatch = '.';
	m_Color = color;
}
