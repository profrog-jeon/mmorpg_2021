#include "stdafx.h"
#include "CollisionDetectEngine.h"
#include "MapBackgroundBillboard.h"

CCollisionDetectEngine::CCollisionDetectEngine(void)
{
}

CCollisionDetectEngine::~CCollisionDetectEngine(void)
{
}

bool CheckCollision(const CObjectSuper* a, const CObjectSuper* b)
{
	double leftForA = a->m_PosX - a->m_Width;
	double rightForA = a->m_PosX + a->m_Width;
	double topForA = a->m_PosY + a->m_Height;
	double bottomForA = a->m_PosY - a->m_Height;

	double leftForB = b->m_PosX - b->m_Width;
	double rightForB = b->m_PosX + b->m_Width;
	double topForB = b->m_PosY + b->m_Height;
	double bottomForB = b->m_PosY - b->m_Height;

	if (rightForA < leftForB)
		return false;

	if (topForA < bottomForB)
		return false;

	if (rightForB < leftForA)
		return false;

	if (topForB < bottomForA)
		return false;

	if (CalculateCollision(a, b))
		return CalculateCollision(b, a);

	return CalculateCollision(a, b);
}

bool CalculateCollision(const CObjectSuper* a, const CObjectSuper* b)
{
	int moveVectorX = a->m_VelX;
	int moveVectorY = a->m_VelY;

	double disVectorX = -1 * (b->m_VelX);
	double disVectorY = -1 * (b->m_VelY);

	if (disVectorX == 0 && disVectorY == 0)
	{
		disVectorX = (b->m_PosX - a->m_PosX);
		disVectorY = (b->m_PosY - a->m_PosY);
	}

	double dotProduct = (moveVectorX * disVectorX) + (moveVectorY * disVectorY);

	double moveLength = sqrt(pow(moveVectorX, 2) + pow(moveVectorY, 2));
	double disLength = sqrt(pow(disVectorX, 2) + pow(disVectorY, 2));

	double costh = dotProduct / (moveLength * disLength);
	double theta = acos(costh);
	double degree = theta * 180 / M_PI;

	if ((90 < degree && degree < 270) || degree == 0)
		return false;
	else
		return true;
}

void CollisionUpdate(CObjectSuper* pObject, const double dElaspedSec)
{
	pObject->m_PosX -= (pObject->m_VelX * dElaspedSec);
	pObject->m_PosY -= (pObject->m_VelY * dElaspedSec);

	pObject->OnCollide(pObject);
}

void CCollisionDetectEngine::Update(double dElaspedSec)
{
	for (CObjectSuper* pObjectA : Objs()->ObjectList)
	{
		int x = pObjectA->m_PosX + pObjectA->m_VelX * dElaspedSec;
		int y = pObjectA->m_PosY + pObjectA->m_VelY * dElaspedSec;

		if (Map()->IsBlocked(x, y))
		{
			pObjectA->m_PosX -= (pObjectA->m_VelX * dElaspedSec);
			pObjectA->m_PosY -= (pObjectA->m_VelY * dElaspedSec);

			pObjectA->OnCollide(pObjectA);
			continue;
		}

		if (0 == pObjectA->m_Weight)
			continue;

		for (CObjectSuper* pObjectB : Objs()->ObjectList)
		{
			if (0 == pObjectB->m_Weight)
				continue;

			if (pObjectA == pObjectB)
				continue;

			if (!CheckCollision(pObjectA, pObjectB))
				continue;
			
			//Log("%s Collide!", pObjectA->m_strName);
			pObjectA->m_PosX -= (pObjectA->m_VelX * dElaspedSec);
			pObjectA->m_PosY -= (pObjectA->m_VelY * dElaspedSec);
			pObjectA->OnCollide(pObjectB);

			pObjectB->m_PosX -= (pObjectB->m_VelX * dElaspedSec);
			pObjectB->m_PosY -= (pObjectB->m_VelY * dElaspedSec);
			pObjectB->OnCollide(pObjectA);
		}
	}
	
}
