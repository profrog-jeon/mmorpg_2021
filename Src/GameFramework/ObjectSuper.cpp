#include "stdafx.h"
#include "ObjectSuper.h"
#include "../Client/GDIHelper.h"

CObjectSuper::CObjectSuper(void)
	: ST_OBJECT_DATA()
	, m_Color(RGB(0, 0, 0))
{
}

CObjectSuper::CObjectSuper(E_OBJECT_TYPE nType)
	: ST_OBJECT_DATA()
{
	m_Type = nType;
}

CObjectSuper::~CObjectSuper(void)
{
}

std::string CObjectSuper::GetName(void)
{
	return m_strName;
}

std::string CObjectSuper::GetGreetingMsg(void)
{
	return m_strGreetingMessage;
}

bool CObjectSuper::IsDestroyed(void)
{
	return false;
}

void CObjectSuper::OnUpdate(double dElapsedSec)
{
}

void CObjectSuper::Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer)
{
	double left = (m_PosX - viewport.cx - m_Width * 0.5) / viewport.scale + g_nPixelCountX / 2;
	double right = (m_PosX - viewport.cx + m_Width * 0.5) / viewport.scale + g_nPixelCountX / 2;
	double top = -(m_PosY - viewport.cy + m_Height * 0.5) / viewport.scale + g_nPixelCountY / 2;
	double bottom = -(m_PosY - viewport.cy - m_Height * 0.5) / viewport.scale + g_nPixelCountY / 2;

	if (right < 0)
		return;
	if (g_nPixelCountX < left)
		return;
	if (bottom < 0)
		return;
	if (g_nPixelCountY < top)
		return;

	pRenderer->Ellipse(left, top, right, bottom, m_Color, m_cPatch);
	if (!m_strName.empty())
		pRenderer->TextOut(left, bottom, m_strName);
}

void CObjectSuper::OnCollide(const CObjectSuper* pOther)
{
	m_VelX = 0;
	m_VelY = 0;
}
