#pragma once

#include "../QuestFramework/QuestFramework.h"
#include "BillboardSuper.h"

class CQuestListBillboard : public CBillboardSuper
{
	bool isClosed;
	int index;

	std::map<std::string, std::vector<ST_NPC_MESSAGE>>* mapQuestsLists;

public:
	CQuestListBillboard();

	void Prev(void);
	void Next(void);
	void Open(void);
	void Close(void);
	//void Select(void);

	bool IsDestroyed(void);

private:
	void Draw(const ST_VIEWPORT& viewport, std::vector<std::string>& refVideoMemory);
	void DrawGDI(const ST_VIEWPORT viewport, HDC hDC);
};
