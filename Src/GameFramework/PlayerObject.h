#pragma once

#include <map>
#include "../../../quest/Src/QuestFramework/QuestFramework.h"
#include "Struct.h"
#include <xutility>
#include "PlayerFSM.h"

class CPlayerObject
	: public CPlayerFSM, public ST_PLAYER_DATA
{
	friend class CPlayerStatusBillboard;

public:
	int m_nLevel;
	double m_HP;
	double m_MP;
	double m_Power;
	double m_dMissileSize;
	double m_Size;
	COLORREF m_MissileColor;

public:
	CPlayerObject(const ST_PLAYER_DATA& playerData);
	~CPlayerObject(void);

	void PowerUp(void);
	void SizeUp(void);
	int GetPlayerID(void);
	void UpdateParts(unsigned char cSpecialPartFlag);
	void Skill(void);
	void AddKillCount(int nMonsterId);

	void OnUpdate(double dElapsedTime);
	void OnCollide(const CObjectSuper* pOther);

	std::map<std::string, std::vector<ST_NPC_MESSAGE>>* GetQuestsHistory();
};

