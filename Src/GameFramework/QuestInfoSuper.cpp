#include "stdafx.h"
#include "QuestInfoSuper.h"

CQuestInfoSuper::CQuestInfoSuper(const ST_QUEST_DATA& stData)
	: ST_QUEST_DATA(stData)
	, m_nIndex(0)
{
}

CQuestInfoSuper::~CQuestInfoSuper(void)
{
}

int CQuestInfoSuper::GetID(void)
{
	return m_nTargetNpcId;
}

bool CQuestInfoSuper::PeekLastMessage(ST_USER_QUESTINFO& userInfo, ST_NPC_MESSAGE& outMessage)
{
	// 아직 시작도 안한 퀘스트라면 종료
	if (!userInfo.IsContectedNPC(m_nTargetNpcId))
		return false;

	const ST_NPC_MESSAGE* pLastMessage = nullptr;
	for(int i = m_nIndex; i < (int)m_Sequence.size(); i++)
	{
		const ST_QUEST_SEQUENCE& seq = m_Sequence[i];
		if (QUEST_SEQUENCE_TYPE_MESSAGE == seq.nType)
			pLastMessage = &seq.message;

		if (QUEST_SEQUENCE_TYPE_CONDITION == seq.nType && !userInfo.Test(seq.filter))
			break;
	}

	// 메시지가 없을 경우에는 직전 메시지를 찾아 넣어줌
	if (nullptr == pLastMessage)
	{
		int i;
		for (i = m_nIndex - 1; 0 <= i; i--)
		{
			if (QUEST_SEQUENCE_TYPE_MESSAGE != m_Sequence[i].nType)
				continue;

			pLastMessage = &m_Sequence[i].message;
			break;
		}
	}

	// 실수로 메시지를 만들지 않은 퀘스트에는 주의를 줌
	if (pLastMessage)
		outMessage = *pLastMessage;
	else
		outMessage = ST_NPC_MESSAGE(0, "** 아무 메시지가 없습니다. 버그 같아요..~ **");
	return true;
}

void CQuestInfoSuper::Process(ST_USER_QUESTINFO& userInfo, std::vector<ST_NPC_MESSAGE>& outMessage)
{
	userInfo.ContactNPC(m_nTargetNpcId);

	int nConditionIndex = m_nIndex;
	for (int i = m_nIndex; i<(int)m_Sequence.size(); i++)
	{
		const ST_QUEST_SEQUENCE& seq = m_Sequence[i];
		if (QUEST_SEQUENCE_TYPE_CONDITION != seq.nType)
			continue;

		if (!userInfo.Test(seq.filter))
			break;

		nConditionIndex = i;
	}

	while (m_nIndex < (int)m_Sequence.size())
	{
		const ST_QUEST_SEQUENCE& seq = m_Sequence[m_nIndex];
		if (QUEST_SEQUENCE_TYPE_MESSAGE == seq.nType && nConditionIndex < m_nIndex)
			outMessage.push_back(seq.message);

		if (QUEST_SEQUENCE_TYPE_REWARD == seq.nType)
			userInfo.Apply(seq.filter);

		if (QUEST_SEQUENCE_TYPE_CONDITION == seq.nType && !userInfo.Test(seq.filter))
			break;

		m_nIndex++;
	}

	// 조건에 걸려서 아무런 대화를 담을 수 없다면 직전 대화를 찾아서 넣어줌
	if (!outMessage.empty())
		return;

	int i;
	for (i = m_nIndex-1; 0 <= i; i--)
	{
		if (QUEST_SEQUENCE_TYPE_MESSAGE != m_Sequence[i].nType)
			continue;

		outMessage.push_back(m_Sequence[i].message);
		break;
	}

	// 실수로 메시지를 만들지 않은 퀘스트에는 주의를 줌
	if (outMessage.empty())
		outMessage.push_back(ST_NPC_MESSAGE(0, "** 아무 메시지가 없습니다. 버그 같아요..~ **"));
}

void CQuestInfoSuper::QueryMessageHistroy(ST_USER_QUESTINFO& userInfo, std::vector<ST_NPC_MESSAGE>& outMessage)
{
	for (int i = 0; i < m_nIndex && i < (int)m_Sequence.size(); i++)
	{
		const ST_QUEST_SEQUENCE& seq = m_Sequence[i];
		if (QUEST_SEQUENCE_TYPE_MESSAGE == seq.nType)
			outMessage.push_back(seq.message);
	}
}

bool CQuestInfoSuper::IsCleared(void)
{
	return m_Sequence.size() <= m_nIndex;
}

bool CQuestInfoSuper::IsProcessed(const ST_USER_QUESTINFO* pUserQuestInfo)
{
	if (0 < m_nIndex)
		return true;

	// 로그인 직후 처음이라면 m_nIndex가 0일테니 이 부분이 필요함
	for (int i = 0; i < (int)m_Sequence.size(); i++)
	{
		const ST_QUEST_SEQUENCE& seq = m_Sequence[i];
		if (QUEST_SEQUENCE_TYPE_CONDITION != seq.nType)
			continue;
		
		if (pUserQuestInfo->Test(seq.filter))
			return true;
	}

	return false;
}
