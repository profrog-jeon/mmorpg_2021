#pragma once

#include "ObjectSuper.h"
#include "TextObject.h"

class CHomeworkObject : public CObjectSuper
{
	double m_CenterX;
	double m_CenterY;
	double m_TargetPosX;
	double m_TargetPosY;
	double m_HP;
	double m_MaxHP;
	int m_nOwnerID;

	CTextObject* m_pLastText;

public:
	CHomeworkObject(const ST_OBJECT_DATA& objData);
	CHomeworkObject(double x, double y, double endX, double endY, double hp, char cPatch, CPlayerObject* pOwner = nullptr);
	~CHomeworkObject(void);

	void Destroy(void);
	void OnUpdate(double dElapsedSec);
	bool IsDestroyed(void);
	void OnCollide(const CObjectSuper* pOther);
	void Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer);
};

