#include "stdafx.h"
#include "TextObject.h"

CTextObject::CTextObject(double x, double y, std::string strText, double durationTime, E_ANIMATE_TYPE nAnimate)
	: m_dwTimeOutTick(GetTickCount() + (DWORD)(durationTime * 1000))
	, m_strText(strText)
	, m_nAnimateType(nAnimate)
	, m_TargetPosX(x)
	, m_TargetPosY(y) 
{
	m_PosX = x;
	m_PosY = y;
	m_Width = strText.length();
	m_Color = RGB(0,255,255);
	m_Height = 0.1;

	if (ANIMATE_RISE == nAnimate)
		m_TargetPosY = y + 4;
}

CTextObject::~CTextObject(void)
{
}

bool CTextObject::IsDestroyed(void)
{
	return m_dwTimeOutTick < GetTickCount();
}

void CTextObject::OnUpdate(double dElapsedSec)
{
	m_PosX += (m_TargetPosX - m_PosX) * dElapsedSec;
	m_PosY += (m_TargetPosY - m_PosY) * dElapsedSec;
}

void CTextObject::OnDraw(int nLeft, int nTop, int nRight, int nBottom, std::vector<std::string>& refVideoMemory)
{
	std::string& strLine = refVideoMemory[nTop];
	int nMaxLen = strLine.length() - nLeft;
	int nCopyLen = std::min<int>(nMaxLen, m_strText.length());
	memcpy((void*)(strLine.c_str() + nLeft), m_strText.c_str(), nCopyLen);
}

void CTextObject::Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer)
{
	double left = (m_PosX - viewport.cx - m_Width / m_strText.length() * 0.5) / viewport.scale + g_nPixelCountX / 2;
	double right = (m_PosX - viewport.cx + m_Width / m_strText.length() * 0.5) / viewport.scale + g_nPixelCountX / 2;
	double top = -(m_PosY - viewport.cy + m_Height / m_strText.length() * 0.5) / viewport.scale + g_nPixelCountY / 2;
	double bottom = -(m_PosY - viewport.cy - m_Height / m_strText.length() * 0.5) / viewport.scale + g_nPixelCountY / 2;

	if (right < 0)
		return;
	if (g_nPixelCountX < left)
		return;
	if (bottom < 0)
		return;
	if (g_nPixelCountY < top)
		return;

	pRenderer->TextOut((int)left, (int)top, m_strText);
}
