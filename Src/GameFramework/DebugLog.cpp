#include "stdafx.h"
#include "DebugLog.h"
#include <tchar.h>

void Log(LPCSTR pszFormat, ...)
{
	va_list list;
	va_start(list, pszFormat);
	char szBuffer[1024];
	vsprintf(szBuffer, pszFormat, list);
	va_end(list);

	strcat(szBuffer, "\n");
	OutputDebugStringA(szBuffer);
}

void Log(LPCWSTR pszFormat, ...)
{
	va_list list;
	va_start(list, pszFormat);
	wchar_t szBuffer[1024];
	_vswprintf(szBuffer, pszFormat, list);
	va_end(list);

	wcscat(szBuffer, L"\n");
	OutputDebugStringW(szBuffer);
}

struct ST_DEBUG_LOG_CONTEXT
{
	std::wstring strDebugLogFile;

	ST_DEBUG_LOG_CONTEXT(void)
	{
		const size_t tBuffSize = 1024;
		TCHAR szBuffer[tBuffSize + 1];
		GetModuleFileName(NULL, szBuffer, tBuffSize);

		strDebugLogFile = szBuffer;

		size_t tDirIndex = strDebugLogFile.rfind(TEXT("\\"));
		strDebugLogFile = strDebugLogFile.substr(0, tDirIndex);
		strDebugLogFile += TEXT("\\keylog.txt");

		DeleteFile(strDebugLogFile.c_str());
	}
}	g_LogContext;

void Debug(LPCSTR pszFormat, ...)
{
	va_list list;
	va_start(list, pszFormat);
	char szBuffer[1024];
	vsprintf(szBuffer, pszFormat, list);
	va_end(list);

	strcat(szBuffer, "\n");

	{
		HANDLE hFile = CreateFile(g_LogContext.strDebugLogFile.c_str(), FILE_APPEND_DATA, NULL, NULL, OPEN_ALWAYS, NULL, NULL);
		if (INVALID_HANDLE_VALUE == hFile)
			return;

		DWORD dwWritten = 0;
		WriteFile(hFile, szBuffer, strlen(szBuffer), &dwWritten, NULL);
		CloseHandle(hFile);
	}
}

std::string Format(LPCSTR pszFormat, ...)
{
	va_list list;
	va_start(list, pszFormat);
	char szBuffer[1024];
	vsprintf(szBuffer, pszFormat, list);
	va_end(list);

	return szBuffer;
}
