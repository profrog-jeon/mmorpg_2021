#pragma once

#include "BillboardSuper.h"

class CPlayerStatusBillboard : public CBillboardSuper
{
public:
	CPlayerStatusBillboard(void);
	~CPlayerStatusBillboard(void);

	void Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer);
};

