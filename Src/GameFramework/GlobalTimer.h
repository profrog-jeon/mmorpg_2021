#pragma once

class CGlobalTimer
{
	DWORD m_dwTickCount;

	CGlobalTimer(void);
	~CGlobalTimer(void);

public:
	void SetTime(DWORD dwTickCount);
	void Ticking(void);
	DWORD GetTime(void);

	static CGlobalTimer* GetInstance(void)
	{
		static CGlobalTimer instance;
		return &instance;
	}
};

inline CGlobalTimer* GlobalTimer(void)
{
	return CGlobalTimer::GetInstance();
}