#include "stdafx.h"
#include "NpcObject.h"

CNpcObject::CNpcObject(const ST_NPC_INFO& info)
	: CObjectSuper(OBJECT_TYPE_NPC)
{
	m_nNpcID = info.id;
	m_PosX = info.x;
	m_PosY = info.y;
	m_Width = info.w;
	m_Height = info.h;
	m_cPatch = info.patch;
	m_strName = info.szName;
	m_strGreetingMessage = info.szGreetMessage;

	m_Color = RGB(255, 0, 0);
	
	m_Weight = 1;
}

CNpcObject::~CNpcObject(void)
{
}

int CNpcObject::GetID(void)
{
	return m_nNpcID;
}

void CNpcObject::OnCollide(const CObjectSuper* pOther)
{
	if (OBJECT_TYPE_MISSILE == pOther->m_Type)
	{
	}
}

