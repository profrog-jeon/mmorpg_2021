#pragma once

#include "ObjectSuper.h"

struct ST_MONSTER_REGEN_DATA
{
	int id;
	DWORD dwCoolTime;
	char szName[21];
	int x;
	int y;
	double w;
	double h;
	int hp;
	int movingRange;
	DWORD dwRegenTimeStamp;
};

struct ST_MONSTER_DATA
{
	int m_nMonsterID;
	char m_szName[21];
	double m_RegenPosX;
	double m_RegenPosY;
	double m_TargetPosX;
	double m_TargetPosY;
	double m_HP;
	double m_MaxHP;
	int m_MovingRadius;

	void Init(int nID, LPCSTR pszName, int x, int y, int hp, int movingRadius)
	{
		m_nMonsterID = nID;
		strcpy(m_szName, pszName);
		m_RegenPosX = x;
		m_RegenPosY = y;
		m_TargetPosX = x;
		m_TargetPosY = y;
		m_HP = m_MaxHP = hp;
		m_MovingRadius = movingRadius;
	}
};

class CMonsterObject : public CObjectSuper, public ST_MONSTER_DATA
{
public:
	CMonsterObject(const ST_OBJECT_DATA& objData, const ST_MONSTER_DATA& monsterData);
	CMonsterObject(const ST_MONSTER_REGEN_DATA& regenData);
	~CMonsterObject(void);

	void Destroy(void);
	void OnUpdate(double dElapsedSec);
	bool IsDestroyed(void);
	void OnCollide(const CObjectSuper* pOther);
	void Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer);
};

