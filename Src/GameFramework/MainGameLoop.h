#pragma once

#include <stack>
#include "../ProtocolFramework/ProtocolFramework.h"
#include "RendererSuper.h"

extern bool GameLoopFlag;

class CMainGameLoop
{
	CRendererSuper* m_pRenderer;

protected:
	CMainGameLoop(CRendererSuper* pRenderer);
	~CMainGameLoop(void);

	virtual bool OnFrontLoop(DWORD dwGameTime) = 0;

public:
	int Loop(bool bServer);
	void Update(int nUserID, const ST_INPUT_INFO* inputArr, int nCount);

};