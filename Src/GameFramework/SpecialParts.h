#pragma once

const unsigned char PARTS_SPEEDUP1  = 0b00000001;
const unsigned char PARTS_SPEEDUP2  = 0b00000010;
const unsigned char PARTS_SPEEDUP3  = 0b00000100;
const unsigned char PARTS_SPEEDUP4  = 0b00001000;
const unsigned char PARTS_SPEEDUP5  = 0b00010000;
const unsigned char PARTS_MULTISHOT = 0b00100000;
const unsigned char PARTS_POWERUP   = 0b01000000;
const unsigned char PARTS_BIGSHOT   = 0b10000000;

const char* GetPartsName(unsigned char cPartsMask);