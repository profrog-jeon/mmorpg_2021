#include "stdafx.h"
#include "QuestListBillboard.h"
#include "../Client/GDIHelper.h"

CQuestListBillboard::CQuestListBillboard()
{
	isClosed = true;
	mapQuestsLists = nullptr;

	index = 0;
}

void CQuestListBillboard::Prev(void)
{
	if (index != 0)
		index--;
}

void CQuestListBillboard::Next(void)
{
	if (index < (int)mapQuestsLists->size() - 1)
		index++;
}

void CQuestListBillboard::Open()
{
	mapQuestsLists = Objs()->GetMyPlayer()->GetQuestsHistory();
	isClosed = false;
}

void CQuestListBillboard::Close()
{
	isClosed = true;
}

bool CQuestListBillboard::IsDestroyed(void)
{
	return isClosed;
}

void CQuestListBillboard::Draw(const ST_VIEWPORT& viewport, std::vector<std::string>& refVideoMemory)
{
	
}

void CQuestListBillboard::DrawGDI(const ST_VIEWPORT viewport, HDC hDC)
{
	ST_PEN Pen(hDC, RGB(0, 0, 0));

	const size_t numOfMenues = 4;
	const size_t heightOfText = 20;

	int start = index - index % numOfMenues;

	std::wstring title = S2WS("����Ʈ ���");
	
	TextOut(hDC, g_nPixelCountX / 2,
		g_nPixelCountY,
		title.c_str(), lstrlen(title.c_str()));

	if (mapQuestsLists->empty()) return;

	auto itQuestMap = mapQuestsLists->begin();

	for (int i = 0; i < start; i++) itQuestMap++;

	for (int i = 0; i < numOfMenues; i++) {
		std::wstring strMessage = S2WS(itQuestMap->first);
		SetTextAlign(hDC, TA_CENTER);

		if (i == index % numOfMenues)
			Rectangle(hDC, g_nPixelCountX / 6, g_nPixelCountY + (g_nDialogCountY / 2) - heightOfText * (numOfMenues / 2 - i) - 2,
				g_nPixelCountX / 6 * 5, g_nPixelCountY + (g_nDialogCountY / 2) - heightOfText * (numOfMenues / 2 - i - 1) - 2);

		TextOut(hDC, g_nPixelCountX / 2,
			g_nPixelCountY + (g_nDialogCountY / 2) - heightOfText * (numOfMenues / 2 - i),
			strMessage.c_str(), lstrlen(strMessage.c_str()));

		itQuestMap++;
		if (itQuestMap == mapQuestsLists->end()) break;

	}
}