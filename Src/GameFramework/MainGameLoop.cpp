﻿#include "stdafx.h"
#include "MainGameLoop.h"
#include "UpdateEngineSuper.h"
#include "ObjectUpdateEngine.h"
#include "PositionUpdateEngine.h"
#include "Camera.h"
#include "RendererSuper.h"
#include "PlayerObject.h"
#include "MissileObject.h"
#include "ObjectContainer.h"
#include "MapBackgroundBillboard.h"
#include "CameraTracingUpdateEngine.h"
#include "PlayerStatusBillboard.h"
#include "CollisionDetectEngine.h"
#include "QuestContainer.h"
#include "NpcObject.h"
#include "BossObject.h"
#include "SpecialParts.h"

bool GameLoopFlag = true;

CMainGameLoop::CMainGameLoop(CRendererSuper* pRenderer)
	: m_pRenderer(pRenderer)
{
}

CMainGameLoop::~CMainGameLoop(void)
{
}

int CMainGameLoop::Loop(bool bServer)
{
	QuestContainer()->Load();

	std::vector<CUpdateEngineSuper*> vecUpdateEngine;
	vecUpdateEngine.push_back(new CObjectUpdateEngine());
	vecUpdateEngine.push_back(new CPositionUpdateEngine());
	vecUpdateEngine.push_back(new CCollisionDetectEngine());
	vecUpdateEngine.push_back(new CCameraTracingUpdateEngine());

	Objs()->AddBackBillboard(new CMapBackgroundBillboard());
	Objs()->AddFrontBillboard(new CPlayerStatusBillboard());
	Objs()->AddObject(new CBossObject());

	const DWORD dwFPS = 10;
	const DWORD dwTickPerFrame = 1000 / dwFPS;
	const double dFrameElaspedSec = 1.0 / dwFPS;

	DWORD dwLastTick = GetTickCount();
	while (GameLoopFlag)
	{
		const DWORD dwGameTime = GlobalTimer()->GetTime();
		const DWORD dwCurrentTick = GetTickCount();
		const DWORD dwElapsedTick = dwLastTick < dwCurrentTick ? (dwCurrentTick - dwLastTick) : 0;
		const DWORD dwRemainedTick = dwElapsedTick < dwTickPerFrame ? (dwTickPerFrame - dwElapsedTick) : 0;

		dwLastTick += dwTickPerFrame;

		if (!OnFrontLoop(dwGameTime))
			break;

		for (size_t i = 0; i < vecUpdateEngine.size(); i++)
			vecUpdateEngine[i]->Update(dFrameElaspedSec);

		// 너무 빠르면 딜레이를 줌
		if (bServer && 0 < dwRemainedTick)
			Sleep(dwRemainedTick);

		{
			ST_VIEWPORT viewport;
			Camera()->QueryViewport(viewport);

			m_pRenderer->Clear();

			for (CBillboardSuper* pBillboard : Objs()->BackBillboardList)
				pBillboard->Draw(viewport, m_pRenderer);

			for (CObjectSuper* pObject : Objs()->ObjectList)
				pObject->Draw(viewport, m_pRenderer);

			for (CBillboardSuper* pBillboard : Objs()->FrontBillboardList)
				pBillboard->Draw(viewport, m_pRenderer);

			m_pRenderer->Flush();
		}

		GlobalTimer()->Ticking();
	}

	Objs()->DestroyObject();
	for (size_t i = 0; i < vecUpdateEngine.size(); i++)
		delete vecUpdateEngine[i];
	vecUpdateEngine.clear();

	return 0;
}

void CMainGameLoop::Update(int nUserID, const ST_INPUT_INFO* inputArr, int nCount)
{
	CPlayerObject* pPlayer = Objs()->GetPlayer(nUserID);
	if (nullptr == pPlayer)
		return;

	ST_PLAYER_KEY_STATE& keyState = *static_cast<ST_PLAYER_KEY_STATE*>(pPlayer);

	int i;
	for (i = 0; i < nCount; i++)
	{
		const ST_INPUT_INFO& curInputInfo = inputArr[i];
		keyState.isDown[curInputInfo.wKey] = curInputInfo.bDown;

		if (curInputInfo.bDown)
		{
			if (KEY_ID_LEFT == curInputInfo.wKey)		pPlayer->OnLeftDown();
			if (KEY_ID_RIGHT == curInputInfo.wKey)		pPlayer->OnRightDown();
			if (KEY_ID_UP == curInputInfo.wKey)			pPlayer->OnUpDown();
			if (KEY_ID_DOWN == curInputInfo.wKey)		pPlayer->OnDownDown();
			if (KEY_ID_SPACE == curInputInfo.wKey)		pPlayer->Skill();
		}
		else
		{
			if (KEY_ID_LEFT == curInputInfo.wKey)		pPlayer->OnLeftUp();
			if (KEY_ID_RIGHT == curInputInfo.wKey)		pPlayer->OnRightUp();
			if (KEY_ID_UP == curInputInfo.wKey)			pPlayer->OnUpUp();
			if (KEY_ID_DOWN == curInputInfo.wKey)		pPlayer->OnDownUp();
		}
	}
}
