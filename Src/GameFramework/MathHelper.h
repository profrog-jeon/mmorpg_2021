#pragma once

struct ST_VECTOR
{
	double x;
	double y;

	ST_VECTOR& operator*(double dScalar)
	{
		x *= dScalar;
		y *= dScalar;
		return *this;
	}
};

double GetDistance(const ST_OBJECT_DATA* pTo, const ST_OBJECT_DATA* pFrom);
double GetDistance(const ST_VECTOR& to, const ST_VECTOR& from);
ST_VECTOR GetVector(const ST_OBJECT_DATA* pTo, const ST_OBJECT_DATA* pFrom);
ST_VECTOR Normalize(const ST_VECTOR& stVector);
