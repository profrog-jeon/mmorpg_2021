#include "stdafx.h"
#include "ObjectUpdateEngine.h"

CObjectUpdateEngine::CObjectUpdateEngine(void)
{
}

CObjectUpdateEngine::~CObjectUpdateEngine(void)
{
}

template <typename T>
static void RemoveDestroyedObject(std::list<T*>& ObjectList)
{
	bool bEraseFound = true;
	do
	{
		bEraseFound = false;
		for (T* pObject : ObjectList)
		{
			if (!pObject->IsDestroyed())
				continue;

			delete pObject;
			ObjectList.remove(pObject);
			bEraseFound = true;
			break;
		}
	} while (bEraseFound);
}

void CObjectUpdateEngine::Update(double dElaspedSec)
{
	for (CBillboardSuper* pBillboard : Objs()->BackBillboardList)
		pBillboard->OnUpdate(dElaspedSec);

	for (CBillboardSuper* pBillboard : Objs()->FrontBillboardList)
		pBillboard->OnUpdate(dElaspedSec);

	for (CObjectSuper* pObject : Objs()->ObjectList)
		pObject->OnUpdate(dElaspedSec);

	RemoveDestroyedObject(Objs()->BackBillboardList);
	RemoveDestroyedObject(Objs()->FrontBillboardList);
	RemoveDestroyedObject(Objs()->ObjectList);
}
