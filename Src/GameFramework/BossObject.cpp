#include "stdafx.h"
#include "BossObject.h"
#include "MissileObject.h"
#include "TextObject.h"

CBossObject::CBossObject(void)
	: CObjectSuper(OBJECT_TYPE_NPC)
	, m_TargetPlayerIDs()
	, m_Homeworks()
	, m_nStatus(STATUS_IDLE)
	, m_dCoolTime(0)
	, m_tSequence(0)
{
	m_PosX = 160;
	m_PosY = 123;
	m_VelX = 0;
	m_VelY = 0;
	m_Width = 1;
	m_Height = 1;
	m_Weight = 100;
	m_Color = RGB(255,0,255);
	m_cPatch = '#';
	m_strName = "전설의 멘토";

	m_vecIntroMent.push_back("음...?");
	m_vecIntroMent.push_back("감히 전설 속의 멘토를 건드리다니..");
	m_vecIntroMent.push_back("과제의 지옥에 빠져들고 싶은게냐?");
	m_vecIntroMent.push_back("각오해라!!");

	m_vecFailureMent.push_back("어리석은 것들");
	m_vecFailureMent.push_back("더 노력해라...");

	m_vecSuccessMent.push_back("이럴수가!!!!!!!!");
	m_vecSuccessMent.push_back("나의 ... 나의.. 과제들이..!!!");
	m_vecSuccessMent.push_back("으아아아아아..~~~~");
	m_vecSuccessMent.push_back("아아아아~~~~");
	m_vecSuccessMent.push_back("끄으우아아아아아아아아~~~~");
	m_vecSuccessMent.push_back("...");
	m_vecSuccessMent.push_back("......");
	m_vecSuccessMent.push_back("죽을 줄 알았지?");
	m_vecSuccessMent.push_back("내가 몬스터냐? ㅋㅋ");
	m_vecSuccessMent.push_back("공부하느라 애썼다 ㅋ");
	m_vecSuccessMent.push_back("옛다 [졸업 메달]이다.");
	m_vecSuccessMent.push_back("다음 기수에 또보자~");
}

CBossObject::~CBossObject(void)
{
}

int CBossObject::ClearHomeworks(void)
{
	int nRemainedCount = 0;
	for (CHomeworkObject* pHomework : m_Homeworks)
	{
		if (!Objs()->IsExist(pHomework))
			continue;

		nRemainedCount++;
		pHomework->Destroy();
	}
	m_Homeworks.clear();
	return nRemainedCount;
}

bool CBossObject::IsClearAllHomework(void)
{
	for (CHomeworkObject* pHomework : m_Homeworks)
	{
		if (Objs()->IsExist(pHomework))
			return false;
	}
	return true;
}

void CBossObject::OnUpdate(double dElapsedSec)
{
	if (0 < m_dCoolTime)
	{
		m_dCoolTime -= dElapsedSec;
		return;
	}

	if (STATUS_INTRO == m_nStatus)
	{
		if (m_vecIntroMent.size() <= m_tSequence)
		{
			m_nStatus = STATUS_CHORES_ATTACK;
			m_tSequence = 0;
			m_dCoolTime = 2;
			return;
		}

		std::string strMessage = m_vecIntroMent[m_tSequence++];
		Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, strMessage, 2));
		m_dCoolTime = 2.5;
		return;
	}

	if (STATUS_FAILURE == m_nStatus)
	{
		ClearHomeworks();
		if (m_vecFailureMent.size() <= m_tSequence)
		{
			m_nStatus = STATUS_IDLE;
			m_tSequence = 0;
			m_dCoolTime = 2;
			return;
		}

		std::string strMessage = m_vecFailureMent[m_tSequence++];
		Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, strMessage, 2));
		m_dCoolTime = 2.5;
		return;
	}

	if (STATUS_SUCCESS == m_nStatus)
	{
		ClearHomeworks();
		if (m_vecSuccessMent.size() <= m_tSequence)
		{
			m_nStatus = STATUS_IDLE;
			m_tSequence = 0;
			m_dCoolTime = 2;
			return;
		}

		std::string strMessage = m_vecSuccessMent[m_tSequence++];
		Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, strMessage, 2));
		m_dCoolTime = 2.5;
		return;
	}

	if (STATUS_CHORES_ATTACK == m_nStatus)
	{
		const size_t tQuantity = 50;
		const size_t tSeq = m_tSequence++;

		if (0 == tSeq)
		{
			Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "사소한 할일 공격이다!!!!!", 1.5));
			m_dCoolTime = 1.5;
		}
		else if (1 == tSeq)
		{
			Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "30초 안에 모두 마무리해라!!", 2));
			m_dCoolTime = 0.5;
		}
		else if (tQuantity < tSeq)
		{
			size_t tElapsedSec = tSeq - tQuantity;
			m_dCoolTime = 1;

			if (30 == tElapsedSec || IsClearAllHomework())
			{
				if (0 < ClearHomeworks())
				{
					Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "사소한 것들이라고 간과하지마라", 2));
					m_dCoolTime = 2;

					m_nStatus = STATUS_FAILURE;
					m_tSequence = 0;
					return;
				}

				Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "아니?? 설마 이걸 다 해낼줄이야...", 2));
				m_dCoolTime = 2;

				m_nStatus = STATUS_HOMEWORK_ATTACK;
				m_tSequence = 0;
				return;
			}
			else if(25 <= tElapsedSec)
			{
				size_t tRemainTime = 30 - tElapsedSec;

				Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, Format("%u.....", tRemainTime), 2));
				m_dCoolTime = 2;
			}
		}
		else
		{
			double dSmallRadius = (double)m_tSequence * 0.1 + 1;
			double dBigRadius = (double)m_tSequence * 0.1 + 5;
			double dAngle = m_tSequence * 0.5;
			double x = m_PosX + sin(dAngle) * dSmallRadius;
			double y = m_PosY + cos(dAngle) * dSmallRadius;
			double endX = m_PosX + sin(dAngle) * dBigRadius;
			double endY = m_PosY + cos(dAngle) * dBigRadius;

			CHomeworkObject* pProject = new CHomeworkObject(x, y, endX, endY, 1, 'C');
			Objs()->AddObject(pProject);
			m_Homeworks.insert(pProject);
			m_dCoolTime = 0.1;
		}		

		return;
	}

	if (STATUS_HOMEWORK_ATTACK == m_nStatus)
	{
		size_t tSeq = m_tSequence++;

		if (0 == tSeq)
		{
			Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "개인 과제 공격이다!", 2));
			m_dCoolTime = 2.5;
		}
		else if (1 == tSeq)
		{
			Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "도와주다 걸리면 죽는다!", 2));
			m_dCoolTime = 1;
		}
		else if (2 == tSeq)
		{
			for (int nPlayerID: m_TargetPlayerIDs)
			{
				CPlayerObject* pPlayer = Objs()->GetPlayer(nPlayerID);
				if (nullptr == pPlayer)
					continue;

				ST_VECTOR stDiff = GetVector(pPlayer, this);
				ST_VECTOR stNormalizedDir = Normalize(stDiff);

				CHomeworkObject* pHomework = new CHomeworkObject(
					m_PosX + stNormalizedDir.x * 2,
					m_PosY + stNormalizedDir.y * 2,
					pPlayer->m_PosX,
					pPlayer->m_PosY,
					50, 'H',
					pPlayer);
				Objs()->AddObject(pHomework);
				m_Homeworks.insert(pHomework);
			}
			m_dCoolTime = 1;
		}
		else
		{
			m_dCoolTime = 1;

			if (30 == tSeq || IsClearAllHomework())
			{
				if (0 < ClearHomeworks())
				{
					Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "크하하 한명쯤은 멍청이가 있을줄 알았지", 2));
					m_dCoolTime = 2;

					m_nStatus = STATUS_FAILURE;
					m_tSequence = 0;
					return;
				}

				Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "뭐야.. 다 똑똑이들이었어?", 2));
				m_dCoolTime = 2;

				m_nStatus = STATUS_PROJECT_ATTACK;
				m_tSequence = 0;
				return;
			}
			else if (25 <= tSeq)
			{
				size_t tRemainTime = 30 - tSeq;

				Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, Format("%u.....", tRemainTime), 2));
				m_dCoolTime = 2;
			}
		}

		return;
	}

	if (STATUS_PROJECT_ATTACK == m_nStatus)
	{
		size_t tSeq = m_tSequence++;

		if (0 == tSeq)
		{
			Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "조별 과제 공격이다!", 2));
			m_dCoolTime = 2.5;
		}
		else if (1 == tSeq)
		{
			Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "30초 안에 해결해봐라!", 2));
			m_dCoolTime = 2.5;

			CHomeworkObject* pProject = new CHomeworkObject(m_PosX, m_PosY - 2, m_PosX, m_PosY - 15, 150, 'P');
			Objs()->AddObject(pProject);
			m_Homeworks.insert(pProject);
			m_dCoolTime = 1;
		}
		else 
		{
			m_dCoolTime = 1;

			if (30 == tSeq || IsClearAllHomework())
			{
				if (0 < ClearHomeworks())
				{
					Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "크크크 니 맘대로 되는게 하나도 없었을거다", 2));
					m_dCoolTime = 2;

					m_nStatus = STATUS_FAILURE;
					m_tSequence = 0;
					return;
				}

				Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "이 그지같은 조별 과제를 잘 해내다니..", 2));
				m_dCoolTime = 2;

				m_nStatus = STATUS_SUCCESS;
				m_tSequence = 0;
				return;
			}
			else if (25 <= tSeq)
			{
				size_t tRemainTime = 30 - tSeq;

				Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, Format("%u.....", tRemainTime), 2));
				m_dCoolTime = 2;
			}
		}

		return;
	}
}

void CBossObject::OnCollide(const CObjectSuper* pOther)
{
	//if (nullptr == dynamic_cast<const CEffectObject*>(pOther))
	//	return;

	if (STATUS_IDLE != m_nStatus)
		return;

	std::set<CPlayerObject*> setPlayers;
	Objs()->GetPlayers(m_PosX, m_PosY, 20, setPlayers);
	if (setPlayers.size() < 1)
	{
		Objs()->AddObject(new CTextObject(m_PosX, m_PosY + 2, "... 날 깨우려거든 최소 4인팟을 만들어와라", 2));
		return;
	}

	m_TargetPlayerIDs.clear();
	for (CPlayerObject* pPlayer : setPlayers)
		m_TargetPlayerIDs.insert(pPlayer->GetPlayerID());

	m_nStatus = STATUS_INTRO;
	m_tSequence = 0;
	m_dCoolTime = 0;
}
