#include "stdafx.h"
#include "ChatBox.h"
#include "Camera.h"
#include "../Client/GDIHelper.h"

CChatBox::CChatBox(void)
	: CBillboardSuper()
{
}

CChatBox::~CChatBox(void)
{
}

void CChatBox::Draw(
	const ST_VIEWPORT& viewport,
	std::vector<std::string>& refVideoMemory)
{
	CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();
	if (nullptr == pMyPlayer)
		return;

	/** Camera Position
	{
		std::string& refLine = refVideoMemory[2];
		char szBuffer[100] = { 0, };
		sprintf(szBuffer, "Camera: (%.0lf, %.0lf)", viewport.cx, viewport.cy);
		memcpy(&refLine[0], szBuffer, strlen(szBuffer));
	}
	*/
	{
		std::string& refLine = refVideoMemory[3];
		char szBuffer[100] = { 0, };
		sprintf(szBuffer, "Position: (%.0lf, %.0lf)", pMyPlayer->m_PosX, pMyPlayer->m_PosY);
		memcpy(&refLine[0], szBuffer, strlen(szBuffer));
	}
}

void CChatBox::DrawGDI(const ST_VIEWPORT viewport, HDC hDC)
{
	CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();
	if (nullptr == pMyPlayer)
		return;

	
	

	{
		std::string PositionStatus = string_format("Position: (%.0lf, %.0lf)", pMyPlayer->m_PosX, pMyPlayer->m_PosY);

		std::wstring playerPosition = S2WS(PositionStatus);
		//TextOut(hDC, 420, 570, playerPosition.c_str(), lstrlen(playerPosition.c_str()));
		RECT rt = { 70, 570, 1000,1000 };
		DrawText(hDC, playerPosition.c_str(), -1, &rt, DT_LEFT | DT_WORDBREAK | DT_NOCLIP);
	}
}
