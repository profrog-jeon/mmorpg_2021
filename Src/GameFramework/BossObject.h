#pragma once

#include <set>
#include "PlayerObject.h"
#include "HomeworkObject.h"

class CBossObject : public CObjectSuper
{
	enum E_STATUS
	{
		STATUS_IDLE,
		STATUS_INTRO,
		STATUS_CHORES_ATTACK,
		STATUS_HOMEWORK_ATTACK,
		STATUS_PROJECT_ATTACK,
		STATUS_FAILURE,
		STATUS_SUCCESS,
	};

	std::set<int> m_TargetPlayerIDs;
	std::set<CHomeworkObject*> m_Homeworks;

	std::vector<std::string> m_vecIntroMent;
	std::vector<std::string> m_vecFailureMent;
	std::vector<std::string> m_vecSuccessMent;

	E_STATUS m_nStatus;
	double m_dCoolTime;
	size_t m_tSequence;

public:
	CBossObject(void);
	~CBossObject(void);

private:
	int ClearHomeworks(void);
	bool IsClearAllHomework(void);

	void OnUpdate(double dElapsedSec);
	void OnCollide(const CObjectSuper* pOther);


};

