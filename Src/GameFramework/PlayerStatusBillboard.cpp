#include "stdafx.h"
#include "PlayerStatusBillboard.h"
#include "Camera.h"
#include "../Client/GDIHelper.h"

CPlayerStatusBillboard::CPlayerStatusBillboard(void)
	: CBillboardSuper()
{
}

CPlayerStatusBillboard::~CPlayerStatusBillboard(void)
{
}

void CPlayerStatusBillboard::Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer)
{
	CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();
	if (nullptr == pMyPlayer)
		return;

	{
		std::string hpStatus = Format("HP: %.0lf", pMyPlayer->m_HP);
		pRenderer->TextOut(450, 430, hpStatus);
	}

	{
		std::string mpStatus = Format("MP: %.0lf", pMyPlayer->m_MP);
		pRenderer->TextOut(450, 450, mpStatus);
	}

	{
		std::string PositionStatus = Format("Position: (%.0lf, %.0lf)", pMyPlayer->m_PosX, pMyPlayer->m_PosY);
		pRenderer->TextOut(420, 470, PositionStatus);
	}
}
