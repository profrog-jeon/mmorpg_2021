#pragma once

#include "ObjectSuper.h"

class CEffectObject : public CObjectSuper
{
	DWORD m_dwTimeOutTick;

public:
	int m_nPlayerID;

	CEffectObject(E_OBJECT_TYPE nType, double power, double size, double x, double y, double vx, double vy, double duration);
	bool IsDestroyed(void);
	void OnCollide(const CObjectSuper* pOther);
};

class CMissileObject : public CEffectObject
{
public:
	CMissileObject(void);
	CMissileObject(double power, double x, double y, double vx, double vy, double size, COLORREF color = RGB(0, 0, 0));
};
