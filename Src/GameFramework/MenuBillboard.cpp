#include "stdafx.h"
#include "MenuBillboard.h"
#include "../Client/GDIHelper.h"

CMenuBillboard::CMenuBillboard()
{
	isClosed = true;
	index = 0;
	m_pQuestListBillboard = nullptr;

	m_vecMenues.push_back("퀘스트 목록");
	m_vecMenues.push_back("접속자 목록");
	m_vecMenues.push_back("게임 종료");
}

void CMenuBillboard::Prev(void)
{
	if (index != 0)
		index--;
}

void CMenuBillboard::Next(void)
{
	if (index < (int)m_vecMenues.size() - 1)
		index++;
}

void CMenuBillboard::Open()
{
	isClosed = false;
}

void CMenuBillboard::Close()
{
	isClosed = true;
}

void CMenuBillboard::Select()
{
	switch (index)
	{
	case 0:
		m_pQuestListBillboard = Objs()->GetMyPlayer()->GetQuestList();
		m_pQuestListBillboard->Open();
		Objs()->AddFrontBillboard(m_pQuestListBillboard);
		break;
	case 1:
		break;
	case 2:
		ExitProcess(0);
		break;
	}
}

bool CMenuBillboard::IsDestroyed(void)
{
	return isClosed;
}

void CMenuBillboard::Draw(const ST_VIEWPORT& viewport, std::vector<std::string>& refVideoMemory)
{
	const size_t tRaw = refVideoMemory.size();
	const size_t numOfMenus = m_vecMenues.size();
	const size_t pos_Menus = tRaw - numOfMenus -2;

	for (int i = 0; i < numOfMenus + 2; i++)
	{
		if (index + 1 == i) {
			refVideoMemory[pos_Menus + i][0] = MARK_WHITE;   // background를 위한 extended ASCII
			memset((void*)(refVideoMemory[pos_Menus + i].c_str() + 1), '.', refVideoMemory[pos_Menus + i].length() - 1);
		}
		else {
			memset((void*)refVideoMemory[pos_Menus + i].c_str(), '.', refVideoMemory[pos_Menus + i].length() - 1);
		}
	}

	if (m_vecMenues.empty())
		return;

	for (int i = 0; i < numOfMenus; i++)
	{
		std::string& strMenu = m_vecMenues.at(i);
		if (index == i) {
			memcpy((void*)(refVideoMemory[pos_Menus + i + 1].c_str() + 3), strMenu.c_str(), strMenu.length());
		}
		else {
			memcpy((void*)(refVideoMemory[pos_Menus + i + 1].c_str() + 2), strMenu.c_str(), strMenu.length());
		}
	}
}

void CMenuBillboard::DrawGDI(const ST_VIEWPORT viewport, HDC hDC)
{
	if (m_vecMenues.empty())
		return;

	ST_PEN Pen(hDC, RGB(0, 0, 0));

	const size_t numOfMenus = m_vecMenues.size();
	const size_t heightOfText = 20;

	for (int i = 0; i < numOfMenus; i++) {
		std::wstring strMessage = S2WS(m_vecMenues[i]);
		SetTextAlign(hDC, TA_CENTER);

		if (i == index)
			Rectangle(hDC, g_nPixelCountX / 6, g_nPixelCountY + (g_nDialogCountY / 2) - heightOfText * (numOfMenus / 2 - i) - 2,
				g_nPixelCountX / 6 * 5, g_nPixelCountY + (g_nDialogCountY / 2) - heightOfText * (numOfMenus / 2 - i - 1) - 2);

		TextOut(hDC, g_nPixelCountX / 2,
			g_nPixelCountY + (g_nDialogCountY / 2) - heightOfText * (numOfMenus / 2 - i),
			strMessage.c_str(), lstrlen(strMessage.c_str()));
	}
}