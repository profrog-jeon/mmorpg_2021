#pragma once

#include "ObjectSuper.h"

enum E_ANIMATE_TYPE
{
	ANIMATE_NONE = 0,
	ANIMATE_RISE,
};

class CTextObject : public CObjectSuper
{
	DWORD m_dwTimeOutTick;
	std::string m_strText;
	E_ANIMATE_TYPE m_nAnimateType;

	double m_TargetPosX;
	double m_TargetPosY;

public:
	CTextObject(double x, double y, std::string strText, double durationTime, E_ANIMATE_TYPE nAnimate = ANIMATE_NONE);
	~CTextObject(void);

	bool IsDestroyed(void);
	void OnUpdate(double dElapsedSec);
	void OnDraw(int nLeft, int nTop, int nRight, int nBottom, std::vector<std::string>& refVideoMemory);
	void Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer);
};

