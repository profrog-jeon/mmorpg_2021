#pragma once

#include "Struct.h"
#include "RendererSuper.h"

class CObjectSuper : public ST_OBJECT_DATA
{
	friend class CKeyInputEngine;
	friend class CConsoleRenderer;

	friend class CPositionUpdateEngine;
	friend class CGravityUpdateEngine;
	friend class CObjectUpdateEngine;
	friend class CCameraTracingUpdateEngine;
	friend class CCollisionDetectEngine;

	friend bool CheckCollision(const CObjectSuper* a, const CObjectSuper* b);
	friend bool CalculateCollision(const CObjectSuper* a, const CObjectSuper* b);

protected:
	std::string m_strName;
	std::string m_strGreetingMessage;
	COLORREF m_Color;

	CObjectSuper(void);
	CObjectSuper(E_OBJECT_TYPE nType);

public:
	virtual ~CObjectSuper(void);

	std::string GetName(void);
	std::string GetGreetingMsg(void);

	virtual bool IsDestroyed(void);
	virtual void OnUpdate(double dElapsedSec);
	virtual void Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer);
	virtual void OnCollide(const CObjectSuper* pOther);
};

