#pragma once

#include "../../../quest/Src/QuestFramework/QuestFramework.h"
#include "ObjectSuper.h"

class CNpcObject : public CObjectSuper
{
	int m_nNpcID;

public:
	CNpcObject(const ST_NPC_INFO& info);
	~CNpcObject(void);

	int GetID(void);
	void OnCollide(const CObjectSuper* pOther);
};


