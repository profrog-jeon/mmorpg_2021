#include "stdafx.h"
#include "MathHelper.h"

double GetDistance(const ST_OBJECT_DATA* pTo, const ST_OBJECT_DATA* pFrom)
{
	double dDiffX = pTo->m_PosX - pFrom->m_PosX;
	double dDiffY = pTo->m_PosY - pFrom->m_PosY;

	double dDistanceSqr = dDiffX * dDiffX + dDiffY * dDiffY;
	return sqrt(dDistanceSqr);
}

double GetDistance(const ST_VECTOR& to, const ST_VECTOR& from)
{
	double dDiffX = to.x - from.x;
	double dDiffY = to.y - from.y;

	double dDistanceSqr = dDiffX * dDiffX + dDiffY * dDiffY;
	return sqrt(dDistanceSqr);
}

ST_VECTOR GetVector(const ST_OBJECT_DATA* pTo, const ST_OBJECT_DATA* pFrom)
{
	ST_VECTOR stRet;
	stRet.x = pTo->m_PosX - pFrom->m_PosX;
	stRet.y = pTo->m_PosY - pFrom->m_PosY;
	return stRet;
}

ST_VECTOR Normalize(const ST_VECTOR& stVector)
{
	ST_VECTOR stZero = { 0, 0 };
	double dDistance = GetDistance(stVector, stZero);

	ST_VECTOR stRet = { stVector.x / dDistance, stVector.y / dDistance };
	return stRet;
}
