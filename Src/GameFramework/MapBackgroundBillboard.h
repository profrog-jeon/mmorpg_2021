#pragma once

#include "BillboardSuper.h"

class CMapBackgroundBillboard : public CBillboardSuper
{
public:
	CMapBackgroundBillboard(void);
	~CMapBackgroundBillboard(void);

private:
	void Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer);
};
