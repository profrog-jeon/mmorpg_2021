#pragma once

#include <string>
#include <Windows.h>

void Log(LPCSTR pszFormat, ...);
void Log(LPCWSTR pszFormat, ...);
void Debug(LPCSTR pszFormat, ...);

std::string Format(LPCSTR pszFormat, ...);
