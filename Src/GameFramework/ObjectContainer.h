#pragma once

#include <list>
#include <set>
#include "../NetworkFramework/NetworkFramework.h"
#include "../ProtocolFramework/ProtocolFramework.h"
#include "ObjectSuper.h"
#include "BillboardSuper.h"
#include "PlayerObject.h"

class CObjectContainer
{
	CCriticalSection m_csPlayersMap;
	std::map<int, CPlayerObject*> m_Players;
	CPlayerObject* m_pMyPlayer;

	int m_nMyPlayerID;
	int m_CameraWarp = false;

	CObjectContainer(void);
	~CObjectContainer(void);

public:
	std::list<CBillboardSuper*> BackBillboardList;
	std::list<CBillboardSuper*> FrontBillboardList;

	CCriticalSection csObjectList;
	std::list<CObjectSuper*> ObjectList;

	bool IsExist(CObjectSuper* pObject);
	void DestroyObject(void);
	void DestroyAll(void);
	void AddBackBillboard(CBillboardSuper* pNewBillboad);
	void AddFrontBillboard(CBillboardSuper* pNewBillboad);
	void AddObject(CObjectSuper* pNewObject);
	void AddObject(CObjectSuper* pNewObject, const ST_OBJECT_DATA& data);
	void AddObjects(const ST_GAME_INFO& gameInfo);
	void AddPlayer(CPlayerObject* pNewPlayer);
	void RemovePlayer(int nID);

	void SetMyPlayer(int nUserID);
	bool ShouldWarpCamera();
	CPlayerObject* GetMyPlayer(void);
	CPlayerObject* GetPlayer(int nUserID);
	void GetPlayers(double x, double y, double radius, std::set<CPlayerObject*>& players);
	void QueryObjects(ST_GAME_INFO& outGameInfo);
	CObjectSuper* FindObject(double x, double y, E_OBJECT_TYPE nType = OBJECT_TYPE_UNDEFINED);
	int CountMonster(int nMonsterID);

	static CObjectContainer* GetInstance(void)
	{
		static CObjectContainer instance;
		return &instance;
	}
};

inline CObjectContainer* Objs(void)
{
	return CObjectContainer::GetInstance();
}
