#include "stdafx.h"
#include "CameraTracingUpdateEngine.h"
#include "Camera.h"

CCameraTracingUpdateEngine::CCameraTracingUpdateEngine(void)
{
}

void CCameraTracingUpdateEngine::Update(double dElaspedSec)
{
	CPlayerObject* pMyPlayer = Objs()->GetMyPlayer();
	if (nullptr == pMyPlayer)
		return;
	else if (Objs()->ShouldWarpCamera())
	{
		Camera()->m_CenterPosX = pMyPlayer->m_PosX;
		Camera()->m_CenterPosY = pMyPlayer->m_PosY;
	}
	else
	{
		Camera()->m_CenterPosX += (pMyPlayer->m_PosX - Camera()->m_CenterPosX) * dElaspedSec;
		Camera()->m_CenterPosY += (pMyPlayer->m_PosY - Camera()->m_CenterPosY) * dElaspedSec;
	}
}
