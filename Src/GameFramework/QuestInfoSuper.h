#pragma once

#include "Struct.h"
#include "../../../quest/Src/QuestFramework/QuestFramework.h"

struct CQuestInfoSuper : public ST_QUEST_DATA
{
protected:
	int m_nIndex;

public:
	CQuestInfoSuper(const ST_QUEST_DATA& stData);
	virtual ~CQuestInfoSuper(void);

	int GetID(void);
	bool PeekLastMessage(ST_USER_QUESTINFO& userInfo, ST_NPC_MESSAGE& outMessage);
	void Process(ST_USER_QUESTINFO& userInfo, std::vector<ST_NPC_MESSAGE>& outMessage);
	void QueryMessageHistroy(ST_USER_QUESTINFO& userInfo, std::vector<ST_NPC_MESSAGE>& outMessage);

	bool IsCleared(void);
	bool IsProcessed(const ST_USER_QUESTINFO* pUserQuestInfo);
};

