#pragma once

#include "BillboardSuper.h"

class CChatBox : public CBillboardSuper
{
public:
	CChatBox(void);
	~CChatBox(void);

	void Draw(const ST_VIEWPORT& viewport, std::vector<std::string>& refVideoMemory);
	virtual void DrawGDI(const ST_VIEWPORT viewport, HDC hDC);
};

