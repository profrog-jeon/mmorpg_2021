#include "stdafx.h"
#include "MapBackgroundBillboard.h"
#include "../Client/GDIHelper.h"

CMapBackgroundBillboard::CMapBackgroundBillboard(void)
{
	m_Weight = 1;
}

CMapBackgroundBillboard::~CMapBackgroundBillboard(void)
{
}

void CMapBackgroundBillboard::Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer)
{
	for (int y = 0; y <= 260; y++)
	{
		for (int x = 0; x <= 260; x++)
		{
			if (!Map()->Data[260 - y][x])
				continue;

			double left = (x - viewport.cx - 0.5) / viewport.scale + g_nPixelCountX / 2;
			double right = (x - viewport.cx + 0.5)/ viewport.scale + g_nPixelCountX / 2;
			double top = -(y - viewport.cy + 0.5) / viewport.scale + g_nPixelCountY / 2;
			double bottom = -(y - viewport.cy - 0.5) / viewport.scale + g_nPixelCountY / 2;

			if (right < 0)
				continue;
			if (g_nPixelCountX < left)
				continue;
			if (bottom < 0)
				continue;
			if (g_nPixelCountY < top)
				continue;
			
			pRenderer->Rectangle(left, top, right, bottom, RGB(158, 158, 158), '#');
		}
	}
}
