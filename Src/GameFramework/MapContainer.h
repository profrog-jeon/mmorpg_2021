#pragma once

class CMapContainer
{
	CMapContainer(void);
	~CMapContainer(void);

public:
	bool IsBlocked(double x, double y);
	
	static char Data[261][261];
	static CMapContainer* GetInstance(void)
	{
		static CMapContainer instance;
		return &instance;
	}
};

inline CMapContainer* Map(void)
{
	return CMapContainer::GetInstance();
}
