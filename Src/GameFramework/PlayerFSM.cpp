#include "stdafx.h"
#include "PlayerFSM.h"

CPlayerFSM::CPlayerFSM(void)
	: CObjectSuper(OBJECT_TYPE_PLAYER)
	, m_nState(PLAYER_STATE_IDLE)
	, m_bLeft(false)
	, m_bRight(false)
	, m_bUp(false)
	, m_bDown(false)
{
}

CPlayerFSM::~CPlayerFSM(void)
{
}

void CPlayerFSM::OnLeftDown(void)
{
	m_bLeft = true;
	m_nState = ChangeState(m_nState, PLAYER_EVENT_LEFT_DOWN);
}

void CPlayerFSM::OnLeftUp(void)
{
	m_bLeft = false;
	m_nState = ChangeState(m_nState, PLAYER_EVENT_LEFT_UP);
}

void CPlayerFSM::OnRightDown(void)
{
	m_bRight = true;
	m_nState = ChangeState(m_nState, PLAYER_EVENT_RIGHT_DOWN);
}

void CPlayerFSM::OnRightUp(void)
{
	m_bRight = false;
	m_nState = ChangeState(m_nState, PLAYER_EVENT_RIGHT_UP);
}

void CPlayerFSM::OnUpDown(void)
{
	m_bUp = true;
	m_nState = ChangeState(m_nState, PLAYER_EVENT_UP_DOWN);
}

void CPlayerFSM::OnUpUp(void)
{
	m_bUp = false;
	m_nState = ChangeState(m_nState, PLAYER_EVENT_UP_UP);
}

void CPlayerFSM::OnDownDown(void)
{
	m_bDown = true;
	m_nState = ChangeState(m_nState, PLAYER_EVENT_DOWN_DOWN);
}

void CPlayerFSM::OnDownUp(void)
{
	m_bDown = false;
	m_nState = ChangeState(m_nState, PLAYER_EVENT_DOWN_UP);
}

void CPlayerFSM::OnLanded(void)
{
	m_nState = ChangeState(m_nState, PLAYER_EVENT_LANDED);
}

double Normalize(double v)
{
	if (v < 0)
		return -1;
	if (v > 0)
		return 1;
	return 0;
}

void CPlayerFSM::OnCollide(const CObjectSuper* pOther)
{
	m_nState = ChangeState(m_nState, PLAYER_EVENT_COLLID);
}

E_PLAYER_STATE CPlayerFSM::ChangeState(E_PLAYER_STATE nCurState, E_PLAYER_EVENT nEvent)
{
	const double jumpVel = 15;
	E_PLAYER_STATE nNewState = nCurState;

	switch (nCurState)
	{
	case PLAYER_STATE_IDLE:
	case PLAYER_STATE_WALKING:
		if (PLAYER_EVENT_LEFT_DOWN == nEvent)
		{
			m_VelX = -m_Speed;
			nNewState = PLAYER_STATE_WALKING;
		}
		if (PLAYER_EVENT_RIGHT_DOWN == nEvent)
		{
			m_VelX = m_Speed;
			nNewState = PLAYER_STATE_WALKING;
		}
		if (PLAYER_EVENT_UP_DOWN == nEvent)
		{
			m_VelY = m_Speed;
			nNewState = PLAYER_STATE_WALKING;
		}
		if (PLAYER_EVENT_DOWN_DOWN == nEvent)
		{
			m_VelY = -m_Speed;
			nNewState = PLAYER_STATE_WALKING;
		}

		if (!m_bLeft && !m_bRight)
		{
			m_VelX = 0;
		}
		if (!m_bUp && !m_bDown)
		{
			m_VelY = 0;
		}

		if (PLAYER_EVENT_COLLID == nEvent)
		{
			m_VelX = 0;
			m_VelY = 0;
		}

		if (0 == m_VelX && 0 == m_VelY)
		{
			nNewState = PLAYER_STATE_IDLE;
		}
		break;

	case PLAYER_STATE_DEAD:
		break;
	}
	return nNewState;
}
