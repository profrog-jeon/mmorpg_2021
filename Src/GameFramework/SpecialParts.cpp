#include "stdafx.h"
#include "SpecialParts.h"

const char* GetPartsName(unsigned char cPartsMask)
{
    switch (cPartsMask)
    {
    case PARTS_SPEEDUP1: return "�⼭�� GTX 1060";
    case PARTS_SPEEDUP2: return "������ RTX 2070";
    case PARTS_SPEEDUP3: return "������ RTX 2070 Ti";
    case PARTS_SPEEDUP4: return "�ֿ��� RTX 3080";
    case PARTS_SPEEDUP5: return "������ RTX 3090";
    case PARTS_MULTISHOT: return "�¹��� �ӽ�/*����*/��(���� 3�߾� ������)";
    case PARTS_POWERUP: return "��ǥ�� ����������(ũ��Ƽ���� �̻���)";
    case PARTS_BIGSHOT: return "�������� BFG9000(ũ�� �Ƹ��ٿ� �̻���)";
    default:
        return nullptr;
    }
    return nullptr;
}
