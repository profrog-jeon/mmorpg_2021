#pragma once

#include <string>

class CRendererSuper
{
protected:
	CRendererSuper(void);
	virtual ~CRendererSuper(void);

public:
	virtual void Clear(void) = 0;
	virtual void Flush(void) = 0;

	virtual void Rectangle(int left, int top, int right, int bottom, COLORREF color, char patch) = 0;
	virtual void Ellipse(int left, int top, int right, int bottom, COLORREF color, char patch) = 0;
	virtual void TextOut(int cx, int cy, std::string text) = 0;
};

