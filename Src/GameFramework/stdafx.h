#pragma once

#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <vector>
#include <string>
#include <algorithm>
#include <conio.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>

#include "Define.h"
#include "Struct.h"
#include "Camera.h"
#include "ObjectContainer.h"
#include "GlobalTimer.h"
#include "DebugLog.h"
#include "MathHelper.h"
#include "MapContainer.h"
#include "../ProtocolFramework/ProtocolFramework.h"