#pragma once

#include <map>
#include "../../../quest/Src/QuestFramework/QuestFramework.h"
#include "QuestInfoSuper.h"

class CQuestContainer
{
	std::map<int, ST_NPC_INFO> m_NpcMap;
	std::multimap<int, CQuestInfoSuper*> m_QuestMap;

	CQuestContainer(void);
	~CQuestContainer(void);

public:

	void Load(void);
	ST_NPC_INFO* GetNPC(int nNpcID);
	CQuestInfoSuper* GetQuest(int nNpcID);
	void QueryQuests(const ST_USER_QUESTINFO* pUserQuestInfo, std::vector<CQuestInfoSuper*>& outQuestInfo);
	int GetClearCount(const ST_USER_QUESTINFO* pUserQuestInfo);

	static CQuestContainer* GetInstance(void)
	{
		static CQuestContainer instance;
		return &instance;
	}
};

inline CQuestContainer* QuestContainer(void)
{
	return CQuestContainer::GetInstance();
}