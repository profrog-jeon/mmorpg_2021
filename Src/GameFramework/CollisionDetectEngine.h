#pragma once

#include "UpdateEngineSuper.h"

class CCollisionDetectEngine : public CUpdateEngineSuper
{
public:
	CCollisionDetectEngine(void);
	~CCollisionDetectEngine(void);

private:
	void Update(double dElaspedSec);
};

