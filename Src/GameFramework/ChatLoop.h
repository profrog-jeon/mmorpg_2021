#pragma once

class CChatLoop
{
protected:
	CChatLoop(void);
	~CChatLoop(void);

	virtual bool OnFrontLoop(DWORD dwGameTime) = 0;
	virtual bool OnBackLoop(DWORD dwGameTime) = 0;

public:
	int Loop(bool bSync);
};