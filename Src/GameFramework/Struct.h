#pragma once

#include "../../../quest/Src/QuestFramework/QuestFramework.h"

#pragma pack(push, 1)

enum E_KEY_ID
{
	KEY_ID_LEFT = 0,
	KEY_ID_RIGHT,
	KEY_ID_UP,
	KEY_ID_DOWN,
	KEY_ID_SPACE,
	KEY_ID_ESC,
	KEY_ID_ENTER,
	KEY_ID_COUNT,
	KEY_ID_UNDEFINED,
};

enum E_OBJECT_TYPE
{
	OBJECT_TYPE_UNDEFINED = 0,
	OBJECT_TYPE_MONSTER,
	OBJECT_TYPE_PLAYER,
	OBJECT_TYPE_MISSILE,
	OBJECT_TYPE_NPC,
	OBJECT_TYPE_HOMEWORK,
};

struct ST_OBJECT_DATA
{
	E_OBJECT_TYPE m_Type;	
	double	m_PosX;
	double	m_PosY;
	double	m_VelX;
	double	m_VelY;
	double	m_Height;
	double	m_Width;
	double	m_Weight;
	double 	m_Power;
	char	m_cPatch;
	double m_Speed;

	ST_OBJECT_DATA(void)
		: m_Type(OBJECT_TYPE_UNDEFINED)
		, m_PosX(0), m_PosY(0), m_VelX(0), m_VelY(0)
		, m_Height(0), m_Width(0), m_Weight(0), m_Power(0), m_cPatch('*')
	{}
};

struct ST_PLAYER_KEY_STATE
{
	bool isDown[KEY_ID_COUNT];

	ST_PLAYER_KEY_STATE(void)
		: isDown{ false, }
	{}
};

const int g_nMaxUserNameLen = 20;
struct ST_PLAYER_DATA : public ST_PLAYER_KEY_STATE, public ST_USER_QUESTINFO
{
	char m_szUserName[g_nMaxUserNameLen+1];
	int m_nPlayerID;
	double m_Angle;
	unsigned char m_SpecialParts;

	ST_PLAYER_DATA(void)
		: ST_PLAYER_KEY_STATE(), ST_USER_QUESTINFO()
		, m_szUserName{ 0, }, m_nPlayerID(-1)
		, m_Angle(0)
		, m_SpecialParts(0x00)
	{}
};

struct ST_PLAYER_POS : public ST_PLAYER_KEY_STATE, public ST_USER_QUESTINFO
{
	double x;
	double y;
	double z;

	ST_PLAYER_POS(void)
		: ST_PLAYER_KEY_STATE(), ST_USER_QUESTINFO()
		, x(0), y(0), z(0)
			{}
};

struct ST_VIEWPORT
{
	double left;
	double right;
	double top;
	double bottom;
	double cx;
	double cy;
	double scale;
};

enum E_RENDERER_BACKGROUND_MARK
{
	MARK_WHITE = '\xb2'
};

#pragma pack(pop)