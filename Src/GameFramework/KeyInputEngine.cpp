#include "stdafx.h"
#include "KeyInputEngine.h"
#include "PlayerObject.h"
#include "ObjectContainer.h"
#include "Camera.h"
#include "NpcObject.h"
#include "QuestContainer.h"


CKeyInputEngine::CKeyInputEngine(void)
	: m_pDialogBillboard(nullptr)
{
	tempHwnd = ::FindWindow(NULL, NULL);

	while (tempHwnd != NULL)
	{
		if (::GetParent(tempHwnd) == NULL) {
			DWORD pid;
			::GetWindowThreadProcessId(tempHwnd, &pid);
			if (::GetCurrentProcessId() == pid) {
				return;
			}
		}
		tempHwnd = ::GetWindow(tempHwnd, GW_HWNDNEXT);
	}
}

CKeyInputEngine::~CKeyInputEngine(void)
{
}

CKeyInputEngine::E_KEY_EVENT CKeyInputEngine::GetKeyEvent(int nVirtKey)
{
	const short nPreState = m_mapKey[nVirtKey];
	const short nCurState = GetAsyncKeyState(nVirtKey);
	const bool bIsPressed = nPreState & 0x8000;
	const bool bIsPressing = nCurState & 0x8000;

	m_mapKey[nVirtKey] = nCurState;

	if (bIsPressed && !bIsPressing)
		return KEY_EVENT_UP;

	if (!bIsPressed && bIsPressing)
		return KEY_EVENT_DOWN;

	return KEY_EVENT_NONE;
}

boolean CKeyInputEngine::isFocused() {

	return (::GetForegroundWindow() == tempHwnd);
}

boolean CKeyInputEngine::isChatted()
{
	const E_KEY_EVENT nEnterKey = GetKeyEvent(VK_RETURN);

	return (nEnterKey == KEY_EVENT_DOWN) && CKeyInputEngine::isFocused();
}


int CKeyInputEngine::Query(ST_INPUT_INFO* outInputArr)
{
	if (!CKeyInputEngine::isFocused())
		return 0;

	int nCount = 0;
	const E_KEY_EVENT nSelectKey = GetKeyEvent(' ');
	const E_KEY_EVENT nLeftKey = GetKeyEvent(VK_LEFT);
	const E_KEY_EVENT nRightKey = GetKeyEvent(VK_RIGHT);
	const E_KEY_EVENT nUpKey = GetKeyEvent(VK_UP);
	const E_KEY_EVENT nDownKey = GetKeyEvent(VK_DOWN);

	if (KEY_EVENT_DOWN == nSelectKey && nullptr == m_pDialogBillboard)
	{
		CPlayerObject* pPlayer = Objs()->GetMyPlayer();
		m_pDialogBillboard = pPlayer->ChatToNpc();

		// Select 키를 대화걸기에 사용할 때는 서버로 전송하지 않음
		if (nullptr != m_pDialogBillboard)
		{
			Objs()->AddFrontBillboard(m_pDialogBillboard);

			// stop the charactor
			outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_LEFT);
			outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_RIGHT);
			outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_UP);
			outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_DOWN);
			return 0;
		}
	}

	if (m_pDialogBillboard)
	{
		if (KEY_EVENT_DOWN == nSelectKey)
			m_pDialogBillboard->Next();

		if (m_pDialogBillboard->IsDestroyed())
			m_pDialogBillboard = NULL;

		return 0;
	}

	if (KEY_EVENT_DOWN == nLeftKey)		outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_LEFT);
	if (KEY_EVENT_UP == nLeftKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_LEFT);
	if (KEY_EVENT_DOWN == nRightKey)	outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_RIGHT);
	if (KEY_EVENT_UP == nRightKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_RIGHT);
	if (KEY_EVENT_DOWN == nUpKey)		outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_UP);
	if (KEY_EVENT_UP == nUpKey)			outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_UP);
	if (KEY_EVENT_DOWN == nDownKey)		outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_DOWN);
	if (KEY_EVENT_UP == nDownKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_DOWN);
	if (KEY_EVENT_DOWN == nSelectKey)	outInputArr[nCount++] = ST_INPUT_INFO(true, KEY_ID_SELECT);
	if (KEY_EVENT_UP == nSelectKey)		outInputArr[nCount++] = ST_INPUT_INFO(false, KEY_ID_SELECT);

	if (!CKeyInputEngine::isFocused())
		return 0;

	return nCount;
}

void CKeyInputEngine::Update(int nUserID, const ST_INPUT_INFO* inputArr, int nCount)
{
	CPlayerObject* pPlayer = Objs()->GetPlayer(nUserID);
	if (nullptr == pPlayer)
		return;

	ST_PLAYER_KEY_STATE& keyState = *static_cast<ST_PLAYER_KEY_STATE*>(pPlayer);

	int i;
	for (i = 0; i < nCount; i++)
	{
		const ST_INPUT_INFO& curInputInfo = inputArr[i];
		keyState.isDown[curInputInfo.wKey] = curInputInfo.bDown;

		if (curInputInfo.bDown)
		{
			if (KEY_ID_LEFT == curInputInfo.wKey)		pPlayer->OnLeftDown();
			if (KEY_ID_RIGHT == curInputInfo.wKey)		pPlayer->OnRightDown();
			if (KEY_ID_UP == curInputInfo.wKey)			pPlayer->OnUpDown();
			if (KEY_ID_DOWN == curInputInfo.wKey)		pPlayer->OnDownDown();
			if (KEY_ID_SELECT == curInputInfo.wKey)		pPlayer->Skill();
		}
		else
		{
			if (KEY_ID_LEFT == curInputInfo.wKey)		pPlayer->OnLeftUp();
			if (KEY_ID_RIGHT == curInputInfo.wKey)		pPlayer->OnRightUp();
			if (KEY_ID_UP == curInputInfo.wKey)			pPlayer->OnUpUp();
			if (KEY_ID_DOWN == curInputInfo.wKey)		pPlayer->OnDownUp();
		}
	}
}
