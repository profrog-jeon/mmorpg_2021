#include "stdafx.h"
#include "ObjectContainer.h"
#include "HomeworkObject.h"
#include "NpcObject.h"
#include "MonsterObject.h"

CObjectContainer::CObjectContainer(void)
	: m_csPlayersMap()
	, m_Players()
	, m_pMyPlayer(nullptr)
	, ObjectList()
	, m_nMyPlayerID(-1)
{
}

CObjectContainer::~CObjectContainer(void)
{
}

bool CObjectContainer::IsExist(CObjectSuper* pTarget)
{
	CCriticalSection::Owner lock(csObjectList);
	for (CObjectSuper* pObject : ObjectList)
	{
		if (pObject == pTarget)
			return true;
	}
	return false;
}

void CObjectContainer::DestroyObject(void)
{
	bool bRemoved = false;
	do
	{
		bRemoved = false;
		for (auto iter = ObjectList.begin(); iter != ObjectList.end(); iter++)
		{
			if (OBJECT_TYPE_NPC == (*iter)->m_Type)
				continue;

			delete* iter;
			ObjectList.erase(iter);
			bRemoved = true;
			break;
		}
	} while (bRemoved);
}

void CObjectContainer::DestroyAll(void)
{
	for (auto iter = BackBillboardList.begin(); iter != BackBillboardList.end(); iter++)
		delete* iter;
	BackBillboardList.clear();

	for (auto iter = FrontBillboardList.begin(); iter != FrontBillboardList.end(); iter++)
		delete* iter;
	FrontBillboardList.clear();

	for (auto iter = ObjectList.begin(); iter != ObjectList.end(); iter++)
		delete* iter;
	ObjectList.clear();
}

void CObjectContainer::AddBackBillboard(CBillboardSuper* pNewBillboad)
{
	BackBillboardList.push_back(pNewBillboad);
}

void CObjectContainer::AddFrontBillboard(CBillboardSuper* pNewBillboad)
{
	FrontBillboardList.push_back(pNewBillboad);
}

void CObjectContainer::AddObject(CObjectSuper* pNewObject)
{
	ObjectList.push_back(pNewObject);
}

void CObjectContainer::AddObject(CObjectSuper* pNewObject, const ST_OBJECT_DATA& data)
{
	ST_OBJECT_DATA* pNewObjectData = dynamic_cast<ST_OBJECT_DATA*>(pNewObject);
	*pNewObjectData = data;
	ObjectList.push_back(pNewObject);
}

void CObjectContainer::AddPlayer(CPlayerObject* pNewPlayer)
{
	{
		CCriticalSection::Owner lock(csObjectList);
		ObjectList.push_back(pNewPlayer);
	}

	{
		CCriticalSection::Owner lock(m_csPlayersMap);
		m_Players.insert(std::make_pair(pNewPlayer->GetPlayerID(), pNewPlayer));
		m_pMyPlayer = GetPlayer(m_nMyPlayerID);
	}
}

void CObjectContainer::RemovePlayer(int nID)
{
	CPlayerObject* pPlayer = nullptr;
	{
		CCriticalSection::Owner lock(m_csPlayersMap);
		auto iter = m_Players.find(nID);
		if (iter == m_Players.end())
		{
			Log("Removing player has failed, id:%d is not found", nID);
			return;
		}

		pPlayer = iter->second;
		m_Players.erase(iter);
	}

	{
		CCriticalSection::Owner lock(csObjectList);
		ObjectList.remove(pPlayer);
	}

	delete pPlayer;
}

#include "MissileObject.h"
void CObjectContainer::AddObjects(const ST_GAME_INFO& gameInfo)
{
	CCriticalSection::Owner lock1(csObjectList);
	CCriticalSection::Owner lock2(m_csPlayersMap);

	int nPlayerIndex = 0;
	int nMonsterIndex = 0;
	int i;
	for (i = 0; i < gameInfo.nObjectCount; i++)
	{
		const ST_OBJECT_DATA& objData = gameInfo.objects[i];
		switch (objData.m_Type)
		{
		case OBJECT_TYPE_PLAYER:
		{
			int nIndex = nPlayerIndex++;

			CPlayerObject* pPlayer = new CPlayerObject(gameInfo.playerData[nIndex]);

			ST_OBJECT_DATA* pObjectData = static_cast<ST_OBJECT_DATA*>(pPlayer);
			*pObjectData = objData;

			AddPlayer(pPlayer);
			break;
		}
		case OBJECT_TYPE_MISSILE:	AddObject(new CMissileObject(), objData);	break;
			//case OBJECT_TYPE_NPC:			AddObject(new CNpcObject(), objData);			break;	// 항상 기본으로 존재함
		case OBJECT_TYPE_HOMEWORK:	AddObject(new CHomeworkObject(objData), objData);	break;
		case OBJECT_TYPE_MONSTER:
		{
			int nIndex = nMonsterIndex++;
			CMonsterObject* pMonster = new CMonsterObject(objData, gameInfo.monsterData[nIndex]);
			AddObject(pMonster);
			break;
		}
		default:
			Log("[ERROR] Undefined object type:%d found", objData.m_Type);
			break;
		}
	}
}

void CObjectContainer::SetMyPlayer(int nUserID)
{
	m_nMyPlayerID = nUserID;
	m_pMyPlayer = GetPlayer(m_nMyPlayerID);
	m_CameraWarp = true;
}

bool CObjectContainer::ShouldWarpCamera()
{
	if (m_CameraWarp)
	{
		m_CameraWarp = false;
		return true;
	}
	else
	{
		return false;
	}
}

CPlayerObject* CObjectContainer::GetMyPlayer(void)
{
	return m_pMyPlayer;
}

CPlayerObject* CObjectContainer::GetPlayer(int nUserID)
{
	CCriticalSection::Owner lock(m_csPlayersMap);
	auto iter = m_Players.find(nUserID);
	if (iter == m_Players.end())
		return nullptr;
	return iter->second;
}

void CObjectContainer::GetPlayers(double x, double y, double radius, std::set<CPlayerObject*>& players)
{
	ST_VECTOR stFrom = { x, y };
	CCriticalSection::Owner lock(m_csPlayersMap);
	for (auto iter = m_Players.begin(); iter != m_Players.end(); iter++)
	{
		ST_VECTOR stTo = { iter->second->m_PosX, iter->second->m_PosY };
		double dDistance = GetDistance(stTo, stFrom);
		if (dDistance <= radius)
			players.insert(iter->second);
	}
}

void CObjectContainer::QueryObjects(ST_GAME_INFO& outGameInfo)
{
	CCriticalSection::Owner lock1(csObjectList);
	CCriticalSection::Owner lock2(m_csPlayersMap);

	outGameInfo.dwGameTick = GlobalTimer()->GetTime();

	int nObjectCount = 0;
	int nPlayerCount = 0;
	int nMonsterCount = 0;
	for (CObjectSuper* pObject : ObjectList)
	{
		if (g_nMaxObjecCount <= nObjectCount)
			break;

		if (OBJECT_TYPE_NPC == pObject->m_Type)
			continue;

		int nObjectIndex = nObjectCount++;
		outGameInfo.objects[nObjectIndex] = *static_cast<ST_OBJECT_DATA*>(pObject);

		if (OBJECT_TYPE_PLAYER == pObject->m_Type)
		{
			CPlayerObject* pPlayer = static_cast<CPlayerObject*>(pObject);
			int nIndex = nPlayerCount++;
			outGameInfo.playerData[nIndex] = *static_cast<ST_PLAYER_DATA*>(pPlayer);
		}

		if (OBJECT_TYPE_MONSTER == pObject->m_Type)
		{
			CMonsterObject* pMonster = static_cast<CMonsterObject*>(pObject);
			int nIndex = nMonsterCount++;
			outGameInfo.monsterData[nIndex] = *static_cast<ST_MONSTER_DATA*>(pMonster);
		}
	}

	outGameInfo.nObjectCount = nObjectCount;
	outGameInfo.nPlayerCount = nPlayerCount;
}

CObjectSuper* CObjectContainer::FindObject(double x, double y, E_OBJECT_TYPE nType)
{
	for (CObjectSuper* pObject : ObjectList)
	{
		if (OBJECT_TYPE_UNDEFINED != nType && pObject->m_Type != nType)
			continue;

		double diffX = abs(pObject->m_PosX - x) - 1;
		double diffY = abs(pObject->m_PosY - y) - 1;
		if (diffX < pObject->m_Width && diffY < pObject->m_Height)
			return pObject;
	}
	return nullptr;
}

int CObjectContainer::CountMonster(int nMonsterID)
{
	int nCount = 0;
	for (CObjectSuper* pObject : ObjectList)
	{
		if (OBJECT_TYPE_MONSTER != pObject->m_Type)
			continue;

		const CMonsterObject* pMonster = (CMonsterObject*)pObject;
		if (nMonsterID != pMonster->m_nMonsterID)
			continue;

		nCount++;
	}
	return nCount;
}

