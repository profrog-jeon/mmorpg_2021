#include "stdafx.h"
#include "MonsterObject.h"
#include "MissileObject.h"
#include "TextObject.h"

CMonsterObject::CMonsterObject(const ST_OBJECT_DATA& objData, const ST_MONSTER_DATA& monsterData)
	: ST_MONSTER_DATA(monsterData)
{
	ST_OBJECT_DATA* pThis = static_cast<ST_OBJECT_DATA*>(this);
	*pThis = objData;

	m_strName = Format("%s (%d/%d)", m_szName, (int)m_HP, (int)m_MaxHP);
}

CMonsterObject::CMonsterObject(const ST_MONSTER_REGEN_DATA& regenData)
	: CObjectSuper(OBJECT_TYPE_MONSTER)
{
	Init(regenData.id, regenData.szName, regenData.x, regenData.y, regenData.hp, regenData.movingRange);
	m_PosX = regenData.x;
	m_PosY = regenData.y;
	m_Width = regenData.w;
	m_Height = regenData.h;
	m_Weight = 1;
	m_strName = Format("%s (%d/%d)", m_szName, (int)m_HP, (int)m_MaxHP);
}

CMonsterObject::~CMonsterObject(void)
{
}

void CMonsterObject::Destroy(void)
{
	m_HP = 0;
}

void CMonsterObject::OnUpdate(double dElapsedSec)
{
	m_VelX = m_TargetPosX - m_PosX;
	m_VelY = m_TargetPosY - m_PosY;
}

bool CMonsterObject::IsDestroyed(void)
{
	return (int)m_HP <= 0;
}

void CMonsterObject::OnCollide(const CObjectSuper* pOther)
{
	if (OBJECT_TYPE_MISSILE != pOther->m_Type)
		return;

	const CMissileObject* pMissile = dynamic_cast<const CMissileObject*>(pOther);
	if (nullptr == pMissile)
		return;

	const double dDamage = pMissile->m_Power * (GlobalTimer()->GetTime() % 50 + 50) * 0.01;
	m_HP -= dDamage;
	Objs()->AddObject(new CTextObject(pOther->m_PosX, pOther->m_PosY, Format("%d", (int)dDamage), 0.5, ANIMATE_RISE));

	{	// ������ �����ϰ� ƨ�ܳ�����
		double dAngle = (double)GlobalTimer()->GetTime() * 7;
		double dDistance = m_MovingRadius * (double)(GlobalTimer()->GetTime() % 17 + 1) / 17;
		m_TargetPosX = m_RegenPosX + sin(dAngle) * dDistance;
		m_TargetPosY = m_RegenPosY + cos(dAngle) * dDistance;
	}

	if (m_HP < 0)
	{
		m_strName.clear();
		CPlayerObject* pMissileOwnerPlayer = Objs()->GetPlayer(pMissile->m_nPlayerID);
		pMissileOwnerPlayer->AddKillCount(m_nMonsterID);
		return;
	}

	m_strName = Format("%s (%d/%d)", m_szName, (int)m_HP, (int)m_MaxHP);
}

void CMonsterObject::Draw(const ST_VIEWPORT& viewport, CRendererSuper* pRenderer)
{
	__super::Draw(viewport, pRenderer);
}
