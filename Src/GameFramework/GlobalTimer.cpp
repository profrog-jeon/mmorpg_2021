#include "stdafx.h"
#include "GlobalTimer.h"

CGlobalTimer::CGlobalTimer(void)
	: m_dwTickCount(0)
{
}

CGlobalTimer::~CGlobalTimer(void)
{
}

void CGlobalTimer::SetTime(DWORD dwTickCount)
{
	m_dwTickCount = dwTickCount;
}

void CGlobalTimer::Ticking(void)
{
	m_dwTickCount++;
}

DWORD CGlobalTimer::GetTime(void)
{
	return m_dwTickCount;
}
